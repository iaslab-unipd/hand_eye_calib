/*
 *   HandEyeUtil.h
 *
 *   Last edit: 27/03/2017
 *   Author: Andrea Gobbi
 */

#ifndef HANDEYEUTIL_H_
#define HANDEYEUTIL_H_

#include <visp/vpDisplayX.h>
#include <visp/vpImage.h>
#include <visp/vpDot2.h>
#include <visp/vpImagePoint.h>
#include <visp/vpPose.h>
#include <visp/vpImageConvert.h>
#include <visp/vpPixelMeterConversion.h>
#include <visp/vpMeterPixelConversion.h>
#include <visp/vpImageIo.h>
#include <visp/vpCalibration.h>

#undef Success // bug
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>

#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/transforms.h>

#include <fstream>
#include <sstream>
#include <thread>        
#include <chrono>
#include <vector>
#include <math.h>

namespace test_hand_eye
{

class HandEyeUtil
{

private:
	//double resolution = 0.6; // Not used
    double HIGH_VALUE = 1000.0;

    /**
     * @brief sign return 1 or -1 depending from the sign of x
     * @param   x: a variable to check the sign
     * @return  1 if x >= 0, -1 otherwise
     */
    double 
    sign( double x );

    /**
     * @brief normalizeQuaternion normalize the quaternion given in input
     * @param   q: the quaternion to normalize
     * @return  the normalized quaternion
     */
    Eigen::Quaterniond 
    normalizeQuaternion( Eigen::Quaterniond q );
    
    /**
     * @brief inverseSignQuaternion changes the sign of the quaternion components.
     * @param   q: the quaternion to inverse the sign
     * @return  the inverse sign quaternion
     */
    Eigen::Quaterniond 
    inverseSignQuaternion( Eigen::Quaterniond q );

    /**
     * @brief weightQuaternion modulate the quaternion given in input with the weight given
     * @param   q: the quaternion to modulate
     * @param   weight: the weight of the quaternion
     * @return  the weighted quaternion
     */
    Eigen::Quaterniond 
    weightQuaternion( Eigen::Quaterniond q, double weight );

    /**
     * @brief areQuaternionsClose controls if two quaternion are close in the space 
              (used to check if two quaternion are similar, but with reversed sign)
     * @param   q1: a quaternion
     * @param   q2: a quaternion
     * @return  true if q1 and q2 are close in the space, false otherwise
     */
    bool 
    areQuaternionsClose( Eigen::Quaterniond q1, Eigen::Quaterniond q2 );

    /**
     * @brief findMin find the index of the lower element in the vector
     * @param[in]   vect: vector of double numbers
     * @return      index of the lower element
     */
    int 
    findMin( std::vector<double>& vect );

    /**
     * @brief matrixIntoQuaternion transform a 3x3 Matrix in the corresponding Quaternion
     * @param[in]  rot_matrix: the matrix to transform
     * @return     the resulting quaternion
     */
    Eigen::Quaterniond 
    matrixIntoQuaternion( Eigen::Matrix3d rot_matrix );

    /**
     * @brief averageQuaternion
     * @param[in]   quaternions: a vector that contains all the quaternion to average
     * @param[in]   weights: a vector that contains the weight of the quaternions in the vector
     * @return      the averaged quaternion
     */
    Eigen::Quaterniond 
    averageQuaternion( std::vector<Eigen::Quaterniond>& quaternions, 
                       std::vector<double> weights );

    /**
     * @brief averageVector
     * @param   vectors: a vector that contains all the Eigen::Vector3d to average
     * @param   weights: a vector that contains the weight of the Eigen::Vector3d
     * @return  the averaged vector
     */
    Eigen::Vector3d 
    averageVector( std::vector<Eigen::Vector3d>& vectors, 
                   std::vector<double> weights );

    /**
     * Not used
     * @brief imagesMinorError calculates the indices of the images with lower Euclidean distance in the two vector
     * @param[in]   r_diff: vector of the rotational differences with the average quaternion
     * @param[in]   t_dist: vector of the translational differences with the average translational vector
     * @param[in]   image_selected_number: number of the selected images
     * @return      indeces of the #image_selected_number selected images
     */
    std::vector<int> 
    imagesMinorError( std::vector<double> r_diff, 
                      std::vector<double> t_dist, 
                      int image_selected_number );
    
public:
    /**
     * @brief computePose return the camera -> object matrix obtained with the computePose function of ViSP
     * @param[in]  point: vector of reference corner coordinates in the chessboard  
     * @param[in]  corners: vector of found corners (using the OpenCV function) in the image 
     * @param[in]  cam: camera intrinsic parameters
     * @param[in]  init: always true
     * @param[out] cMo: CAM->CHESSBOARD (camera -> object) matrix
     */
    void 
    computePose( std::vector<vpPoint> &point, 
                 const std::vector<cv::Point2f> &corners,
                 const vpCameraParameters &cam, 
                 bool init, 
                 vpHomogeneousMatrix &cMo );

    /**
     * @brief returnITError2D computes the error value of the calibration in the same way of IT Robotics, and later 
              orders the images by increasing error 
     * @param[in]  w_e_visp: vector of BASE->TCP matrices
     * @param[in]  eMc_visp: calibration Hand-Eye matrix
     * @param[in]  c_o_visp: vector of CAM->CHESSBOARD matrices
     * @param[in]  images_corners: vector with all the corners of all the images
     * @param[in]  areCornersImagesInverted: vector that reports the images with inverse order of the corners
     * @param[out] images_error_order_2D: output vector of the ordered images for increasing error
     * @param[in]  rows, cols: number of rows and columns of corners
     * @param[in]  width, height: width and height of one image in pixels
     * @param[in]  focal_x, focal_y: camera focal lengths
     * @param[in]  c_x, c_y: optical centers expressed in pixels coordinates
     * @param[in]  L: side of a square of the chessboard in [mm]
     * @return     IT_err, the error computed in pixels
     */
	double
	returnITError2D( std::vector<vpHomogeneousMatrix>& w_e_visp,
				     vpHomogeneousMatrix& eMc_visp,
                     std::vector<vpHomogeneousMatrix>& c_o_visp,
                     std::vector<std::vector<cv::Point2f>>& images_corners,
                     std::vector<bool>& areCornersImagesInverted, 
                     std::vector<int>& images_error_order_2D,
				     const int rows, const int cols,
                     const int width, const int height,
				     const double focal_x, const double focal_y,
				     const double c_x, const double c_y,
				     const double L );

    /**
     * @brief returnReprojectionError3D computes the error value of the projection of the corners respect
              the scene frame, and later orders the images by increasing error
     * @param[in]  w_e_visp: vector of BASE->TCP matrices
     * @param[in]  eMc_visp: calibration Hand-Eye matrix
     * @param[in]  c_o_visp: vector of CAM->CHESSBOARD matrices
     * @param[out] images_error_order_3D: output vector of the ordered images for increasing error
     * @param[in]  rows, cols: number of rows and columns of corners
     * @param[out] source: output PointCloud of the original corners
     * @param[out] transformed: output PointCloud of the projected corners
     * @param[in]  L: side of a square of the chessboard in [mm]
     * @return     rep_err, the error computed in mm
     */
    double
    returnReprojectionError3D( std::vector<vpHomogeneousMatrix>& w_e_visp,
                               vpHomogeneousMatrix& eMc_visp,
                               std::vector<vpHomogeneousMatrix>& c_o_visp,
                               std::vector<int>& images_error_order_3D,
                               const int rows, const int cols,
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr& source,
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr& transformed,
                               const double L );

    /**
     * @brief calculateErrors is the original function that computes the calibration error using the standard deviation
     * @param[in]  w_e: vector of BASE->TCP matrices
     * @param[in]  eMc: calibration Hand-Eye matrix
     * @param[in]  c_o: vector of CAM->CHESSBOARD matrices
     * @return     tmp: vector of length 4 (tmp[0] -> translation error, tmp[1]-tmp[2]-tmp[3] -> x-y-z rotation error)
     */
    std::vector<double> 
    calculateErrors( vpHomogeneousMatrix& eMc, 
                     std::vector<vpHomogeneousMatrix>& c_o, 
                     std::vector<vpHomogeneousMatrix>& w_e );

    /**
     * Not used
     * @brief selectImagesN isn't used, it selects the image_selected_number images with lower error, 
              valued by means of the Euclidean distance from the average quaternion and the average translation vector 
              (obtained from the product c_o_matrix * eMc_matrix * w_e_matrix)
     * @param[in]  w_e: vector of BASE->TCP matrices
     * @param[in]  eMc: calibration Hand-Eye matrix (calculated with all the images of the dataset)
     * @param[in]  c_o: vector of CAM->CHESSBOARD matrices
     * @param[in]  image_selected_number: number of the selected images
     * @return vector with the indeces of the selected images 
     */
    /*
    std::vector<int> 
    selectImagesN( std::vector<vpHomogeneousMatrix>& w_e,
                   vpHomogeneousMatrix& eMc,               
                   std::vector<vpHomogeneousMatrix>& c_o, 
                   int image_selected_number );
    */
    /**
     * @brief Calibration of the camera with the pictures given to obtain the intrinsic parameters
     * @param[in] img_path: path of the folder with the images        
     * @param[in] img_extension: extension of the images
     * @param[in] img_number: number of images
     * @param[in] square_size: size of the square in mm
     * @param[in] width: number of squares in one row
     * @param[in] height: number of squares in one column
     * @param[out] focal_x, focal_y: camera focal lengths
     * @param[out] c_x, c_y: optical centers expressed in pixels coordinates
     */
    void 
    cameraCalibration( std::string img_path, 
    				   std::string img_extension, 
    				   const int img_number, const float square_size, 
                       const int width, const int height, 
    				   float& focal_x, float& focal_y, 
                       float& c_x, float& c_y );

    /**
     * @brief Visualization of the results
     * @param[in]  w_e: vector of BASE->TCP matrices
     * @param[in]  eMc: calibration Hand-Eye matrix (calculated with all the images of the dataset)
     * @param[in]  c_o: vector of CAM->CHESSBOARD matrices
     */    
    void 
    stampResult( std::vector<vpHomogeneousMatrix>& w_e,
                 vpHomogeneousMatrix& eMc,               
                 std::vector<vpHomogeneousMatrix>& c_o );
}; 

} // namespace test_hand_eye

#endif /* TEST_HAND_EYE_INCLUDE_TEST_AND_EYE_HANDEYEUTIL_H_ */