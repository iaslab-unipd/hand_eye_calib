/**
 *   AutoAcquisitionUtil.h
 *
 *   Last edit: 04/04/2017
 *   Author: Andrea Gobbi
**/

#ifndef AUTOACQUISITIONUTIL_H_
#define AUTOACQUISITIONUTIL_H_

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
//#include <test_hand_eye/SaveImage.h>
//#include <test_hand_eye/FindChessboard.h>

//#undef Success // bug
//#include <tf/transform_datatypes.h>
#include <tf_conversions/tf_eigen.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <test_hand_eye/transform_manager.h>
//#include <geometry_msgs/Transform.h>
//#include <tf/tf.h>
#include <sensor_msgs/CameraInfo.h>

#include <visp/vpPose.h>
#include <visp/vpImageConvert.h>
#include <visp/vpPixelMeterConversion.h>
#include <visp/vpMeterPixelConversion.h>
#include <visp/vpImageIo.h>
#include <visp/vpCalibration.h>

#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <fstream>
#include <sstream>
//#include <thread>        
//#include <chrono>
#include <vector>
//#include <math.h>

namespace test_hand_eye
{

class AutoAcquisitionUtil
{
	#define PI 3.14159
	#define radians(deg)  deg * PI / 180
	#define degrees(rad)  rad * 180 / PI

private:
	/**
	 * @brief loadPoseFromFile reads a file with a pose and puts the values in a matrix 
	 * @param[in]  filename: path of the file
	 * @param[out] motion_matrix: output matrix 
	 * @return     the success of the operation
	 */
	bool 
	loadPoseFromFile( const std::string& filename, 
	                  Eigen::MatrixXd& motion_matrix );

	/**
	 * @brief getTransformMatrix, given the translation and the rotation vectors, return the rototranslation matrix
	 * @param[in]  transl: translation vector (x, y, z) [m]
	 * @param[in]  rot_degree: rotation vector (rot_x, rot_y, rot_z) [rad]
	 * @return     tf_matrix, the rototranslation matrix
	 */
	tf::Transform 
	getTransformMatrix( const tf::Vector3& transl, 
	                    const tf::Vector3& rot );
	
	/**
	 * @brief getTranslAndRotRPYVectors, given the rototranslation matrix, 
	 *        return the translation and the rotation vectors
	 * @param[in]   tf_matrix: rototranslation matrix
	 * @param[out]  transl: translation vector (x, y, z) [m]
	 * @return      rot_degree, rotation vector (rot_x, rot_y, rot_z) [rad]
	 */
	tf::Vector3 
	getTranslAndRotRPYVectors( const tf::Transform& tf_matrix, 
	                           tf::Vector3& transl );

	/** 
	 * @brief planJointMotionToGoal plans to a pose goal for the joints 
	          of the robot and, if the planning is successful, performs the movement
	 * @param[in]  group: MoveGroup object that performs the movement
	 * @param[in]  joint_values: vector of the joint desired final values 
	 * @param[in]  time: time for the function sleep
	 * @return     success, the success of the operation
	 */
	bool 
	planJointMotionToGoal( moveit::planning_interface::MoveGroup& group, 
	                       const std::vector<double>& joint_values, 
	                       const double time );
public:
    /**
     * @brief computePose return the camera -> object matrix obtained with the computePose function of ViSP
     * @param[in]  point: vector of reference corner coordinates in the chessboard  
     * @param[in]  corners: vector of found corners (using the OpenCV function) in the image 
     * @param[in]  cam: camera intrinsic parameters
     * @param[in]  init: always true
     * @param[out] cMo: CAM->CHESSBOARD (camera -> object) matrix
     */
    void 
    computePose( std::vector<vpPoint> &point, 
                 const std::vector<cv::Point2f> &corners,
                 const vpCameraParameters &cam, 
                 bool init, 
                 vpHomogeneousMatrix &cMo );

	/** 
	 * @brief computeMovement calls planJointMotionToGoal to perform the movement to one or more joint poses
	 * @param[in]  group: MoveGroup object that performs the movement
	 * @param[in]  motion_matrix: matrix with one or more joint poses to perform
	 * @param[in]  time: time for the function sleep
	 * @return     success, the success of the operation
	 */
	bool 
	computeMovement( moveit::planning_interface::MoveGroup& group, 
	                 const Eigen::MatrixXd& motion_matrix, 
	                 const double time );

	/**
	 * @brief planPoseMotionToGoal plans to a pose goal for the end effector of the robot and,
	          if the planning is successful, performs the movement
	 * @param[in]  group: MoveGroup object that performs the movement
	 * @param[in]  transl: translation vector (x, y, z) [m]
	 * @param[in]  quat: rotation quaternion 
	 * @param[in]  time: time for the function sleep
	 * @return     success, the success of the operation
	 */
	bool 
	planPoseMotionToGoal( moveit::planning_interface::MoveGroup& group, 
	                      const tf::Vector3& transl,
	                      const tf::Quaternion& quat,
	                      const double time );

	/**
	 * @brief planPoseMotionToGoal plans to a pose goal for the end effector of the robot respect the camera and,
	          if the planning is successful, performs the movement
	 * @param[in]  group: MoveGroup object that performs the movement
	 * @param[in]  transl: translation vector (x, y, z) [m]
	 * @param[in]  quat: rotation quaternion 
	 * @param[in]  hand_eye: computed hand-eye
	 * @param[in]  time: time for the function sleep
	 * @return     success, the success of the operation
	 */
	bool 
	planPoseMotionToCameraGoal( moveit::planning_interface::MoveGroup& group, 
	                      		const tf::Vector3& transl,
	                      		const tf::Quaternion& quat,
	                      		const tf::Transform& hand_eye,
	                      		const double time );

	/** 
	 * @brief saveEEPosition saves in the text file the end effector position respect to the world reference
	 * @param[in]  fout: object useful to write the line in the output file
	 * @param[in]  tf_mng: object useful to find the transform between two frames
	 * @param[in]  base_frame: base frame to find BASE->TCP matrix (wMe)
	 * @param[in]  hand_frame: hand frame to find BASE->TCP matrix (wMe)
	 * @return     
	 */
	void 
	saveEEPosition( std::ofstream& fout,
                    TransformManager& tf_mng,
                    const std::string base_frame, 
                    const std::string hand_frame );

	/** 
	 * @brief saveEEPosition saves in the text file the end effector position respect to the world reference
	 * @param[in]  fout: object useful to write the line in the output file
	 * @param[in]  tf_mng: object useful to find the transform between two frames
	 * @param[in]  base_frame: base frame to find BASE->TCP matrix (wMe)
	 * @param[in]  hand_frame: hand frame to find BASE->TCP matrix (wMe)
	 * @param[in]  wMe: world - end effector matrix
	 * @return     
	 */
	void 
	saveEEPosition( std::ofstream& fout,
                    TransformManager& tf_mng,
                    const std::string base_frame, 
                    const std::string hand_frame,
                    vpHomogeneousMatrix& wMe );
};

} // namespace test_hand_eye

#endif /* TEST_HAND_EYE_INCLUDE_TEST_AND_EYE_AUTOACQUISITIONUTIL_H_ */