#ifndef TRANSFORM_MANAGER_H_
#define TRANSFORM_MANAGER_H_

#include <iostream>
#include <tf/transform_listener.h>
#include <eigen3/Eigen/Core>
#include <visp/vpHomogeneousMatrix.h>
#include <visp/vpVelocityTwistMatrix.h>

class TransformManager
{
public:
    TransformManager(ros::NodeHandle &nh);
    ~TransformManager();

    bool get_Tf(const std::string &tf_b, const std::string &tf_o, tf::Transform &tf, const double duration = 0.2);
    bool get_Jv(const std::string &tf_b, const std::string &tf_o, Eigen::MatrixXd &Jv, const double duration = 0.2);
    bool get_q(const std::string &tf_b, const std::string &tf_o, tf::Quaternion &q, const double duration = 0.2);
    bool get_R(const std::string &tf_b, const std::string &tf_o, tf::Matrix3x3 &R, const double duration = 0.2);
    bool get_t(const std::string &tf_b, const std::string &tf_o, tf::Vector3 &t, const double duration = 0.2);

    bool get_Tf(const std::string &tf_b, const std::string &tf_o, vpHomogeneousMatrix &tf, const double duration = 0.2);
    bool get_Jv(const std::string &tf_b, const std::string &tf_o, vpVelocityTwistMatrix &Jv, const double duration = 0.2);
    bool get_q(const std::string &tf_b, const std::string &tf_o, vpQuaternionVector &q, const double duration = 0.2);
    bool get_R(const std::string &tf_b, const std::string &tf_o, vpRotationMatrix &R, const double duration = 0.2);
    bool get_t(const std::string &tf_b, const std::string &tf_o, vpTranslationVector &t, const double duration = 0.2);

private:
    tf::TransformListener tf_listener_;
};


#endif //TRANSFORM_MANAGER_H_
