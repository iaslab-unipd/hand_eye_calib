#Preliminary steps in a nutshell#

This repo contains tools to perform the hand-eye calibration offline (branch *master*) and online. The online tools can work in our simulated environment in $Gazebo$ (branch *master*) or with a real robot (branch *real_robot*). Your camera intrinsics has to be calculated beforehand.

The offline tools need pre-acquired data. To compile them, you need *ROS* (*Indigo* or *Kinetic*), *VISP* (as explained in the following) and *PCL*. Your *VISP* and *PCLé directories have to be set manually from the project *CMake* file.

The online tools do not need pre-acquired data and aim at making both acquisition and calibration automatic thanks to the library *MoveIt!*. They were tested on the *UR10* robot.

#Preliminary steps#

```bash
sudo apt-get update
sudo apt-get install unzip liblapack-dev libxml2-dev libv4l-dev libzbar-dev libdc1394-22-dev libaria-dev libois-dev libdmtx-dev
sudo apt-get install ros-indigo-opencv-apps
sudo apt-get install ros-indigo-moveit-*
sudo apt-get install ros-indigo-gazebo-*

cd ~/Downloads/
wget https://github.com/Itseez/opencv/archive/2.4.12.zip -O opencv_2.4.12.zip
unzip opencv_2.4.12.zip
mv opencv-2.4.12 ~/workspace/opencv

wget http://gforge.inria.fr/frs/download.php/latestfile/475/visp-3.0.0.tar.gz
tar xzvf visp-3.0.0.tar.gz
mv visp-3.0.0 ~/workspace/visp

cd ~/workspace/
git clone https://github.com/PointCloudLibrary/pcl.git
cd pcl
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE - ..
make -j 8

cd ~/workspace/opencv
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D WITH_OPENCL=OFF ..
make -j 8

cd ~/workspace/visp
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE ..
make -j 8

cd ~/workspace/ros/catkin/src
git clone https://github.com/lagadic/vision_visp.git
git clone https://github.com/lagadic/visp_ros.git
git clone https://github.com/ros-industrial/universal_robot
git clone https://<your_bb_username>@bitbucket.org/iaslab-unipd/test_hand eye.git 
cd .. 
catkin_make
```

#Hand-eye calibration on pre-acquired data#

The main programs are the following:

* *OfflineHandEyeCircles*, if you have a circle grid, in particular the *OpenCV* asymmetrical circle grid.

* *OfflineHandEyeSquares*, if you have a checkerboard with squares.

* *OfflineHandEyeCirclesMyAcquisition*, if you acquired it by means of the nodes *AutoAcquisition* and *FindChessboardFrame* (i.e. in the simulation environment and with the circle grid).

##How to use them?##

They all need a set of images and poses. You can find a sample dataset in the folder *dataset_circles*. It contains a set of images (.bmp format, .png or .jpg formats are ok too) and a txt file with robot poses in ZYZ Euler angle convention (x y z rx ry rz).

For example, after setting all the params in *config/demo_offline_hand_eye.yaml* (camera intrinsics and checkerboard params), run this command:
```bash
roslaunch test_hand_eye offline_hand_eye_calib.launch.
```
The angle convention ZYZ is still hard-coded, see line: "vpRzyzVector r(rad_x,rad_y,rad_z);". In case of need, just change it and recompile.

#Automatic hand-eye calibration#

The main programs are the following: 

* *MoveRobotTest*, to move the robot end-effector to a fixed Cartesian position (read from a config file) with respect to a fixed frame (read from a config file). 

* *FindChessboardFrame*, to calculate the checkerboard position and publish its frame. The robot must start from a position from which the checkerboard is visible. 

* *ScaleInvariantAutoAcquisition*, to automatically acquire images moving along the checkerboard reference frame. Many images are acquired moving the camera along several planes parallel/inclined w.r.t. the checkerboard frame. Fixed translations are performed.

* *AutoAcquisition*, similar to the previous one but more generic since it does not perform fixed translations. 

* *Util*, to print translation and quaternion of the affine matrix in the configuration file and calculate the angle between two quaternions read from the config file. These methods were exploited in the paper experimental evaluation.

##How to use them?##

* Launch the simulated environment in Gazebo setting the checkerboard:
```bash
roslaunch test_hand_eye simulation_acircle.launch limited:=true 
```

* Launch ROS and MoveIt!:
```bash
roslaunch ur10_moveit_config ur10_moveit_planning_execution.launch sim:=true
```

* Automatically calculate the checkerboard position and publish its frame: 
```bash
rosrun test_hand_eye FindChessboardFrame
```

* Automatically acquire images and robot positions: 
```bash
rosrun test_hand_eye AutoAcquisition 
```
or
```bash
rosrun test_hand_eye ScaleInvariantAutoAcquisition
```
# Related Publications #
Please cite this work if you make use of our system in any of your own endeavors:

* **[A Fully Automatic Hand-Eye Calibration System]**, *Antonello, Morris and Gobbi, Andrea and Michieletto, Stefano and Ghidoni, Stefano and Menegatti, Emanuele*, ECMR 2017.