#include <fstream>
#include <iostream>
#include <Eigen/Dense>

#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <ros/package.h>

bool planJointGoal(moveit::planning_interface::MoveGroup& group, const std::vector<double>& group_variable_values, double time)
{
	// Planning to a Pose goal
	// ^^^^^^^^^^^^^^^^^^^^^^^
	// We can plan a motion for this group to a desired pose for the
	// end-effector.
//	geometry_msgs::Pose target_pose1;
//	target_pose1.orientation.x = 0.1;
//	target_pose1.orientation.y = 0.9;
//	target_pose1.orientation.z = 0.1;
//	target_pose1.orientation.w = 0.01;
//	target_pose1.position.x = 0.01;
//	target_pose1.position.y = 0.5;
//	target_pose1.position.z = 0.4;
//	group.setPoseTarget(target_pose1);

	// Planning to a Joint goal
	// ^^^^^^^^^^^^^^^^^^^^^^^^
	// We can plan a motion for this group to a desired joint pose for the robot.
	group.setJointValueTarget(group_variable_values);

	group.setPlannerId("RRTConnectkConfigDefault");
	group.setPlanningTime(0.25);
	group.setMaxVelocityScalingFactor(0.1);

	int tentativi = 100;

	double time_sum = 0.0;
	int fail = 0;
	bool success;
	for (int i = 0; i < tentativi && !success; i++)
	{
		moveit::planning_interface::MoveGroup::Plan plan;
		success = group.plan(plan);
		if (success)
		{
			//group.execute(plan);
			time_sum += plan.planning_time_;
		}
		else
			fail++;
	}
	group.move();

    sleep(time);

	return success;
}

bool computeMovement(moveit::planning_interface::MoveGroup& group, const Eigen::MatrixXd& motion_matrix, double time)
{
	//std::cout << motion_matrix << std::endl << std::endl;

	std::vector<double> group_variable_values;
	group.getCurrentState()->copyJointGroupPositions(group.getCurrentState()->getRobotModel()->getJointModelGroup(group.getName()), group_variable_values);
	for(auto i=0; i< motion_matrix.cols(); ++i)
	{
		for (auto j=0; j<6; ++j)
			group_variable_values[j] = motion_matrix(j,i);
        planJointGoal(group, group_variable_values,time);
	}
	return true;
}

template <class MatrixT>
bool loadFromFile(const std::string& filename, Eigen::PlainObjectBase<MatrixT>& result)
{
	std::ifstream file(filename);
	if(!file.is_open())
		return false;

	size_t rows = 1;
	size_t cols = 0;

	std::string line;
	if(!std::getline(file, line))
		return false;

	std::stringstream line_stream(line);
	typename MatrixT::Scalar element;
	while(line_stream >> element)
			cols++;

	while(std::getline(file, line))
		rows++;

	if(rows*cols == 0)
		return false;

	result.resize(rows, cols);
	file.clear();
	file.seekg(0); // returns to beginning of the file
	size_t i_row = 0, i_col;
	while(std::getline(file, line))
	{
		i_col = 0;
		std::stringstream line_stream(line);
		typename MatrixT::Scalar element;
		while(line_stream >> element)
			result(i_row, i_col++) = element;

		if(i_col != cols)
		{
			std::cerr << "error at line: " << i_row << " invalid element number: " << i_col << std::endl;
			std::cerr << line << std::endl;
			return false;
		}

		i_row++;
	}

	file.close();

	//std::cout << "Matrix:" << std::endl << result << std::endl; 
	return true;
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "basic_motion_node");
	ros::NodeHandle node_handle;
	ros::AsyncSpinner spinner(1);
	spinner.start();

	std::string
		path_dir,
		root_dir,
		sub_dir;
	std::vector<std::string> motion_names;

	ROS_INFO("loading parameters from yaml files...");
	node_handle.param(ros::this_node::getName() + "/root_dir", root_dir, std::string(""));
	node_handle.param(ros::this_node::getName() + "/sub_dir", sub_dir, std::string("/demo/"));
    node_handle.param(ros::this_node::getName() + "/motion_names", motion_names, std::vector<std::string>() = {"stop", "01_pose_01.txt", "stop", "02_pose_02.txt", "stop"});

    if (root_dir.compare("") == 0)
    {
    	root_dir = ros::package::getPath("test_hand_eye");
    }

    path_dir = root_dir + sub_dir;
    ROS_INFO_STREAM("Path dir: " << path_dir);

	Eigen::MatrixXd motion_matrix;

	// Setup
	// ^^^^^
	//
	// The :move_group_interface:`MoveGroup` class can be easily
	// setup using just the name
	// of the group you would like to control and plan for.
	//moveit::planning_interface::MoveGroup group("manipulator");
	moveit::planning_interface::MoveGroup group("manipulator");

	moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

	group.setStartStateToCurrentState();

	for(auto i=0; i < motion_names.size(); i++)
	{
		if (motion_names[i].compare("stop") == 0)
		{
			ROS_INFO("Manual operation ...");
			char c;
			std::cin >> c;
		}
		else
		{
			ROS_INFO_STREAM("Loading file: " << path_dir << motion_names[i]);
            double time = 0.1;
            switch (motion_names[i].c_str()[1]) {
            case '1':
            case '3':
            case '5':
                time = 5.0;
                break;
            default:
                break;
            }
			loadFromFile(path_dir + motion_names[i],motion_matrix);
            computeMovement(group,motion_matrix,time);
		}
	}

	ros::waitForShutdown();

	return 0;
}
