/*
 *   HandEyeUtil.cpp
 *
 *   Last edit: 27/03/2017
 *   Author: Andrea Gobbi
 */

#include "test_hand_eye/HandEyeUtil.h"

namespace test_hand_eye
{
	/**
     * @brief sign return 1 or -1 depending from the sign of x
     * @param   x: a variable to check the sign
     * @return  1 if x >= 0, -1 otherwise
     */
    double HandEyeUtil::sign( double x )
    {
        return (x >= 0.0f) ? +1.0f : -1.0f;
    }

    /**
     * @brief normalizeQuaternion normalize the quaternion given in input
     * @param   q: the quaternion to normalize
     * @return  the normalized quaternion
     */
    Eigen::Quaterniond HandEyeUtil::normalizeQuaternion( Eigen::Quaterniond q )
    {    
        double normalizing_factor = sqrt(1.0f / (q.w() * q.w() + q.x() * q.x() + q.y() * q.y() + q.z() * q.z()));
        Eigen::Quaterniond normalized_quaternion( q.w() * normalizing_factor, 
                                                  q.x() * normalizing_factor,
                                                  q.y() * normalizing_factor,
                                                  q.z() * normalizing_factor ); 

        return normalized_quaternion;
    }

    /**
     * @brief inverseSignQuaternion changes the sign of the quaternion components.
     * @param   q: the quaternion to inverse the sign
     * @return  the inverse sign quaternion
     */
    Eigen::Quaterniond HandEyeUtil::inverseSignQuaternion( Eigen::Quaterniond q )
    {
        Eigen::Quaterniond inverse_sign_quaternion( -q.w(), -q.x(), -q.y(), -q.z() );

        return inverse_sign_quaternion;
    }

    /**
     * @brief weightQuaternion modulate the quaternion given in input with the weight given
     * @param   q: the quaternion to modulate
     * @param   weight: the weight of the quaternion
     * @return  the weighted quaternion
     */
    Eigen::Quaterniond HandEyeUtil::weightQuaternion( Eigen::Quaterniond q, double weight )
    {
        Eigen::Quaterniond weighted_quaternion( q.w() * weight, q.x() * weight, q.y() * weight, q.z() * weight );

        return weighted_quaternion;
    }

    /**
     * @brief areQuaternionsClose controls if two quaternion are close in the space 
              (used to check if two quaternion are similar, but with reversed sign)
     * @param   q1: a quaternion
     * @param   q2: a quaternion
     * @return  true if q1 and q2 are close in the space, false otherwise
     */
    bool HandEyeUtil::areQuaternionsClose( Eigen::Quaterniond q1, Eigen::Quaterniond q2 )
    {
        double dotProduct = q1.x() * q2.x() + q1.y() * q2.y() + q1.z() * q2.z() + q1.w() * q2.w();
        
        return dotProduct >= 0.0f;
    }

    /**
     * @brief findMin find the index of the lower element in the vector
     * @param[in]   vect: vector of double numbers
     * @return      index of the lower element
     */
    int HandEyeUtil::findMin( std::vector<double>& vect )
    {
        int index_min = 0;

        for( int i=1; i<vect.size(); ++i )
            if( vect[i]<vect[index_min] )
                index_min = i;

        vect[ index_min ] = HIGH_VALUE;

        return index_min;
    }

    /**
     * @brief matrixIntoQuaternion transform a 3x3 Matrix in the corresponding Quaternion
     * @param[in]  rot_matrix: the matrix to transform
     * @return     the resulting quaternion
     */
    Eigen::Quaterniond HandEyeUtil::matrixIntoQuaternion( Eigen::Matrix3d rot_matrix )
    {
        //Mathematical conversion of matrix in quaternion
        double w = ( rot_matrix(0,0) + rot_matrix(1,1) + rot_matrix(2,2) + 1.0f) / 4.0f;
        double x = ( rot_matrix(0,0) - rot_matrix(1,1) - rot_matrix(2,2) + 1.0f) / 4.0f;
        double y = (-rot_matrix(0,0) + rot_matrix(1,1) - rot_matrix(2,2) + 1.0f) / 4.0f;
        double z = (-rot_matrix(0,0) - rot_matrix(1,1) + rot_matrix(2,2) + 1.0f) / 4.0f;

        //Check boundaries
        if(w < 0.0f)
            w = 0.0f;
        if(x < 0.0f)
            x = 0.0f;
        if(y < 0.0f)
            y = 0.0f;
        if(z < 0.0f)
            z = 0.0f;

        w = sqrt(w);
        x = sqrt(x);
        y = sqrt(y);
        z = sqrt(z);

        if(w >= x && w >= y && w >= z)
        {
            w *= +1.0f;
            x *= sign(rot_matrix(2,1) - rot_matrix(1,2));
            y *= sign(rot_matrix(0,2) - rot_matrix(2,0));
            z *= sign(rot_matrix(1,0) - rot_matrix(0,1));
        }
        else if(x >= w && x >= y && x >= z)
        {
            w *= sign(rot_matrix(2,1) - rot_matrix(1,2));
            x *= +1.0f;
            y *= sign(rot_matrix(1,0) + rot_matrix(0,1));
            z *= sign(rot_matrix(0,2) + rot_matrix(2,0));
        }
        else if(y >= w && y >= x && y >= z)
        {
            w *= sign(rot_matrix(0,2) - rot_matrix(2,0));
            x *= sign(rot_matrix(1,0) + rot_matrix(0,1));
            y *= +1.0f;
            z *= sign(rot_matrix(2,1) + rot_matrix(1,2));
        }
        else if(z >= w && z >= x && z >= y)
        {
            w *= sign(rot_matrix(1,0) - rot_matrix(0,1));
            x *= sign(rot_matrix(2,0) + rot_matrix(0,2));
            y *= sign(rot_matrix(2,1) + rot_matrix(1,2));
            z *= +1.0f;
        }
        else
        {
            std::cout << "Impossible to create quaternion from this matrix" << std::endl;
        }

        //Create the new quaternion
        Eigen::Quaterniond quat( w, x, y, z );

        //Normalization, if necessary
        quat = normalizeQuaternion( quat );

        return quat;
    }

    /**
     * @brief averageQuaternion
     * @param[in]   quaternions: a vector that contains all the quaternion to average
     * @param[in]   weights: a vector that contains the weight of the quaternions in the vector
     * @return      the averaged quaternion
     */
    Eigen::Quaterniond HandEyeUtil::averageQuaternion( std::vector<Eigen::Quaterniond>& quaternions, 
                                                       std::vector<double> weights )
    {
        //Calculate total weight
        double tot_weight = 0;
        for(std::vector<double>::iterator it = weights.begin(); it != weights.end(); ++it)
        {
            tot_weight += *it;
        }

        Eigen::MatrixXd quaternion_matrix(4, quaternions.size());

        //Using the first quaternion of the list as reference for the sign
        Eigen::Quaterniond firstQuaternion = quaternions.at(0);

        int i = 0;
        for(std::vector<Eigen::Quaterniond>::iterator it = quaternions.begin(); it != quaternions.end(); ++it)
        {
            Eigen::Quaterniond nextQuaternion = *it;
            if(!areQuaternionsClose(nextQuaternion, firstQuaternion))
            {
                //Before we add the new rotation to the average, we have to check whether the quaternion has to be inverted.
                nextQuaternion = inverseSignQuaternion(nextQuaternion);
            }

            //Using score to weight the quaternion
            double nextWeight = weights.at(i) / tot_weight;
            nextQuaternion = weightQuaternion(nextQuaternion, nextWeight);

            //Fill the corresponding cols in the matrix
            quaternion_matrix(0, i) = nextQuaternion.w();
            quaternion_matrix(1, i) = nextQuaternion.x();
            quaternion_matrix(2, i) = nextQuaternion.y();
            quaternion_matrix(3, i) = nextQuaternion.z();

            ++i;
        }

        //Compute svd. The first cols of U matrix is the averaged quaternion
        Eigen::JacobiSVD<Eigen::MatrixXd> svd(quaternion_matrix, Eigen::ComputeFullU);
        Eigen::MatrixXd EigenVectors = svd.matrixU();

        //Create the averaged quaternion
        Eigen::Quaterniond averaged_quaternion( EigenVectors(0,0),
                                                EigenVectors(1,0),
                                                EigenVectors(2,0),
                                                EigenVectors(3,0) );

        //Invert the sign to keep rotations consistent
        if(!areQuaternionsClose(averaged_quaternion, firstQuaternion))
        {
            averaged_quaternion = inverseSignQuaternion(averaged_quaternion);
        }

        //Normalize if necessary
        averaged_quaternion = normalizeQuaternion(averaged_quaternion);

        return averaged_quaternion;
    }

    /**
     * @brief averageVector
     * @param   vectors: a vector that contains all the Eigen::Vector3d to average
     * @param   weights: a vector that contains the weight of the Eigen::Vector3d
     * @return  the averaged vector
     */
    Eigen::Vector3d HandEyeUtil::averageVector( std::vector<Eigen::Vector3d>& vectors, 
                                                     std::vector<double> weights )
    {
        //Calculate total weight
        double tot_weight = 0;
        for(std::vector<double>::iterator it = weights.begin(); it != weights.end(); ++it)
        {
            tot_weight += *it;
        }

        //Calculate average vector
        double final_x = 0;
        double final_y = 0;
        double final_z = 0;
        int i = 0;
        for(std::vector<double>::iterator it = weights.begin(); it != weights.end(); ++it)
        {
            final_x += (vectors.at(i).x() * ((*it) / tot_weight));
            final_y += (vectors.at(i).y() * ((*it) / tot_weight));
            final_z += (vectors.at(i).z() * ((*it) / tot_weight));
            ++i;
        }

        return Eigen::Vector3d( final_x, final_y, final_z );
    }

    /**
     * Not used
     * @brief imagesMinorError calculates the indices of the images with lower Euclidean distance in the two vector
     * @param[in]   r_diff: vector of the rotational differences with the average quaternion
     * @param[in]   t_dist: vector of the translational differences with the average translational vector
     * @param[in]   image_selected_number: number of the selected images
     * @return      indeces of the #image_selected_number selected images
     */
    std::vector<int> HandEyeUtil::imagesMinorError( std::vector<double> r_diff, 
                                   					std::vector<double> t_dist, 
                                   					int image_selected_number )
    {
        std::vector<double> rotat( r_diff ); // Copy constructor first vector
        std::vector<double> trasl( t_dist ); // Copy constructor second vector

        // Create two vectors (rotat_position and trasl_position) where to the ith element 
        // is assigned the position that this would have if r_diff/t_dist were increasingly sorted 
        std::vector<double> rotat_position( r_diff.size() );
        std::vector<double> trasl_position( r_diff.size() );
        for( int i=0; i<r_diff.size(); ++i )
        {
            int index_min = findMin( rotat );
            //std::cout << " - " << i << " - " << index_min << " ";
            rotat_position[ index_min ] = i;

            index_min = findMin( trasl );
            //std::cout << index_min << std::endl;
            trasl_position[ index_min ] = i;
        }

        // For every image is computed the sum of the corrisponding values in two previous vectors
        std::vector<double> sum_position( r_diff.size() );
        for( int j=0; j<r_diff.size(); ++j )
        {
            sum_position[j] = rotat_position[j] + trasl_position[j];
            //std::cout << " - " << j << " - " << rotat_position[j] << " " << trasl_position[j] << " " << sum_position[j] << std::endl;
        }

        // The image_selected_number with lower sum in the vector sum_position are chosen
        std::vector<int> lower_error( image_selected_number );
        for( int k=0; k<image_selected_number; ++k )
        {
            int index_min = findMin( sum_position );
            lower_error[k] = index_min;
            //std::cout << " - " << k << " - " << lower_error[k] << " " << std::endl;
        }

        return lower_error;
    }

    /**
     * @brief computePose return the camera -> object matrix obtained with the computePose function of ViSP
     * @param[in]  point: vector of reference corner coordinates in the chessboard  
     * @param[in]  corners: vector of found corners (using the OpenCV function) in the image 
     * @param[in]  cam: camera intrinsic parameters
     * @param[in]  init: always true
     * @param[out] cMo: CAM->CHESSBOARD (camera -> object) matrix
     */
    void HandEyeUtil::computePose( std::vector<vpPoint> &point, 
                                   const std::vector<cv::Point2f> &corners,
                                   const vpCameraParameters &cam, 
                                   bool init, 
                                   vpHomogeneousMatrix &cMo )
    {
        vpPose pose;
        double x=0, y=0;
        for( int i=0; i<point.size(); ++i )
        {
            //The function finds the x and y coordinates in [m] of the corners in the image
            vpPixelMeterConversion::convertPoint( cam, vpImagePoint(corners[i].y, corners[i].x), x, y );

            point[i].set_x(x); //Update the vpPoint before using the computePose function
            point[i].set_y(y);
            pose.addPoint(point[i]);
        }

        // LAGRANGE -> Lagrange approach (test is done to switch between planar and non planar algorithm)
        // DEMENTHON -> Dementhon approach (test is done to switch between planar and non planar algorithm)
        // VIRTUAL_VS -> Virtual visual servoing approach
        // DEMENTHON_VIRTUAL_VS -> Virtual visual servoing approach initialized using Dementhon approach
        // LAGRANGE_VIRTUAL_VS -> Virtual visual servoing initialized using Lagrange approach 
        
        if( init == true ) 
        {
            vpHomogeneousMatrix cMo_dem;
            vpHomogeneousMatrix cMo_lag;
            pose.computePose( vpPose::DEMENTHON, cMo_dem ); //for a linear approach
            pose.computePose( vpPose::LAGRANGE, cMo_lag );  //for a non linear approach

            double residual_dem = pose.computeResidual( cMo_dem );
            double residual_lag = pose.computeResidual( cMo_lag );
            
            if( residual_dem < residual_lag )
                cMo = cMo_dem;
            else
                cMo = cMo_lag;
        }
        //Compute the camera -> object matrix using the ViSP function
        pose.computePose( vpPose::VIRTUAL_VS, cMo );
    }

    /**
     * @brief returnITError2D computes the error value of the calibration in the same way of IT Robotics, and later 
              orders the images by increasing error 
     * @param[in]  w_e_visp: vector of BASE->TCP matrices
     * @param[in]  eMc_visp: calibration Hand-Eye matrix
     * @param[in]  c_o_visp: vector of CAM->CHESSBOARD matrices
     * @param[in]  images_corners: vector with all the corners of all the images
     * @param[in]  areCornersImagesInverted: vector that reports the images with inverse order of the corners
     * @param[out] images_error_order_2D: output vector of the ordered images for increasing error
     * @param[in]  rows, cols: number of rows and columns of corners
     * @param[in]  width, height: width and height of one image in pixels
     * @param[in]  focal_x, focal_y: camera focal lengths
     * @param[in]  c_x, c_y: optical centers expressed in pixels coordinates
     * @param[in]  L: side of a square of the chessboard in [mm]
     * @return     IT_err, the error computed in pixels
     */
    double HandEyeUtil::returnITError2D( std::vector<vpHomogeneousMatrix>& w_e_visp,
                        			     vpHomogeneousMatrix& eMc_visp,
                                         std::vector<vpHomogeneousMatrix>& c_o_visp,
                                         std::vector<std::vector<cv::Point2f>>& images_corners,
                                         std::vector<bool>& areCornersImagesInverted, 
                                         std::vector<int>& images_error_order_2D,
                        			     const int cols, const int rows,
                                         const int width, const int height,          // 800 - 800 pixels
                        			     const double focal_x, const double focal_y, 
                        			     const double c_x, const double c_y,         
                        			     const double L )
    {
        std::vector<double> images_error_mean, images_error_dev; // Arithmetic mean and standard deviation
        double total_rep_err_mean = 0; // Value to return

        Eigen::Matrix4f eMc;
        // eMc found by IT+Robotics (for the dataset 201603_Calibration)
        //eMc << -0.013530614604462248, -0.998882606036236, 0.045282025426026148, 27.595201103361649,
        //        0.9998981223650425, -0.013310697260654026, 0.0051546318295026391, -3.2793921965400368,
        //       -0.0045461367432158544, 0.045347157537082551, 0.99896094415348347, 153.1841738801009,
        //        0, 0, 0, 1; 
        
        // Our eMc
        eMc << eMc_visp[0][0], eMc_visp[0][1], eMc_visp[0][2], eMc_visp[0][3],
               eMc_visp[1][0], eMc_visp[1][1], eMc_visp[1][2], eMc_visp[1][3],
               eMc_visp[2][0], eMc_visp[2][1], eMc_visp[2][2], eMc_visp[2][3],
               eMc_visp[3][0], eMc_visp[3][1], eMc_visp[3][2], eMc_visp[3][3];
        
        std::cout << " -- returnITError2D -- " << std::endl;

        for( int i=0; i<w_e_visp.size(); ++i ) 
        {
            // Conversion of the world -> end-effector matrix from ViSP to Eigen
            vpHomogeneousMatrix w_e_matrix = w_e_visp[i];
            Eigen::Matrix4f w_e;
            w_e << w_e_matrix[0][0], w_e_matrix[0][1], w_e_matrix[0][2], w_e_matrix[0][3],
                   w_e_matrix[1][0], w_e_matrix[1][1], w_e_matrix[1][2], w_e_matrix[1][3],
                   w_e_matrix[2][0], w_e_matrix[2][1], w_e_matrix[2][2], w_e_matrix[2][3],
                   w_e_matrix[3][0], w_e_matrix[3][1], w_e_matrix[3][2], w_e_matrix[3][3];
            //if(i == 0)
            //    std::cout << "w_e: " << std::endl << w_e << std::endl << std::endl;

            // Conversion of the camera -> object matrix from ViSP to Eigen
            //vpHomogeneousMatrix c_o_matrix = c_o_visp[i];
            //Eigen::Matrix4f c_o;
            //c_o << c_o_matrix[0][0], c_o_matrix[0][1], c_o_matrix[0][2], c_o_matrix[0][3],
            //       c_o_matrix[1][0], c_o_matrix[1][1], c_o_matrix[1][2], c_o_matrix[1][3],
            //       c_o_matrix[2][0], c_o_matrix[2][1], c_o_matrix[2][2], c_o_matrix[2][3],
            //       c_o_matrix[3][0], c_o_matrix[3][1], c_o_matrix[3][2], c_o_matrix[3][3];
            //if(i == 0)
            //    std::cout << "c_o: " << std::endl << c_o << std::endl << std::endl;

            //Eigen::Matrix4f projection_matrix = eMc.inverse() * w_e.inverse()
            Eigen::Matrix4f projection_matrix = (w_e * eMc).inverse();
            //if(i == 0)
            //    std::cout << "projection_matrix: " << std::endl << projection_matrix << std::endl << std::endl;
            
            cv::Mat chessboard_reprojected = cv::Mat::zeros( height, width, CV_8UC3 );

            pcl::PointCloud<pcl::PointXYZ>::Ptr source_p_c( new pcl::PointCloud<pcl::PointXYZ> );
            pcl::PointCloud<pcl::PointXYZ>::Ptr reprojected_p_c( new pcl::PointCloud<pcl::PointXYZ> );
            
            for( int x=0; x<cols; ++x )     //cols <- height-1 <- 11-1(10)
            {
                for( int y=0; y<rows; ++y ) //rows <- width-1 <- 9-1(8)
                {
                    //if((x == 0 && y == 0) || (x == 1 && y == 0)) // To verify the projection orientation 
                    //    continue;

                    // PointCloud with the corners in the 2D plane xy
                    pcl::PointXYZ point_source;
                    point_source.x = x * L;
                    point_source.y = y * L;
                    point_source.z = 0;

                    source_p_c->push_back( point_source );
                }
            }

            // PCL function that transform the source PointCloud according to the projection matrix
            pcl::transformPointCloud( *source_p_c, *reprojected_p_c, projection_matrix );
            // Corners of the i-th image
            std::vector<cv::Point2f> corners_tmp = images_corners[i];
            
            std::vector<double> temp_corners_err_dist;
            double temp_err_mean = 0, temp_err_dev = 0;
            
            for(int z=0; z<reprojected_p_c->size(); ++z)
            {
                //if( z == 0 || z == 1 ) // To verify the reprojection orientation 
                //    continue;

                pcl::PointXYZ point_transformed;
                point_transformed.x = reprojected_p_c->points[z].x;
                point_transformed.y = reprojected_p_c->points[z].y;
                point_transformed.z = reprojected_p_c->points[z].z;
                
                Eigen::Vector4f hom_p_3D( point_transformed.x, point_transformed.y, point_transformed.z, 1 );
                //if(i==0)
                //    std::cout << "hom_p_3D: " << hom_p_3D(0) << " " << hom_p_3D(1) << " " << hom_p_3D(2) << std::endl;

                double u = (focal_x * point_transformed.x / point_transformed.z) + c_x; 
                double v = (focal_y * point_transformed.y / point_transformed.z) + c_y;
                //if(i==0)
                //    std::cout << " u = " << u << ", v = " << v << std::endl;

                //u /= width;   // The normalization isn't necessary
                //v /= height;

                // Because the order of the reprojected corners is different of the order in the images_corners vector
                int corner_index = z / rows;
                if( !areCornersImagesInverted[i] )
                    corner_index = (corner_index + 1)*rows - 1 - z%rows;
                else
                    corner_index = (cols - corner_index - 1)*rows + z%rows;

                // To compute the Euclidean distance between the original corner and the reprojected one
                double diff_x = 0, diff_y = 0; 

                if( u>=0 && u<width && v>=0 && v<height )
                {
                    // The found point is drawn if it finishes inside the space of the image
                    cv::Point pt_corner;
                    pt_corner.x = corners_tmp[ corner_index ].x;
                    pt_corner.y = corners_tmp[ corner_index ].y;                            
                    cv::circle( chessboard_reprojected, pt_corner, 3, cv::Scalar(0,0,255), CV_FILLED );

                    cv::Point pt_reprojected;
                    pt_reprojected.x = u; 
                    pt_reprojected.y = v;                        // thickness,    BGR,          type
                    cv::circle( chessboard_reprojected, pt_reprojected, 3, cv::Scalar(0,255,0), CV_FILLED );

                    cv::line( chessboard_reprojected, pt_corner, pt_reprojected, cv::Scalar(255,255,255), 2 );

                    diff_x = u - corners_tmp[ corner_index ].x;
                    diff_y = v - corners_tmp[ corner_index ].y;
                }

                double euclidean_distance = sqrt( pow(diff_x, 2.0) + pow(diff_y, 2.0) );
                ///if( i == 0 ) 
                //    std::cout << "euclidean_distance image " << i << ": " << euclidean_distance << std::endl;

                temp_err_mean += euclidean_distance;  // To compute the arithmetic mean of each image
                temp_corners_err_dist.push_back( euclidean_distance ); // To compute the standard deviation of each image
            }

            temp_err_mean /= reprojected_p_c->size(); // Mean of the image
            images_error_mean.push_back( temp_err_mean );
            total_rep_err_mean += temp_err_mean;      // To compute the total mean

            for(int z=0; z<temp_corners_err_dist.size(); ++z)
                temp_err_dev += pow( temp_corners_err_dist[z] - temp_err_mean, 2.0 ); 

            temp_err_dev /= temp_corners_err_dist.size();
            temp_err_dev = sqrt( temp_err_dev );      // Standard deviation of one image

            images_error_dev.push_back( temp_err_dev ); 
            
            std::cout << "Image " << i << " corners error mean: " << temp_err_mean << std::endl;
            //std::cout << "Image " << i << " corners standard deviation: " << temp_err_dev << std::endl;

            // Save the drawn image
            std::string path = "/home/andrea/workspace/ros/catkin/src/test_hand_eye/img_reprojected/Img" 
                               + std::to_string(i) + ".png";
            cv::imwrite( path, chessboard_reprojected );
        }

        total_rep_err_mean /= images_error_mean.size(); // Total mean
        std::cout << " -- Reprojection error mean: " << total_rep_err_mean << " [pxs]" << std::endl; 

        double total_rep_err_dev = 0; 
        std::vector<double> images_error_mean_copy( images_error_mean ); // Copy constructor images_error_mean vector

        for( int i=0; i<images_error_mean.size(); ++i )
        {
            int index_img_min_error = findMin( images_error_mean_copy ); // To find the image with smallest error
            images_error_order_2D.push_back( index_img_min_error );      // And order the images for increasing error

            total_rep_err_dev += pow( images_error_mean[i] - total_rep_err_mean, 2.0 ); // To compute the total st. dev.
        }

        total_rep_err_dev /= images_error_mean.size(); 
        total_rep_err_dev = sqrt( total_rep_err_dev ); // Total standard deviation
        std::cout << " -- Reprojection standard deviation: " << total_rep_err_dev << std::endl << std::endl; 

        return total_rep_err_mean;
    }

    /**
     * @brief returnReprojectionError3D computes the error value of the projection of the corners respect
              the scene frame, and later orders the images by increasing error
     * @param[in]  w_e_visp: vector of BASE->TCP matrices
     * @param[in]  eMc_visp: calibration Hand-Eye matrix
     * @param[in]  c_o_visp: vector of CAM->CHESSBOARD matrices
     * @param[out] images_error_order_3D: output vector of the ordered images for increasing error
     * @param[in]  cols, rows: number of rows and columns of corners
     * @param[out] source: output PointCloud of the original corners
     * @param[out] transformed: output PointCloud of the projected corners
     * @param[in]  L: side of a square of the chessboard in [mm]
     * @return     rep_err, the error computed in mm
     */
    double HandEyeUtil::returnReprojectionError3D( std::vector<vpHomogeneousMatrix>& w_e_visp,
                                                   vpHomogeneousMatrix& eMc_visp,
                                                   std::vector<vpHomogeneousMatrix>& c_o_visp,
                                                   std::vector<int>& images_error_order_3D,
                                                   const int cols, const int rows, 
                                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr& source,
                                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr& transformed,
                                                   const double L )
    {
        std::vector<double> images_error_mean, images_error_dev; // Arithmetic mean and standard deviation
        double total_rep_err_mean = 0; // Value to return

        Eigen::Matrix4f eMc;
        // eMc found by IT+Robotics (for the dataset 201603_Calibration)
        //eMc << -0.013530614604462248, -0.998882606036236, 0.045282025426026148, 27.595201103361649,
        //        0.9998981223650425, -0.013310697260654026, 0.0051546318295026391, -3.2793921965400368,
        //       -0.0045461367432158544, 0.045347157537082551, 0.99896094415348347, 153.1841738801009,
        //        0, 0, 0, 1; 
        
        // Our eMc
        eMc << eMc_visp[0][0], eMc_visp[0][1], eMc_visp[0][2], eMc_visp[0][3],
               eMc_visp[1][0], eMc_visp[1][1], eMc_visp[1][2], eMc_visp[1][3],
               eMc_visp[2][0], eMc_visp[2][1], eMc_visp[2][2], eMc_visp[2][3],
               eMc_visp[3][0], eMc_visp[3][1], eMc_visp[3][2], eMc_visp[3][3];

        // 180 degrees rotation and translation of [ L*cols, L*rows, 0]
        Eigen::Matrix4f correction_chessboard_m;
        correction_chessboard_m << -1, 0, 0, L * cols,
                                    0, -1, 0, L * rows,
                                    0, 0, 1, 0,
                                    0, 0, 0, 1; 
        
        std::cout << " -- returnReprojectionError3D -- " << std::endl;

        for( int i=0; i<w_e_visp.size(); ++i ) 
        {
            // Conversion of the world -> end-effector matrix from ViSP to Eigen
            vpHomogeneousMatrix w_e_matrix = w_e_visp[i];
            Eigen::Matrix4f w_e;
            w_e << w_e_matrix[0][0], w_e_matrix[0][1], w_e_matrix[0][2], w_e_matrix[0][3],
                   w_e_matrix[1][0], w_e_matrix[1][1], w_e_matrix[1][2], w_e_matrix[1][3],
                   w_e_matrix[2][0], w_e_matrix[2][1], w_e_matrix[2][2], w_e_matrix[2][3],
                   w_e_matrix[3][0], w_e_matrix[3][1], w_e_matrix[3][2], w_e_matrix[3][3];
            //if(i == 0)
            //    std::cout << "w_e: " << std::endl << w_e << std::endl << std::endl;

            // Conversion of the camera -> object matrix from ViSP to Eigen
            vpHomogeneousMatrix c_o_matrix = c_o_visp[i];
            Eigen::Matrix4f c_o;
            c_o << c_o_matrix[0][0], c_o_matrix[0][1], c_o_matrix[0][2], c_o_matrix[0][3],
                   c_o_matrix[1][0], c_o_matrix[1][1], c_o_matrix[1][2], c_o_matrix[1][3],
                   c_o_matrix[2][0], c_o_matrix[2][1], c_o_matrix[2][2], c_o_matrix[2][3],
                   c_o_matrix[3][0], c_o_matrix[3][1], c_o_matrix[3][2], c_o_matrix[3][3];
            //if(i == 0)
            //    std::cout << "c_o: " << std::endl << c_o << std::endl << std::endl;

            Eigen::Matrix4f projection_matrix = w_e * eMc * c_o * correction_chessboard_m;
            /*
            Eigen::Matrix4f projection_matrix;
            projection_matrix << 1, 0, 0, 0,
                                 0, 1, 0, 0,
                                 0, 0, 1, 0,
                                 0, 0, 0, 1; 
            */
            if(i == 0)
                std::cout << "projection_matrix: " << std::endl << projection_matrix << std::endl << std::endl;

            pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_p_c( new pcl::PointCloud<pcl::PointXYZRGB> );
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr reprojected_p_c( new pcl::PointCloud<pcl::PointXYZRGB> );
            
            int count = 0;

            for( int x=0; x<cols; ++x )  //cols <- height-1 <- 11-1(10)
            {
                for( int y=0; y<rows; ++y ) //rows <- width-1 <- 9-1(8)
                {
                    // PointCloud with the corners in the 2D plane xy
                    pcl::PointXYZRGB point_source;
                    point_source.x = L + x * L;
                    point_source.y = L + y * L;
                    point_source.z = 0;

                    // To verify the projection orientation 
                    //if( count == 0 || count == 1 || count == 2 || count == 8 || count == 10 )
                    //    continue;
                    
                    source_p_c->push_back( point_source );

                    count++;
                }
            }

            // PCL function that transform the source PointCloud according to the projection matrix
            pcl::transformPointCloud( *source_p_c, *reprojected_p_c, projection_matrix );
            
            std::vector<double> temp_corners_err_dist;
            double temp_err_mean = 0, temp_err_dev = 0;
            
            for(int z=0; z<source_p_c->size(); ++z)
            {
                pcl::PointXYZRGB point_source;
                point_source.x = source_p_c->points[z].x;
                point_source.y = source_p_c->points[z].y;
                point_source.z = source_p_c->points[z].z;

                pcl::PointXYZRGB point_reprojected;
                point_reprojected.x = reprojected_p_c->points[z].x;
                point_reprojected.y = reprojected_p_c->points[z].y;
                point_reprojected.z = reprojected_p_c->points[z].z;

                // Output to show one of the result of the projection
                if( i == 0 )
                {
                    source->push_back( point_source );
                    transformed->push_back( point_reprojected );
                }
                
                // To find the Euclidean distance between every original point and the corresponding projected
                double diff_x = point_source.x - point_reprojected.x;
                double diff_y = point_source.y - point_reprojected.y;
                double diff_z = point_source.z - point_reprojected.z;
                double euclidean_distance = sqrt( pow(diff_x, 2.0) + pow(diff_y, 2.0) + pow(diff_z, 2.0) );
                //if( i == 0 ) 
                //    std::cout << "euclidean_distance, img " << i << ": " << euclidean_distance << std::endl;
                
                temp_err_mean += euclidean_distance;  // To compute the arithmetic mean of each image
                temp_corners_err_dist.push_back( euclidean_distance ); // To compute the standard deviation of each image
            }

            temp_err_mean /= source_p_c->size(); // Mean of the image
            images_error_mean.push_back( temp_err_mean );
            total_rep_err_mean += temp_err_mean;      // To compute the total mean

            for(int z=0; z<temp_corners_err_dist.size(); ++z)
                temp_err_dev += pow( temp_corners_err_dist[z] - temp_err_mean, 2.0 ); 

            temp_err_dev /= temp_corners_err_dist.size();
            temp_err_dev = sqrt( temp_err_dev );      // Standard deviation of one image

            images_error_dev.push_back( temp_err_dev ); 
            
            std::cout << "Image " << i << " corners error mean: " << temp_err_mean << std::endl;
            //std::cout << "Image " << i << " corners standard deviation: " << temp_err_dev << std::endl;
        }   

        total_rep_err_mean /= images_error_mean.size(); // Total mean
        std::cout << " -- Reprojection error mean: " << total_rep_err_mean << " [mm]" << std::endl; 

        double total_rep_err_dev = 0; 
        std::vector<double> images_error_mean_copy( images_error_mean ); // Copy constructor images_error_mean vector

        for( int i=0; i<images_error_mean.size(); ++i )
        {
            int index_img_min_error = findMin( images_error_mean_copy ); // To find the image with smallest error
            images_error_order_3D.push_back( index_img_min_error );      // And order the images for increasing error

            total_rep_err_dev += pow( images_error_mean[i] - total_rep_err_mean, 2.0 ); // To compute the total st. dev.
        }

        total_rep_err_dev /= images_error_mean.size(); 
        total_rep_err_dev = sqrt( total_rep_err_dev ); // Total standard deviation
        std::cout << " -- Reprojection standard deviation: " << total_rep_err_dev << std::endl << std::endl; 

        return total_rep_err_mean;
    }

    /**
     * @brief calculateErrors is the original function that computes the calibration error using the standard deviation
     * @param[in]  w_e: vector of BASE->TCP matrices
     * @param[in]  eMc: calibration Hand-Eye matrix
     * @param[in]  c_o: vector of CAM->CHESSBOARD matrices
     * @return     tmp: vector of length 4 (tmp[0] -> translation error, tmp[1]-tmp[2]-tmp[3] -> x-y-z rotation error)
     */
    std::vector<double> HandEyeUtil::calculateErrors( vpHomogeneousMatrix& eMc, 
                                                      std::vector<vpHomogeneousMatrix>& c_o, 
                                                      std::vector<vpHomogeneousMatrix>& w_e )
    {
        // Construct a vector of 6 dimension pose vector (3 translations and 3 rotations)
        std::vector<vpPoseVector> poses(c_o.size());
        double mx = 0;
        double my = 0;
        double mz = 0;
        double mrx = 0;
        double mry = 0;
        double mrz = 0;

        // Initialize the pose for each acquisition and calculate the mean for each value of the pose
        for( int i=0; i<poses.size(); ++i )
        {
            vpPoseVector p(w_e[i]*eMc*c_o[i]);
            //std::cout << "vpPoseVector: " << std::endl << p << std::endl;

            poses[i] = p;
            mx += p[0];
            my += p[1];
            mz += p[2];
            mrx += p[3];
            mry += p[4];
            mrz += p[5];
        }
        mx /= poses.size();
        my /= poses.size();
        mz /= poses.size();
        mrx /= poses.size();
        mry /= poses.size();
        mrz /= poses.size();

        // Translation vector + euclidean angles of the mean
        vpTranslationVector t(mx,my,mz);
        double rx = mrx*180 / 3.1415; 
        double ry = mry*180 / 3.1415;
        double rz = mrz*180 / 3.1415;

        std::vector<double> t_dist( poses.size() ); // Translation distance
        std::vector<std::vector<double> > r_diff( poses.size() ); // Rotation difference

        for( int i=0; i<poses.size(); ++i )
        {
            vpTranslationVector T;
            vpQuaternionVector Q;
            poses[i].extract(T);
            poses[i].extract(Q);

            // Euclidean distance between the object and the mean
            double X = pow(T[0] - t[0], 2);
            double Y = pow(T[1] - t[1], 2);
            double Z = pow(T[2] - t[2], 2);
            t_dist[i] = sqrt(X+Y+Z);

            // Euclidean angles of one reprojection of the object
            double RX = Q.x() / sqrt(1- Q.w()*Q.w());
            double RY = Q.y() / sqrt(1- Q.w()*Q.w());
            double RZ = Q.z() / sqrt(1- Q.w()*Q.w());

            std::vector<double> v(3);
            v[0] = RX-rx;
            v[1] = RY-ry;
            v[2] = RZ-rz;
            r_diff[i] = v;
        }

        // Error calculation, using the standard deviation, for the translation
        double dist_err = 0;
        double EX2 = 0; // Expected value of x^2, i.e the average of r_dist[i]^2 for every i;
        double EX = 0;  // Expected value of x, i.e the averege of r_dist
        
        for( int i=0; i<t_dist.size(); ++i )
        {
          double x = t_dist[i];
          EX += x; 
          EX2 += x*x;
        }

        EX /= t_dist.size();    
        EX2 /= t_dist.size();  

        dist_err = sqrt(EX2 - EX*EX); // See wikipedia at https://en.wikipedia.org/wiki/Standard_deviation for more info
        std::cout << " -- calculateErrors -- \nTranslation error:\n" << dist_err << std::endl;

        // Error calculation, using like before the standard deviation, for the three values of the rotation
        std::vector <double> Ex(3);
        std::vector <double> Ex2(3);

        for( int i=0; i<poses.size(); ++i )
        {
            Ex[0] += r_diff[i][0];
            Ex[1] += r_diff[i][1];
            Ex[2] += r_diff[i][2];

            Ex2[0] += r_diff[i][0]*r_diff[i][0];
            Ex2[1] += r_diff[i][1]*r_diff[i][1];
            Ex2[2] += r_diff[i][2]*r_diff[i][2];
        }
        Ex[0] /= r_diff.size();
        Ex[1] /= r_diff.size();
        Ex[2] /= r_diff.size();
        Ex2[0] /= r_diff.size();
        Ex2[1] /= r_diff.size();
        Ex2[2] /= r_diff.size();

        std::vector<double> tmp(4);
        tmp.push_back(dist_err);

        std::cout << "Rotation errors (deg, x y z):" << std::endl;
        for( int i=0; i<3; ++i )
        {
            double rot_err = sqrt( Ex2[i]- Ex[i]*Ex[i] );
            tmp.push_back(rot_err);
            std::cout << rot_err << std::endl;
        }
        std::cout << std::endl;

        return tmp;
    }

    /**
     * @brief selectImagesN isn't used, it selects the image_selected_number images with lower error, 
              valued by means of the Euclidean distance from the average quaternion and the average translation vector 
              (obtained from the product c_o_matrix * eMc_matrix * w_e_matrix)
     * @param[in]  w_e: vector of BASE->TCP matrices
     * @param[in]  eMc: calibration Hand-Eye matrix (calculated with all the images of the dataset)
     * @param[in]  c_o: vector of CAM->CHESSBOARD matrices
     * @param[in]  image_selected_number: number of the selected images
     * @return vector with the indeces of the selected images 
     */
    /*
    std::vector<int> HandEyeUtil::selectImagesN( std::vector<vpHomogeneousMatrix>& w_e,
                                				 vpHomogeneousMatrix& eMc,               
                                				 std::vector<vpHomogeneousMatrix>& c_o, 
                                				 int image_selected_number )
    {
        std::vector<Eigen::Quaterniond> vec_quat( w_e.size() );
        std::vector<Eigen::Vector3d> vec_transl( w_e.size() );
        std::vector<double> weights( w_e.size() );

        vpHomogeneousMatrix eMc_matrix = eMc;

        // Obtain the hand-eye quaternion and translation vector 
        for( int i=0; i<w_e.size(); ++i )
        {
            // CAM->CHESSBOARD[i] * TCP->CAM * BASE->TCP[i]
            vpHomogeneousMatrix c_o_matrix = c_o[i];
            vpHomogeneousMatrix w_e_matrix = w_e[i];
            vpHomogeneousMatrix baseToChess_v = c_o_matrix * eMc_matrix * w_e_matrix;

            // Quaternion
            Eigen::Matrix3d baseToChess_rotat;
            baseToChess_rotat << baseToChess_v[0][0], baseToChess_v[0][1], baseToChess_v[0][2], 
                                 baseToChess_v[1][0], baseToChess_v[1][1], baseToChess_v[1][2], 
                                 baseToChess_v[2][0], baseToChess_v[2][1], baseToChess_v[2][2];
            Eigen::Quaterniond quat = matrixIntoQuaternion( baseToChess_rotat );

            // Translation vector   
            Eigen::Vector3d transl( baseToChess_v[0][3], baseToChess_v[1][3], baseToChess_v[2][3] );

            // Save the vector of quaternions and the vector of translation vectors, every element with the same weight
            vec_quat[i] = quat;
            vec_transl[i] = transl;
            weights[i] = 1; 
        }    

        // Obtain the average quaternion and the average translation vector
        Eigen::Quaterniond rif_quat = averageQuaternion( vec_quat, weights );
        Eigen::Vector3d rif_transl = averageVector( vec_transl, weights );
        //std::cout << "rif_quat: " << std::endl << rif_quat.vec() << std::endl << rif_quat.w() << std::endl;
        //std::cout << "rif_transl: " << std::endl << rif_transl << std::endl;

        std::vector<double> t_dist( w_e.size() ); // Translation distance
        std::vector<double> r_diff( w_e.size() ); // Rotation difference

        for( int j=0; j<w_e.size(); ++j )
        {
            // Get j-th quaternion and the j-th translation vector
            Eigen::Quaterniond quat = vec_quat[j];
            Eigen::Vector3d trasl = vec_transl[j];

            // Euclidean distance between the rif_quat and the j-th quaternion
            double X = pow( rif_quat.x() - quat.x(), 2 );
            double Y = pow( rif_quat.y() - quat.y(), 2 );
            double Z = pow( rif_quat.z() - quat.z(), 2 );
            double W = pow( rif_quat.w() - quat.w(), 2 );
            r_diff[j] = sqrt( X + Y + Z + W );

            // Euclidean distance between the rif_trasl and the j-th translation vector
            X = pow( rif_transl[0] - trasl[0], 2 );
            Y = pow( rif_transl[1] - trasl[1], 2 );
            Z = pow( rif_transl[2] - trasl[2], 2 );
            t_dist[j] = sqrt( X + Y + Z );
            //std::cout << " - " << j << " - r_diff | t_dist: " << r_diff[j] << " " << t_dist[j]  << " " << std::endl;
        }

        double r_diff_sum_original = 0, t_dist_sum_original = 0;
        double r_diff_average_original = 0, t_dist_average_original = 0;

        // Calculation of the average rotation/translation difference between all the images
        // and the average quaternion/translation vector 
        for( int z=0; z<w_e.size(); ++z )
        {
        	r_diff_sum_original += r_diff[z];
        	t_dist_sum_original += t_dist[z];
        }

        r_diff_average_original = r_diff_sum_original / image_selected_number;
        t_dist_average_original = t_dist_sum_original / image_selected_number;

        std::vector<int> lower_error( image_selected_number );
        lower_error = imagesMinorError( r_diff, t_dist, image_selected_number );

        double r_diff_sum = 0, t_dist_sum = 0;
        double r_diff_average = 0, t_dist_average = 0;

        // Calculation of the average rotation/translation difference between the #image_selected_number
        // selected images and the average quaternion/translation vector 
        for( int z=0; z<image_selected_number; ++z )
        {
        	r_diff_sum += r_diff[ lower_error[z] ];
        	t_dist_sum += t_dist[ lower_error[z] ];
        }

        r_diff_average = r_diff_sum / image_selected_number;
        t_dist_average = t_dist_sum / image_selected_number;

        std::cout << "Average rotation difference (with all the images): " << r_diff_average_original << std::endl;
        std::cout << "Average rotation difference (with the N selected images): " << r_diff_average << std::endl;
        std::cout << "Average translation difference (with all the images): " << t_dist_average_original << std::endl;
        std::cout << "Average translation difference (with the N selected images): " << t_dist_average << std::endl;

        return lower_error;
    }
    */

    /**
     * @brief cameraCalibration of the camera with the pictures given to obtain the intrinsic parameters
     * @param[in] img_path: path of the folder with the images        
     * @param[in] img_extension: extension of the images
     * @param[in] img_number: number of images
     * @param[in] square_size: size of the square in mm
     * @param[in] width: number of squares in one row
     * @param[in] height: number of squares in one column
     * @param[out] focal_x, focal_y: camera focal lengths
     * @param[out] c_x, c_y: optical centers expressed in pixels coordinates
     */
    void HandEyeUtil::cameraCalibration( std::string img_path, 
                                         std::string img_extension, 
                                         int img_number, float square_size, int width, int height, 
                                         float& focal_x, float& focal_y, float& c_x, float& c_y )
    {
        int numCornersHor = width;   // Enclosed corners horizontally on the chessboard
        int numCornersVer = height;  // Enclosed corners vertically on the chessboard
        int numSquares = width * height; 

        cv::Size board_sz = cv::Size( numCornersHor, numCornersVer);
        std::vector<std::vector<cv::Point3f>> object_points; 
        std::vector<std::vector<cv::Point2f>> image_points; 
        std::vector<cv::Point3f> obj; 
        std::vector<cv::Point2f> corners;

        cv::Mat gray_image;
        
        for (int y=0; y<numCornersVer; y++) 
        {
            for(int x=0; x<numCornersHor; x++) 
            {
                obj.push_back(cv::Point3f(y * square_size, x * square_size, 0.0f));
            }
        }

        cv::namedWindow("Gray_image");

        for( int count=0; count<img_number; count++)
        {
            // Obtain the name of the image
            std::string number;
            if (count < 10){
                std::string tmp = static_cast<std::ostringstream*>( &(std::ostringstream() << count) )->str();
                number = "00"+tmp;
            }
            else if (count >= 10 && count < 100){
                std::string tmp = static_cast<std::ostringstream*>( &(std::ostringstream() << count) )->str();
                number = "0" + tmp;
            }
            else
                number = static_cast<std::ostringstream*>( &(std::ostringstream() << count) )->str();;

            gray_image = cv::imread( img_path+number+img_extension, CV_LOAD_IMAGE_GRAYSCALE );

            // Corners acquisition
            bool found = cv::findChessboardCorners( gray_image, board_sz, corners, CV_CALIB_CB_ADAPTIVE_THRESH );
            //bool found = cv::findChessboardCorners( gray_image, board_sz, corners, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE );

            if(found) 
            { 
                cv::cornerSubPix( gray_image, corners, cv::Size(11, 11), cv::Size(-1, -1), cv::TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1) ); 
                cv::drawChessboardCorners( gray_image, board_sz, corners, found ); 
            } 

            cv::imshow("Gray_image", gray_image);
            int key = cv::waitKey(100);

            //int key = cv::waitKey(0);
            //if( key == 27 ) return; 

            if( found ) 
            { 
                image_points.push_back(corners); 
                object_points.push_back(obj); 
            }
        }

        cv::destroyWindow("Gray_image");

        cv::Mat intrinsic;
        cv::Mat distCoeffs; 
        std::vector<cv::Mat> rvecs; 
        std::vector<cv::Mat> tvecs;

        std::cout << "Start calibrateCamera." << std::endl;  

        cv::calibrateCamera( object_points, image_points, gray_image.size(), intrinsic, distCoeffs, rvecs, tvecs );

        std::cout << "End calibrateCamera." << std::endl; 

        // Return the interest values
        focal_x = (float) intrinsic.at<double>(0,0);
        focal_y = (float) intrinsic.at<double>(1,1);
        c_x = (float) intrinsic.at<double>(0,2);
        c_y = (float) intrinsic.at<double>(1,2);

        std::cout << "Intrinsic matrix: " << std::endl << intrinsic << std::endl;  
        std::cout << "Distortion coefficients: " << std::endl << distCoeffs << std::endl;  
        std::cout << "Camera focal lengths (focal_x, focal_y): (" << focal_x << ", " << focal_y << ")" << std::endl;  
        std::cout << "Optical centers (c_x, c_y): (" << c_x << ", " << c_y << ")" << std::endl;  
    }

    /**
     * @brief Visualization of the results
     * @param[in]  w_e: vector of BASE->TCP matrices
     * @param[in]  eMc: calibration Hand-Eye matrix (calculated with all the images of the dataset)
     * @param[in]  c_o: vector of CAM->CHESSBOARD matrices
     */    
    void HandEyeUtil::stampResult( std::vector<vpHomogeneousMatrix>& w_e,
                                   vpHomogeneousMatrix& eMc,               
                                   std::vector<vpHomogeneousMatrix>& c_o )
    {
        std::cout << "Hand-eye camera matrix:\n" << eMc << std::endl;

        vpQuaternionVector q;
        eMc.extract(q);
        vpTranslationVector t;
        eMc.extract(t);
        vpRotationMatrix RR(q);
        std::cout << "\nTranslation:\n" << t << "\nRotation:\n" << q << "\n" << std::endl;

        //Eigen::Matrix3d e_RR;
        //e_RR << RR[0][0], RR[0][1], RR[0][2], 
        //        RR[1][0], RR[1][1], RR[1][2],
        //        RR[2][0], RR[2][1], RR[2][2]; 
        //std::cout << "e_RR: " << std::endl << e_RR << std::endl << std::endl;

        //Eigen::Vector3d e_Angles_xyz = e_RR.eulerAngles(0, 1, 2); // 0->x, 1->y, 2->z
        //std::cout << "e_Angles_xyz: " << std::endl << e_Angles_xyz << std::endl << std::endl;

        //Eigen::Vector3d e_Angles_zyz = e_RR.eulerAngles(2, 1, 2); // 2->z, 1->y, 2->z
        //std::cout << "e_Angles_zyz: " << std::endl << e_Angles_zyz << std::endl << std::endl;
        
        std::vector<double> calculateErrors_result(4);
        calculateErrors_result = calculateErrors( eMc, c_o, w_e );
    }

} // namespace test_hand_eye