/**
 *   AutoAcquisitionUtil.cpp
 *
 *   Last edit: 04/04/2017
 *   Author: Andrea Gobbi
**/

#include "test_hand_eye/AutoAcquisitionUtil.h"

namespace test_hand_eye
{
	/**
	 * @brief loadPoseFromFile reads a file with a pose and puts the values in a matrix 
	 * @param[in]  filename: path of the file
	 * @param[out] motion_matrix: output matrix 
	 * @return     the success of the operation
	 */
	bool AutoAcquisitionUtil::loadPoseFromFile( const std::string& filename, 
	                       						Eigen::MatrixXd& motion_matrix ) // 6 rows and 1 column
	{
	    std::ifstream file(filename);            // Try to open the position file
	    if(!file.is_open())
	        return false;
	  
	    std::string line;
	    int row = 0;
	    while(std::getline(file, line))
	    {
	        std::stringstream line_stream(line);
	        float element;
	        while(line_stream >> element)
	        {
	            //std::cout << element << std::endl;
	            motion_matrix(row, 0) = element; // Write the read value in the matrix
	        }
	        
	        row++;
	    }

	    file.close();
	    std::cout << "Motion matrix:" << std::endl << motion_matrix << std::endl << std::endl;

	    return true;
	}

	/**
	 * @brief getTransformMatrix, given the translation and the rotation vectors, return the rototranslation matrix
	 * @param[in]  transl: translation vector (x, y, z) [m]
	 * @param[in]  rot_degree: rotation vector (rot_x, rot_y, rot_z) [rad]
	 * @return     tf_matrix, the rototranslation matrix
	 */
	tf::Transform AutoAcquisitionUtil::getTransformMatrix( const tf::Vector3& transl, 
	                                  	             	   const tf::Vector3& rot )
	{
	    double roll = rot.getX();
	    double pitch = rot.getY();
	    double yaw = rot.getZ();

	    tf::Matrix3x3 rot_matrix;
	    rot_matrix.setRPY( roll, pitch, yaw ); // Get the matrix represented as roll pitch and yaw about fixed axes XYZ
	    //std::cout << "rpy: " << roll << " " << pitch << " " << yaw << std::endl;

	    tf::Transform tf_matrix;
	    tf_matrix.setBasis( rot_matrix );      // Set the rotational element by Matrix3x3
	    tf_matrix.setOrigin( transl );         // Set the translational element

	    Eigen::Matrix4d stamp;
	    stamp << rot_matrix[0][0], rot_matrix[0][1], rot_matrix[0][2], transl.getX(), 
	             rot_matrix[1][0], rot_matrix[1][1], rot_matrix[1][2], transl.getY(), 
	             rot_matrix[2][0], rot_matrix[2][1], rot_matrix[2][2], transl.getZ(), 
	             0, 0, 0, 1;
	    //std::cout << "Transform matrix: \n" << stamp << std::endl;  

	    return tf_matrix;
	}

	/**
	 * @brief getTranslAndRotRPYVectors, given the rototranslation matrix, 
	 *        return the translation and the rotation vectors
	 * @param[in]   tf_matrix: rototranslation matrix
	 * @param[out]  transl: translation vector (x, y, z) [m]
	 * @return      rot_degree, rotation vector (rot_x, rot_y, rot_z) [rad]
	 */
	tf::Vector3 AutoAcquisitionUtil::getTranslAndRotRPYVectors( const tf::Transform& tf_matrix, 
	                                       						tf::Vector3& transl )
	{
	    transl = tf_matrix.getOrigin(); // Return the origin vector translation

	    tf::Matrix3x3 rot_matrix;
	    rot_matrix = tf_matrix.getBasis(); // Return the basis matrix for the rotation

	    double roll, pitch, yaw; // Get the matrix represented as roll pitch and yaw about fixed axes XYZ 
	    rot_matrix.getRPY( roll, pitch, yaw );
	    //std::cout << "rot_matrix -> roll, pitch, yaw: " << roll << " " << pitch << " " << yaw << std::endl;

	    tf::Vector3 rot_degree( roll, pitch, yaw );

	    return rot_degree;
	}

	/** 
	 * @brief planJointMotionToGoal plans to a pose goal for the joints 
	          of the robot and, if the planning is successful, performs the movement
	 * @param[in]  group: MoveGroup object that performs the movement
	 * @param[in]  joint_values: vector of the joint desired final values 
	 * @param[in]  time: time for the function sleep
	 * @return     success, the success of the operation
	 */
	bool AutoAcquisitionUtil::planJointMotionToGoal( moveit::planning_interface::MoveGroup& group, 
	                            					 const std::vector<double>& joint_values, 
	                            					 const double time )
	{
	    // Planning to a Joint goal for group to a desired joint pose for the robot
	    group.setJointValueTarget(joint_values);

	    group.setPlanningTime(5.0);
	    group.setMaxVelocityScalingFactor(0.4);
	    moveit::planning_interface::MoveGroup::Plan plan;
	    bool success = group.plan(plan);
	    /*
	    group.setPlannerId("RRTConnectkConfigDefault");
	    group.setPlanningTime(0.5);
	    group.setMaxVelocityScalingFactor(0.1);

	    int attempt_number = 100, attempt_fail_number = 0;
	    double time_sum = 0.0;
	    bool success = false;
	    for(int i=0; i<attempt_number && !success; ++i)
	    {
	        moveit::planning_interface::MoveGroup::Plan plan;
	        success = group.plan(plan);

	        if(success)
	            time_sum += plan.planning_time_;
	        else
	            attempt_fail_number++;
	    }
	    */
	    group.move();

	    sleep(time);

	    return success;
	}

    /**
     * @brief computePose return the camera -> object matrix obtained with the computePose function of ViSP
     * @param[in]  point: vector of reference corner coordinates in the chessboard  
     * @param[in]  corners: vector of found corners (using the OpenCV function) in the image 
     * @param[in]  cam: camera intrinsic parameters
     * @param[in]  init: always true
     * @param[out] cMo: CAM->CHESSBOARD (camera -> object) matrix
     */
    void AutoAcquisitionUtil::computePose( std::vector<vpPoint> &point, 
		                                   const std::vector<cv::Point2f> &corners,
		                                   const vpCameraParameters &cam, 
		                                   bool init, 
		                                   vpHomogeneousMatrix &cMo )
    {
        vpPose pose;
        double x=0, y=0;
        for( int i=0; i<point.size(); ++i )
        {
            //The function finds the x and y coordinates in [m] of the corners in the image
            vpPixelMeterConversion::convertPoint( cam, vpImagePoint(corners[i].y, corners[i].x), x, y );

            point[i].set_x(x); //Update the vpPoint before using the computePose function
            point[i].set_y(y);
            pose.addPoint(point[i]);
        }

        // LAGRANGE -> Lagrange approach (test is done to switch between planar and non planar algorithm)
        // DEMENTHON -> Dementhon approach (test is done to switch between planar and non planar algorithm)
        // VIRTUAL_VS -> Virtual visual servoing approach
        // DEMENTHON_VIRTUAL_VS -> Virtual visual servoing approach initialized using Dementhon approach
        // LAGRANGE_VIRTUAL_VS -> Virtual visual servoing initialized using Lagrange approach 
        
        if( init == true ) 
        {
            vpHomogeneousMatrix cMo_dem;
            vpHomogeneousMatrix cMo_lag;
            pose.computePose( vpPose::DEMENTHON, cMo_dem ); //for a linear approach
            pose.computePose( vpPose::LAGRANGE, cMo_lag );  //for a non linear approach

            double residual_dem = pose.computeResidual( cMo_dem );
            double residual_lag = pose.computeResidual( cMo_lag );
            
            if( residual_dem < residual_lag )
                cMo = cMo_dem;
            else
                cMo = cMo_lag;
        }
        //Compute the camera -> object matrix using the ViSP function
        pose.computePose( vpPose::VIRTUAL_VS, cMo );
    }

	/** 
	 * @brief computeMovement calls planJointMotionToGoal to perform the movement to one or more joint poses
	 * @param[in]  group: MoveGroup object that performs the movement
	 * @param[in]  motion_matrix: matrix with one or more joint poses to perform
	 * @param[in]  time: time for the function sleep
	 * @return     success, the success of the operation
	 */
	bool AutoAcquisitionUtil::computeMovement( moveit::planning_interface::MoveGroup& group, 
						                       const Eigen::MatrixXd& motion_matrix, 
						                       const double time )
	{
	    std::vector<double> joint_values;
	    group.getCurrentState()->copyJointGroupPositions(group.getCurrentState()->getRobotModel()->getJointModelGroup(group.getName()), joint_values);
	    bool success = false;

	    for( int i=0; i<motion_matrix.cols(); ++i ) // If we have more positions saved in the motion_matrix
	    {
	        //std::cout << motion_matrix.col(i) << std::endl;
	        for( int j=0; j<motion_matrix.rows(); ++j )
	            joint_values[j] = motion_matrix(j,i);

	        success = planJointMotionToGoal( group, joint_values, time ); // Perform the i-th movement
	    }

	    return success;
	}

	/**
	 * @brief planPoseMotionToGoal plans to a pose goal for the end effector of the robot and,
	          if the planning is successful, performs the movement
	 * @param[in]  group: MoveGroup object that performs the movement
	 * @param[in]  transl: translation vector (x, y, z) [m]
	 * @param[in]  quat: rotation quaternion 
	 * @param[in]  time: time for the function sleep
	 * @return     success, the success of the operation
	 */
	bool AutoAcquisitionUtil::planPoseMotionToGoal( moveit::planning_interface::MoveGroup& group, 
						                            const tf::Vector3& transl,
						                            const tf::Quaternion& quat,
						                            const double time )
	{
	    //tf::Quaternion quat = tf_matrix.getRotation(); // Return a quaternion representing the rotation
	    //tf::Vector3 transl = tf_matrix.getOrigin();    // Return the origin vector translation

	    // Planning to a Pose goal for group to a desired pose for the end effector
	    geometry_msgs::Pose target_pose;
	    target_pose.orientation.x = quat.x();
	    target_pose.orientation.y = quat.y();
	    target_pose.orientation.z = quat.z();
	    target_pose.orientation.w = quat.w();
	    target_pose.position.x = transl[0];
	    target_pose.position.y = transl[1];
	    target_pose.position.z = transl[2];
	    group.setPoseTarget(target_pose);

	    //std::cout << "Target pose:" << std::endl;
	    //std::cout << "q: " << quat.x() << " " << quat.y() << " " << quat.z() << " " << quat.w() << std::endl;
	    //std::cout << "t: " << transl[0] << " " << transl[1] << " " << transl[2] << std::endl << std::endl;

	    group.setPlanningTime(5.0);
	    group.setMaxVelocityScalingFactor(0.4);
	    //group.setMaxVelocityScalingFactor(0.1);
	    //group.setMaxAccelerationScalingFactor(0.05);
	    moveit::planning_interface::MoveGroup::Plan plan;
	    bool success = group.plan(plan);
	    /*
	    group.setPlannerId("RRTConnectkConfigDefault");
	    group.setPlanningTime(0.5);
	    group.setMaxVelocityScalingFactor(0.1);

	    int attempt_number = 100, attempt_fail_number = 0;
	    double time_sum = 0.0;
	    bool success = false;
	    for( int i=0; i<attempt_number && !success; ++i )
	    {
	        moveit::planning_interface::MoveGroup::Plan plan;
	        success = group.plan(plan);

	        if(success)
	            time_sum += plan.planning_time_;
	        else
	            attempt_fail_number++;
	    }
	    */
	    group.move();
	    
	    sleep(time);

	    return success;
	}

	/**
	 * @brief planPoseMotionToGoal plans to a pose goal for the end effector of the robot respect the camera and,
	          if the planning is successful, performs the movement
	 * @param[in]  group: MoveGroup object that performs the movement
	 * @param[in]  transl: translation vector (x, y, z) [m]
	 * @param[in]  quat: rotation quaternion 
	 * @param[in]  hand_eye: computed hand-eye
	 * @param[in]  time: time for the function sleep
	 * @return     success, the success of the operation
	 */
	bool AutoAcquisitionUtil::planPoseMotionToCameraGoal( moveit::planning_interface::MoveGroup& group, 
								                      	  const tf::Vector3& transl,
								                      	  const tf::Quaternion& quat,
								                      	  const tf::Transform& hand_eye,
								                      	  const double time )
	{
		tf::Transform chessboardToCam;
        chessboardToCam.setOrigin( transl );
        chessboardToCam.setRotation( quat );  

        tf::Transform chessboardToHand;    
        chessboardToHand.mult( chessboardToCam, hand_eye );

        bool success = planPoseMotionToGoal( group, 
                                             chessboardToHand.getOrigin(),
										     chessboardToHand.getRotation(), 
                                             time );
        
        return success;
	}

	/** 
	 * @brief saveEEPosition saves in the text file the end effector position respect to the world reference
	 * @param[in]  fout: object useful to write the line in the output file
	 * @param[in]  tf_mng: object useful to find the transform between two frames
	 * @param[in]  base_frame: base frame to find BASE->TCP matrix (wMe)
	 * @param[in]  hand_frame: hand frame to find BASE->TCP matrix (wMe)
	 * @return     
	 */
	void AutoAcquisitionUtil::saveEEPosition( std::ofstream& fout,
						                      TransformManager& tf_mng,
						                      const std::string base_frame, 
						                      const std::string hand_frame )
	{
	    vpHomogeneousMatrix wMe;
	    bool tf_found = tf_mng.get_Tf( base_frame, hand_frame, wMe, 0.05 );
	    //std::cout << "wMe: " << std::endl << wMe << std::endl << std::endl;

	    vpRotationMatrix rot_m;
	    wMe.extract( rot_m );
	    //std::cout << "rot_m: " << std::endl << rot_m << std::endl << std::endl;

	    vpRzyzVector r; 
	    r.buildFrom( rot_m );
	    //std::cout << "r: " << std::endl << r << std::endl << std::endl;

	    vpTranslationVector transl_v;
	    wMe.extract( transl_v );
	    //std::cout << "transl_v: " << std::endl << transl_v << std::endl << std::endl;
	    
	    // Position to save (t_x, t_y, t_z, r_x, r_y, r_z)
	    std::stringstream pose_temp;
	    pose_temp << transl_v[0] << "; " << transl_v[1] << "; " << transl_v[2] << "; " 
	              << r[0] << "; " << r[1] << "; " << r[2];   
	    std::string line = pose_temp.str();
	    std::cout << "Line: " << line << std::endl;

	    // Write the six values of the end-effector position in the file
	    fout << line + "\n";
	}

	/** 
	 * @brief saveEEPosition saves in the text file the end effector position respect to the world reference
	 * @param[in]  fout: object useful to write the line in the output file
	 * @param[in]  tf_mng: object useful to find the transform between two frames
	 * @param[in]  base_frame: base frame to find BASE->TCP matrix (wMe)
	 * @param[in]  hand_frame: hand frame to find BASE->TCP matrix (wMe)
	 * @param[in]  wMe: world - end effector matrix
	 * @return     
	 */
	void AutoAcquisitionUtil::saveEEPosition( std::ofstream& fout,
						                      TransformManager& tf_mng,
						                      const std::string base_frame, 
						                      const std::string hand_frame,
						                      vpHomogeneousMatrix& wMe )
	{
	    bool tf_found = tf_mng.get_Tf( base_frame, hand_frame, wMe, 0.05 );
	    //std::cout << "wMe: " << std::endl << wMe << std::endl << std::endl;

	    vpRotationMatrix rot_m;
	    wMe.extract( rot_m );
	    //std::cout << "rot_m: " << std::endl << rot_m << std::endl << std::endl;

	    vpRzyzVector r; 
	    r.buildFrom( rot_m );
	    //std::cout << "r: " << std::endl << r << std::endl << std::endl;

	    vpTranslationVector transl_v;
	    wMe.extract( transl_v );
	    //std::cout << "transl_v: " << std::endl << transl_v << std::endl << std::endl;
	    
	    // Position to save (t_x, t_y, t_z, r_x, r_y, r_z)
	    std::stringstream pose_temp;
	    pose_temp << transl_v[0] << "; " << transl_v[1] << "; " << transl_v[2] << "; " 
	              << r[0] << "; " << r[1] << "; " << r[2];   
	    std::string line = pose_temp.str();
	    std::cout << "Line: " << line << std::endl;

	    // Write the six values of the end-effector position in the file
	    fout << line + "\n";
	}

} // namespace test_hand_eye