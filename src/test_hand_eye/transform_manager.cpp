#include <test_hand_eye/transform_manager.h>
#include <tf_conversions/tf_eigen.h>
#include <visp_bridge/3dpose.h>

TransformManager::TransformManager(ros::NodeHandle &nh) :
    tf_listener_(nh)
{

}

TransformManager::~TransformManager()
{

}

bool TransformManager::get_Tf(const std::string &tf_b, const std::string &tf_o, tf::Transform &tf, const double duration)
{
    if (tf_listener_.frameExists(tf_b) && tf_listener_.frameExists(tf_o))
    {
        //std::cout<<"OK frames_exist"<<std::endl;

        if (tf_listener_.waitForTransform(tf_b, tf_o, ros::Time(0), ros::Duration(duration)))
        {
            try
            {
                tf::StampedTransform transform;
                tf_listener_.lookupTransform(tf_b, tf_o, ros::Time(0), transform);
                tf = transform;
                return true;
            }
            catch (tf::TransformException ex)
            {
                ROS_ERROR("%s",ex.what());
                return false;
            }
        }
        else
        {
            std::cout<<"Transform not found"<<std::endl;
            return false;
        }
    }
    else
    {
        //std::cout<<"Invalid frames id"<<std::endl;
        return false;
    }
}

bool TransformManager::get_Jv(const std::string &tf_b, const std::string &tf_o, Eigen::MatrixXd &Jv, const double duration)
{
    if (tf_listener_.frameExists(tf_b) && tf_listener_.frameExists(tf_o))
    {
        //std::cout<<"OK frames_exist"<<std::endl;
        if (tf_listener_.waitForTransform(tf_b, tf_o, ros::Time(0), ros::Duration(duration)))
        {
            try
            {
                tf::StampedTransform transform;
                tf_listener_.lookupTransform(tf_b, tf_o, ros::Time(0), transform);
                tf::Transform tf = transform;
                tf::Matrix3x3 R = tf.getBasis();
                tf::Vector3 t = tf.getOrigin();
                tf::Vector3 v1, v2, v3;

                t.getSkewSymmetricMatrix(&v1, &v2, &v3);
                tf::Matrix3x3 S_t(v1.getX(), v1.getY(), v1.getZ(), v2.getX(), v2.getY(), v2.getZ(), v3.getX(), v3.getY(), v3.getZ());
                tf::Matrix3x3 M = R*S_t;

                Jv.resize(6,6);
                Eigen::Matrix3d tmp = Eigen::Matrix3d::Zero();
                tf::matrixTFToEigen(R,tmp);
                Jv.block(0,0 , 3,3) << tmp;
                Jv.block(3,3 , 3,3) << tmp;
                tf::matrixTFToEigen(M,tmp);
                Jv.block(0,3 , 3,3) << tmp;
                Jv.block(3,0 , 3,3) << 0,0,0,0,0,0,0,0,0;

                return true;
            }
            catch (tf::TransformException ex)
            {
                ROS_ERROR("%s",ex.what());
                return false;
            }
        }
        else
        {
            std::cout<<"Transform not found"<<std::endl;
            return false;
        }
    }
    else
    {
        std::cout<<"Invalid frames id"<<std::endl;
        return false;
    }
}

bool TransformManager::get_q(const std::string &tf_b, const std::string &tf_o, tf::Quaternion &q, const double duration)
{
    if (tf_listener_.frameExists(tf_b) && tf_listener_.frameExists(tf_o))
    {
        //std::cout<<"OK frames_exist"<<std::endl;

        if (tf_listener_.waitForTransform(tf_b, tf_o, ros::Time(0), ros::Duration(duration)))
        {
            try
            {
                tf::StampedTransform transform;
                tf_listener_.lookupTransform(tf_b, tf_o, ros::Time(0), transform);
                tf::Transform tf = transform;
                q = tf.getRotation();
                return true;
            }
            catch (tf::TransformException ex)
            {
                ROS_ERROR("%s",ex.what());
                return false;
            }
        }
        else
        {
            std::cout<<"Transform not found"<<std::endl;
            return false;
        }
    }
    else
    {
        std::cout<<"Invalid frames id"<<std::endl;
        return false;
    }
}

bool TransformManager::get_R(const std::string &tf_b, const std::string &tf_o, tf::Matrix3x3 &R, const double duration)
{
    if (tf_listener_.frameExists(tf_b) && tf_listener_.frameExists(tf_o))
    {
        //std::cout<<"OK frames_exist"<<std::endl;

        if (tf_listener_.waitForTransform(tf_b, tf_o, ros::Time(0), ros::Duration(duration)))
        {
            try
            {
                tf::StampedTransform transform;
                tf_listener_.lookupTransform(tf_b, tf_o, ros::Time(0), transform);
                tf::Transform tf = transform;
                R = tf.getBasis();
                return true;
            }
            catch (tf::TransformException ex)
            {
                ROS_ERROR("%s",ex.what());
                return false;
            }
        }
        else
        {
            std::cout<<"Transform not found"<<std::endl;
            return false;
        }
    }
    else
    {
        std::cout<<"Invalid frames id"<<std::endl;
        return false;
    }
}

bool TransformManager::get_t(const std::string &tf_b, const std::string &tf_o, tf::Vector3 &t, const double duration)
{
    if (tf_listener_.frameExists(tf_b) && tf_listener_.frameExists(tf_o))
    {
        //std::cout<<"OK frames_exist"<<std::endl;

        if (tf_listener_.waitForTransform(tf_b, tf_o, ros::Time(0), ros::Duration(duration)))
        {
            try
            {
                tf::StampedTransform transform;
                tf_listener_.lookupTransform(tf_b, tf_o, ros::Time(0), transform);
                tf::Transform tf = transform;
                t = tf.getOrigin();
                return true;
            }
            catch (tf::TransformException ex)
            {
                ROS_ERROR("%s",ex.what());
                return false;
            }
        }
        else
        {
            std::cout<<"Transform not found"<<std::endl;
            return false;
        }
    }
    else
    {
        std::cout<<"Invalid frames id"<<std::endl;
        return false;
    }
}

bool TransformManager::get_Tf(const std::string &tf_b, const std::string &tf_o, vpHomogeneousMatrix &tf, const double duration)
{
    tf::Transform T;
    bool valid = get_Tf(tf_b,tf_o,T,duration);
    if (valid)
    {
        geometry_msgs::Transform msg;
        tf::transformTFToMsg(T,msg);
        tf = visp_bridge::toVispHomogeneousMatrix(msg);

        return true;
    }
    else
        return false;

}

bool TransformManager::get_Jv(const std::string &tf_b, const std::string &tf_o, vpVelocityTwistMatrix &Jv, const double duration)
{
    tf::Transform T;
    bool valid = get_Tf(tf_b,tf_o,T,duration);
    if (valid)
    {
        geometry_msgs::Transform msg;
        tf::transformTFToMsg(T,msg);
        vpHomogeneousMatrix tf = visp_bridge::toVispHomogeneousMatrix(msg);
        Jv.buildFrom(tf);

        return true;
    }
    else
        return false;
}

bool TransformManager::get_q(const std::string &tf_b, const std::string &tf_o, vpQuaternionVector &q, const double duration)
{
    vpRotationMatrix R;
    bool valid = get_R(tf_b,tf_o,R,duration);
    if (valid)
    {
        q.buildFrom(R);

        return true;
    }
    else
        return false;
}

bool TransformManager::get_R(const std::string &tf_b, const std::string &tf_o, vpRotationMatrix &R, const double duration)
{
    vpHomogeneousMatrix TF;
    bool valid = get_Tf(tf_b,tf_o,TF,duration);
    if (valid)
    {
        TF.extract(R);

        return true;
    }
    else
        return false;
}

bool TransformManager::get_t(const std::string &tf_b, const std::string &tf_o, vpTranslationVector &t, const double duration)
{
    tf::Vector3 trasl;
    bool valid = get_t(tf_b,tf_o,trasl,duration);
    if (valid)
    {
        t[0] = trasl[0];
        t[1] = trasl[1];
        t[2] = trasl[2];

        return true;
    }
    else
        return false;
}
