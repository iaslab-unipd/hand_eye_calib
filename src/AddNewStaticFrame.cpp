/**
 *   AddNewStaticFrame.cpp
 *
 *   Last edit: 03/04/2017
 *   Author: Andrea Gobbi
**/

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <opencv2/core/core.hpp>
 
int main(int argc, char** argv)
{
    ros::init(argc, argv, "AddNewStaticFrame_node");
    ros::NodeHandle nh;
    ROS_INFO(" --- AddNewStaticFrame node active --- ");

    std::string configuration_path = "/home/andrea/workspace/ros/catkin/src/test_hand_eye/config/Configuration_RA_FindChessboardFrame.yaml";

    cv::FileStorage fs( configuration_path, cv::FileStorage::READ );
    if( !fs.isOpened() ) 
    {
        ROS_ERROR("Error opening configuration file");
        exit(-1);
    }

    std::string path_project, config_file_name;
    std::string parent_frame, new_frame;
    std::vector<double> tf;

    fs["path_project"] >> path_project;
    fs["config_file_name"] >> config_file_name;
    fs["parent_frame"] >> parent_frame;
    fs["new_frame"] >> new_frame;

    fs.release();

    cv::FileStorage fs_tf( path_project + config_file_name, cv::FileStorage::READ );
    if( !fs_tf.isOpened() ) 
    {
        ROS_ERROR("Error opening configuration file");
        exit(-1);
    }

    fs_tf["tf_chessboard"] >> tf;

    fs_tf.release();

    // Translation vector of the frame
    double t_x = tf[3];
    double t_y = tf[7];
    double t_z = tf[11];

    tf::Matrix3x3 rot_matrix;
    rot_matrix[0][0] = tf[0]; rot_matrix[0][1] = tf[1]; rot_matrix[0][2] = tf[2];
    rot_matrix[1][0] = tf[4]; rot_matrix[1][1] = tf[5]; rot_matrix[1][2] = tf[6];
    rot_matrix[2][0] = tf[8]; rot_matrix[2][1] = tf[9]; rot_matrix[2][2] = tf[10];

    // Rotation quaternion of the frame
    tf::Quaternion q(0, 0, 0, 0);
    rot_matrix.getRotation( q );
    std::cout << "q from rot_matrix: " << q.x() << " " << q.y() << " " << q.z() << " " << q.w() << std::endl;

    tf::TransformBroadcaster br;
    tf::Transform transform;

    // Pubblication of the new frame
    ros::Rate rate(10.0);
    while( nh.ok() )
    {
        transform.setOrigin( tf::Vector3(t_x, t_y, t_z) );
        transform.setRotation( q );
        br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), parent_frame, new_frame));
        rate.sleep();
    }

    return 0;
}