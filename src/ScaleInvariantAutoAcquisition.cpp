/**
 *   ScaleInvariantAutoAcquisition.cpp
 *
 *   Last edit: 20/04/2017
 *   Author: Andrea Gobbi
**/

#include "test_hand_eye/AutoAcquisitionUtil.h"

cv::Mat leftImage; 

// Receives left camera images 
void leftImageCallback( const sensor_msgs::ImageConstPtr& msg ) 
{
    cv_bridge::CvImagePtr cv_ptr;

    try
    { 
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        leftImage = cv_ptr->image.clone();

        // Image visualization
        cv::namedWindow("Left image");
        cv::imshow("Left image", leftImage);
        cv::startWindowThread();    
    }
    catch(cv_bridge::Exception& e)
    {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "MoveRobot_node_");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ROS_INFO(" --- MoveRobot_node active --- ");

    std::string configuration_path = "/home/andrea/workspace/ros/catkin/src/test_hand_eye/config/Configuration_RA_FindChessboardFrame.yaml";
    test_hand_eye::AutoAcquisitionUtil obj_acq_util;
    TransformManager tf_mng(nh);

    cv::FileStorage fs( configuration_path, cv::FileStorage::READ );
    if( !fs.isOpened() ) 
    {
        ROS_ERROR("Error opening configuration file");
        exit(-1);
    }

    const int number_imgs_for_plane = 9;
    int num_p = 0, number_inclinations = 0; 
    double time = 0.0, d_p = 0.0, transl_x = 0.0, transl_y = 0.0;
    double t_x = 0.0, t_y = 0.0, t_z = 0.0, r_x = 0.0, r_y = 0.0, r_z = 0.0; // Start position
    double x_step = 0.0, y_step = 0.0; // X and Z step in the translation [m]
    std::vector<double> x_increment, y_increment; // To have the chessboard in the center of the image [m]
    std::vector<double> x_angles, y_angles, z_angles; // To have different orientation of the end effector [degree]
    std::string image_topic, base_frame, hand_frame, target_frame;
    std::string path_project, dir_acquisitions, dir_drawncenters, pose_file_name;

    fs["num_p"] >> num_p;
    fs["d_p"] >> d_p;
    fs["ros_time"] >> time;

    fs["t_x"] >> t_x;
    fs["t_y"] >> t_y;
    fs["t_min_p"] >> t_z;
    fs["r_x"] >> r_x;
    fs["r_y"] >> r_y;
    fs["r_z"] >> r_z;

    fs["transl_x"] >> transl_x;
    fs["transl_y"] >> transl_y;
    fs["x_step"] >> x_step;
    fs["y_step"] >> y_step;
    fs["x_increment"] >> x_increment;
    fs["y_increment"] >> y_increment;

    fs["x_angles"] >> x_angles;
    fs["y_angles"] >> y_angles;
    fs["z_angles"] >> z_angles;

    fs["image_topic"] >> image_topic;
    fs["base_frame"] >> base_frame;
    fs["hand_frame"] >> hand_frame;
    fs["new_frame"] >> target_frame; // When chessboard frame is computed
    //fs["acircle_frame"] >> target_frame;  

    fs["path_project"] >> path_project;
    fs["sub_dir_acquisitions"] >> dir_acquisitions;
    fs["sub_dir_drawncenters"] >> dir_drawncenters;
    fs["pose_file_name"] >> pose_file_name;

    fs.release();

    sleep(3);

    // Read left camera images from the topic image_raw
    image_transport::ImageTransport imgLeft( nh );
    image_transport::Subscriber subLeft = imgLeft.subscribe( image_topic, 1, leftImageCallback );    

    sleep(5);

    std::cout << "Reading configuration file" << std::endl
              << "num_p: " << num_p << ", d_p: " << d_p 
              << ", number_inclinations: " << number_inclinations << std::endl;
    std::cout << "path_project: " << path_project << std::endl;
    std::cout << "sub_dir_acquisitions: " << path_project << dir_acquisitions << std::endl; 

    std::string path_acquisitions = path_project + dir_acquisitions;
    std::string path_drawnCirclesImages = path_acquisitions + dir_drawncenters;
    
    // File where to save the end effector positions
    std::ofstream fout( path_acquisitions + pose_file_name );

    //The :move_group_interface:`MoveGroup` class can be easily setup using just the name 
    //of the group you would like to control and plan for.
    moveit::planning_interface::MoveGroup group("manipulator");
    //We will use the :planning_scene_interface:`PlanningSceneInterface` class to deal directly with the world.
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface; 
    //If you don't provide any start state, Moveit will use the current state
    group.setStartStateToCurrentState();

    // Get the tolerance that is used for reaching an orientation goal. 
    // This is the tolerance for roll, pitch and yaw, in radians. 
    double ori_tolerance = group.getGoalOrientationTolerance();
    // Get the tolerance that is used for reaching a position goal. 
    // This is be the radius of a sphere where the end-effector must reach. 
    double pose_tolerance = group.getGoalPositionTolerance();
    std::cout << "Position tolerance: " << pose_tolerance << ", Orientation tolerance: " << ori_tolerance << std::endl;
    //Set the orientation tolerance that is used for reaching the goal when moving to a pose.
    //group.setGoalOrientationTolerance(0.01);
    //ori_tolerance = group.getGoalOrientationTolerance();
    //Set the position tolerance that is used for reaching the goal when moving to a pose. 
    //group.setGoalPositionTolerance(0.001);
    //pose_tolerance = group.getGoalPositionTolerance();
    //std::cout << "Position tolerance: " << pose_tolerance << ", Orientation tolerance: " << ori_tolerance << std::endl; 

    std::string frame = group.getPoseReferenceFrame();
    std::cout << "Reference frame: " << frame << std::endl;

    if( frame != target_frame )
    {
        group.setPoseReferenceFrame(target_frame);
        frame = group.getPoseReferenceFrame();
        std::cout << "Reference frame: " << frame << std::endl;
    }

    sleep(time);

    //int attempts = 3;
    //group.setNumPlanningAttempts(attempts);

    std::vector<cv::Point2f> centers;
    //Size(points_per_row, points_per_colum) 
    cv::Size s(4, 11); // The circles aren't found with s(11, 4)
    std::vector<vpPoint> point( s.height * s.width );
    int j=s.height, k=0;
    int width_image_px = 480, height_image_px = 640;
    double L = 0.04; // [m]
    int width = 4, height = 11;
    double focal_x = 381.362, focal_y = 381.362;
    double c_x = 320.5, c_y = 240.5;

    //vpCameraParameters cam(800, 800, 415.575, 344.406);
    vpCameraParameters cam(focal_x, focal_y, c_x, c_y); 

    //std::cout << s.height << " " << s.width << std::endl;
    while( k < s.height*s.width )
    {
        if( k%s.width == 0 )
            j--;

        point[k].setWorldCoordinates( L/2*j, L/2*(j%2)+(k%s.width)*L, 0 );
        //std::cout << "-- Point" << k << " " << point[k].oP[0] << " " << point[k].oP[1] << " " << point[k].oP[2] << std::endl; 

        k++;
    }

    if( x_angles.size() == y_angles.size() && x_angles.size() == z_angles.size() && 
        x_increment.size() == y_increment.size() && x_angles.size() == x_increment.size() )
        number_inclinations = x_angles.size();
    else
        ROS_ERROR("Check that x_angles, y_angles, z_angles, x_increment and y_increment have the same size in the configuration file.");
    
    // Acquisition
    double z_temp = t_z;
    int count_image = 0;
    for( int plane = 0; plane < num_p; ++plane )
    {
        for( int angle = 0; angle < number_inclinations; ++angle )
        {
            // Copy the start position of the end effector
            double x_temp = t_x; 
            double y_temp = t_y;
            //double z_temp = t_z;
            double r_x_temp = r_x;
            double r_y_temp = r_y;
            double r_z_temp = r_z;

            x_temp += x_increment[ angle ];
            y_temp += y_increment[ angle ];

            // Change the orientation of the end effector according to the read values
            r_x_temp += x_angles[ angle ];
            r_y_temp += y_angles[ angle ];
            r_z_temp += z_angles[ angle ];

            tf::Quaternion q;
            q.setRPY( radians(r_x_temp), radians(r_y_temp), radians(r_z_temp) );   

            for(int y=0; y<number_imgs_for_plane; ++y)
            {
                switch(y)
                {
                    case 0:
                        break;
                    case 1:
                        y_temp -= transl_y;
                        break;
                    case 2:
                        x_temp -= transl_x;
                        break;
                    case 3:
                        y_temp += transl_y;
                        break;
                    case 4:
                        y_temp += transl_y;
                        break;
                    case 5:
                        x_temp += transl_x;
                        break;
                    case 6:
                        x_temp += transl_x;
                        break;
                    case 7:
                        y_temp -= transl_y;
                        break;
                    case 8:
                        y_temp -= transl_y;
                        break;
                }

                tf::Vector3 transl( x_temp, y_temp, z_temp );

                // Go to the goal position
                bool success = obj_acq_util.planPoseMotionToGoal( group, transl, q, time );
                std::cout << "Plane " << plane << ", angle " << angle << ", image " << y << ", : " << success << std::endl;
                /*
                if(!success)
                {
                    double t_x_step = x_temp, t_y_step = y_temp, t_z_step = z_temp;

                    for(int i=0; i<6; ++i)
                    {
                        if(!success)
                        {
                            switch(i)
                            {
                                case 0:
                                    t_x_step += 0.01;
                                    break;
                                case 1:
                                    t_x_step -= 0.01;
                                    break;
                                case 2:
                                    t_y_step += 0.01;
                                    break;
                                case 3:
                                    t_y_step -= 0.01;
                                    break;
                                case 4:
                                    t_z_step += 0.01;
                                    break;
                                case 5:
                                    t_z_step -= 0.01;
                                    break;
                            }

                            tf::Vector3 transl_temp( t_x_step, t_y_step, t_z_step );

                            ROS_ERROR_STREAM("Try to reach position (" << t_x_step << ", " << t_y_step << ", " << t_z_step <<   
                                             ") instead of position (" << x_temp << ", " << y_temp << ", " << z_temp << ")");
                            success = planPoseMotionToGoal( group, transl_temp, q, time );
                        }
                    }
                }
                */
                
                cv::Mat image = leftImage.clone();
                std::string image_name_temp; 

                bool found = cv::findCirclesGrid(image, s, centers, cv::CALIB_CB_ASYMMETRIC_GRID);

                if( success )
                {
                    // Give the name to the image to save
                    char num[400];
                    sprintf(num, "Img%03d.bmp", count_image);

                    std::stringstream name;
                    name << num;   
                    image_name_temp = name.str();

                    // Save the image
                    cv::imwrite( path_acquisitions + image_name_temp, image ); 
                    count_image++;

                    vpHomogeneousMatrix cMo;
                    obj_acq_util.computePose( point, centers, cam, true, cMo ); 
                    std::cout << "cMo: " << std::endl << cMo << std::endl << std::endl; 

                    // Save in the text file the end effector position respect to the world reference
                    obj_acq_util.saveEEPosition( fout, tf_mng, base_frame, hand_frame );

                    // Draw the centers in the image and then save it
                    cv::drawChessboardCorners( image, s, centers, found ); 
                    cv::imwrite( path_drawnCirclesImages + image_name_temp, image );  

                    ROS_INFO_STREAM("Image (" << image_name_temp << ") saved.");   
                }
                else
                    ROS_ERROR_STREAM("Check if the position (" << x_temp << ", " << y_temp << ", " << z_temp << ") is reachable from the end effector");       
            }
        }

        z_temp -= d_p;
    }
    
    // Come back to the start position
    tf::Vector3 transl( t_x, t_y, t_z );
    tf::Quaternion q;
    q.setRPY( radians(r_x), radians(r_y), radians(r_z) );   
    bool success = obj_acq_util.planPoseMotionToGoal( group, transl, q, time );

    if( success )
        ROS_INFO("The acquisition is completed.");

    cv::destroyWindow("Left image");
    
    return 0;
}