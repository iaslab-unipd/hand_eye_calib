#include <test_hand_eye/transform_manager.h>
#include <ros/ros.h>
#include <geometry_msgs/Transform.h>

#include <tf/tf.h>

#include <visp_bridge/3dpose.h>

#include <visp_ros/vpROSGrabber.h>
/*#include <visp_hand2eye_calibration/TransformArray.h>
#include <visp_hand2eye_calibration/reset.h>
#include <visp_hand2eye_calibration/compute_effector_camera.h>
#include <visp_hand2eye_calibration/compute_effector_camera_quick.h>*/
#include <visp/vpCalibration.h>

#include <visp/vpDisplayX.h>
#include <visp/vpImage.h>
#include <visp/vpDot2.h>
#include <visp/vpImagePoint.h>
#include <visp/vpPose.h>
#include <visp/vpImageConvert.h>
#include <visp/vpPixelMeterConversion.h>
#include <visp/vpMeterPixelConversion.h>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <math.h>

// PARAMETERS
std::string HEIGHT = "chessboard_height";
std::string WIDTH = "chessboard_width";
std::string IMAGE_TOPIC = "image";
std::string CAMERA_INFO_TOPIC = "camera_info";
std::string WORLD_FRAME = "world_frame";
std::string HAND_FRAME = "hand_frame";
std::string SIZE = "size";


void computePose(std::vector<vpPoint> &point, const std::vector<cv::Point2f> &c,
                 const vpCameraParameters &cam, bool init, vpHomogeneousMatrix &cMo)
{
    vpPose pose;
    double x=0, y=0;
    for (unsigned int i=0; i < point.size(); i ++)
    {
        vpPixelMeterConversion::convertPoint(cam, vpImagePoint(c[i].y, c[i].x), x, y);
        point[i].set_x(x);
        point[i].set_y(y);
        pose.addPoint(point[i]);
    }
    if (init == true) {
        vpHomogeneousMatrix cMo_dem;
        vpHomogeneousMatrix cMo_lag;
        pose.computePose(vpPose::DEMENTHON, cMo_dem);
        pose.computePose(vpPose::LAGRANGE, cMo_lag);
        double residual_dem = pose.computeResidual(cMo_dem);
        double residual_lag = pose.computeResidual(cMo_lag);
        if (residual_dem < residual_lag)
            cMo = cMo_dem;
        else
            cMo = cMo_lag;
    }
    pose.computePose(vpPose::VIRTUAL_VS, cMo);
}

//calculate errors and returns them in a vector, in the order dist_error, rot_x_error, rot_y_error, rot_z_error. Rot errors are in deg
std::vector<double> calculateErrors(vpHomogeneousMatrix eMc, std::vector< vpHomogeneousMatrix> c_o, std::vector< vpHomogeneousMatrix > w_e)
{
    //create a vector with a pose for each acquisition and calculate the mean for each value of the pose
    std::vector<vpPoseVector> poses(c_o.size());
    double mx = 0;
    double my =0;
    double mz = 0;
    double mrx =0;
    double mry = 0;
    double mrz = 0;
    for (int i = 0; i < poses.size(); i++)
    {
        vpPoseVector p(w_e[i]*eMc*c_o[i]);
        poses[i] = p;
        mx += p[0];
        my += p[1];
        mz += p[2];
        mrx += p[3];
        mry += p[4];
        mrz += p[5];
    }
    mx /= poses.size();
    my /= poses.size();
    mz /= poses.size();
    mrx /= poses.size();
    mry /= poses.size();
    mrz /= poses.size();

    //translation + euclidean angles of the mean
    vpTranslationVector t(mx,my,mz);
    double rx = mrx*180 / 3.1415;
    double ry = mry*180 / 3.1415;
    double rz = mrz*180 / 3.1415;

    //take the pose of the known object (the grid)
    /*tf::Transform wTo;
    tf_mng.get_Tf(base_frame, "acircle_frame" , wTo, 0.05); //  base_frame = /ur_base
    tf::Vector3 t = wTo.getOrigin();
    tf::Quaternion q = wTo.getRotation();

    //euclidean angles of the known object
    float rx = q.x() / sqrt(1- q.w()*q.w());
    float ry = q.y() / sqrt(1- q.w()*q.w());
    float rz = q.z() / sqrt(1- q.w()*q.w());*/

    std::vector< double > t_dist(poses.size()); //translation distance
    std::vector< std::vector<double> > r_diff(poses.size()); //rotation difference
    for (int i = 0; i < poses.size(); i++)
    {
        vpTranslationVector T;
        vpQuaternionVector Q;
        poses[i].extract(T);
        poses[i].extract(Q);

        //euclidean distance between the object and one reprojection
        double X = pow(T[0] - t[0], 2);
        double Y = pow(T[1] - t[1], 2);
        double Z = pow(T[2] - t[2], 2);
        t_dist[i] = sqrt(X+Y+Z);

        //euclidean angles of one reprojection of the object
        double RX = Q.x() / sqrt(1- Q.w()*Q.w());
        double RY = Q.y() / sqrt(1- Q.w()*Q.w());
        double RZ = Q.z() / sqrt(1- Q.w()*Q.w());

        std::vector<double> v(3);
        v[0] = RX-rx;
        v[1] = RY-ry;
        v[2] = RZ-rz;
        r_diff[i] = v;
    }

    //error calculation

    double dist_err = 0;
    double EX2 = 0; //expected value of x^2, i.e the average of r_dist[i]^2 for every i;
    double EX = 0; //expected value of x, i.e the averege of r_dist
    for (int i = 0; i < t_dist.size(); i++)
    {
        double x = t_dist[i];
        EX += x;
        EX2 += x*x;
    }
    EX = EX / t_dist.size();
    EX2 = EX2 / t_dist.size();

    dist_err = sqrt(EX2 - EX*EX); //see wikipedia at https://en.wikipedia.org/wiki/Standard_deviation for more info
    std::cout<<"TRANSLATION ERROR: "<<dist_err<<std::endl;

    std::vector < double > Ex(3);
    std::vector < double > Ex2(3);
    for (int i = 0; i < poses.size(); i++)
    {
        double n = Ex[0];
        Ex[0] = Ex[0] + r_diff[i][0];
        Ex[1] = Ex[1] + r_diff[i][1];
        Ex[2] = Ex[2] + r_diff[i][2];

        double m = r_diff[i][0];
        Ex2[0] = Ex2[0] + m*m;
        m = r_diff[i][1];
        Ex2[1] = Ex2[1] + m*m;
        m = r_diff[i][2];
        Ex2[2] = Ex2[2] + m*m;
    }
    Ex[0] = Ex[0] / r_diff.size();
    Ex[1] = Ex[1] / r_diff.size();
    Ex[2] = Ex[2] / r_diff.size();
    Ex2[0] = Ex2[0] / r_diff.size();
    Ex2[1] = Ex2[1] / r_diff.size();
    Ex2[2] = Ex2[2] / r_diff.size();

    std::vector <double> tmp(4);
    tmp.push_back(dist_err);

    std::cout<<"ROTATION ERRORS (deg, x y z)"<<std::endl;
    for (int i = 0; i < 3; i++)
    {
        double y = sqrt(Ex2[i]- Ex[i]*Ex[i]);
        tmp.push_back(y);
        std::cout<< y <<std::endl;
    }

    return tmp;
}


int main(int argc, char** argv)
{

    ros::init(argc, argv, "basic_hand_eye_node");
    ros::NodeHandle nh("~");

    int width,height;
    nh.param<int>(WIDTH,width,6);
    nh.param<int>(HEIGHT,height,7);

    std::string base_frame, hand_frame;
    nh.param<std::string>(WORLD_FRAME,base_frame,"/ur_base");
    nh.param<std::string>(HAND_FRAME,hand_frame,"/ee_link");

    std::string image_topic, camera_info_topic;
    nh.param<std::string>(IMAGE_TOPIC,image_topic,"/multisense_sl/camera/left/image_raw");
    nh.param<std::string>(CAMERA_INFO_TOPIC,camera_info_topic,"/multisense_sl/camera/left/camera_info");

    double L= 0.04;
    nh.param<double>(SIZE,L,0.04); //4 cm


    TransformManager tf_mng(nh);

    vpImage<unsigned char> I; // Create a gray level image container
    vpROSGrabber g;
    g.setMasterURI("http://localhost:11311"); // Create a grabber based on ROS
    g.setImageTopic(image_topic);
    g.setCameraInfoTopic(camera_info_topic);
    g.setRectify(true);
    g.open(I);
    vpDisplayX d(I);

    std::printf("\nStarting HAND-EYE calibration with continuous acquisition!\n");

    vpCameraParameters cam;
    g.getCameraInfo(cam);

    vpHomogeneousMatrix cMo;

    // Object initialization
    cv::Size s(4, 11);
    std::vector<vpPoint> point(s.height*s.width);
    int j=s.height, k=0;
    while (k < s.height*s.width)
    {
        if (k%s.width == 0)
            j--;

        point[k].setWorldCoordinates(L/2*j, L/2*(j%2)+(k%s.width)*L,0);
        k++;
    }


    ros::spinOnce();

    g.acquire(I);

    vpDisplay::display(I);

    std::vector<vpHomogeneousMatrix> c_o;
    std::vector<vpHomogeneousMatrix> w_e;


    int nAcquisitions = 0;
    ros::Rate loop_rate(5);
    while (ros::ok())
    {
        if ((nAcquisitions + 1) % 10 == 0) //idealmente circa 1 msg ogni 2 secondi
            std::cout<<"working..."<<std::endl;

        g.acquire(I);
        if (vpDisplay::getClick(I, false))
        {
            vpDisplay::display(I);
            vpDisplay::displayText(I, vpImagePoint(10,10), "Computing calibration...", vpColor::red);
            vpDisplay::flush(I);
            break;
        }

        cv::Mat frame;
        vpImageConvert::convert(I,frame);
        std::vector<cv::Point2f> centers;
        bool found = cv::findCirclesGrid(frame, s, centers, cv::CALIB_CB_ASYMMETRIC_GRID /*|| cv::CALIB_CB_CLUSTERING*/);

        vpDisplay::display(I);
        if (found)
        {

            for(int i = 0; i < centers.size() && centers.size() > 0; i++)
            {
                vpImagePoint p(centers[i].y,centers[i].x);
                vpDisplay::displayCircle(I, p, 4, vpColor::red, false, 2);
            }
            vpDisplay::displayText(I, vpImagePoint(10,10), "Click somewhere to stop the acquisition and start computing the calibration...", vpColor::red);
            vpDisplay::flush(I);
            computePose(point, centers, cam, true, cMo);

            //DEBUG
            for (int i = 0; i < point.size(); i++)
            {
                //repoject points to have a visual confirmation of the computed cMo
                point[i].project(cMo);
                double x = point[i].p[0], y = point[i].p[1];
                vpImagePoint p(0,0);
                vpMeterPixelConversion::convertPoint(cam, x, y, p);
                vpDisplay::displayCircle(I, p, 2, vpColor::blue ,false, 2);
            }
            vpDisplay::displayText(I,vpImagePoint(I.getHeight()-10,10), "Blu points are the reprojection of the virtual object, they should be roughly inside red points", vpColor::blue);
            vpDisplay::flush(I);
            //END DEBUG

            c_o.push_back(cMo);

            tf::Transform wTe;
            if (!tf_mng.get_Tf(base_frame, hand_frame, wTe, 0.05))
            {
                std::cout<<"At acquisition "<<nAcquisitions + 1<<" frame didn't exist, rejected"<<std::endl;
                tf::Matrix3x3 base = wTe.getBasis();
                //std::cout << "base: " << std::endl << base << std::endl << std::endl;
                continue;
            }

            tf::Quaternion q = wTe.getRotation();
            //std::cout << "q: " << std::endl << q << std::endl << std::endl;

            tf::Vector3 O = wTe.getOrigin();
            //std::cout << "O: " << std::endl << O << std::endl << std::endl;

            vpTranslationVector T;
            T[0] = O[0];
            T[1] = O[1];
            T[2] = O[2];
            vpQuaternionVector Q;
            Q[0] = q[0];
            Q[1] = q[1];
            Q[2] = q[2];
            Q[3] = q[3];
            vpHomogeneousMatrix wMe(T,Q);
            //std::cout << "wMe: " << std::endl << wMe << std::endl << std::endl;

            w_e.push_back(wMe);

            nAcquisitions++;
        }
        else
        {
            vpDisplay::displayText(I, vpImagePoint(10,10), "Click somewhere to stop the acquisition and start computing the calibration...", vpColor::red);
            vpDisplay::flush(I);
        }

        loop_rate.sleep();
        ros::spinOnce();
    }

    ros::spinOnce();

    if(nh.ok())
    {
        vpHomogeneousMatrix eMc;

        std::cout<<"\n\nworking with "<<nAcquisitions<< " acquisitions"<<std::endl;


        if (c_o.size() == w_e.size()) //per sicurezza
        {
            std::cout<<"c_o.size() "<< c_o.size() 
            << std::endl;
            vpCalibration::calibrationTsai(c_o, w_e, eMc);
            std::cout << "eMc:\n" << eMc << std::endl << std::endl;
            
            vpQuaternionVector q;
            vpTranslationVector t;
            eMc.extract(t);
            eMc.extract(q);
            ROS_INFO_STREAM("hand_camera calibration:\nTRASLATION:\n"<<t<<"\nROTATION:\n"<<q<<std::endl);
        }
        else
        {
            ROS_INFO_STREAM("\n\nBAD DATA ACQUISITION, FAILED CALIBRATION\n\n");
        }


        std::vector<double> errors = calculateErrors(eMc, c_o, w_e);

    }

    return 0;
}
