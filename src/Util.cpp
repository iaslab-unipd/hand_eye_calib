/**
 *   Util.cpp
 *
 *   Last edit: 11/05/2017
 *   Author: Andrea Gobbi
**/

#include "test_hand_eye/AutoAcquisitionUtil.h"

int main(int argc, char** argv)
{
    /*
    ros::init(argc, argv, "MoveRobot_node");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ROS_INFO(" --- Util_node active --- ");
    */
    std::string configuration_path = "/home/andrea/workspace/ros/catkin/src/test_hand_eye/config/Configuration_RA_FindChessboardFrame.yaml";
    test_hand_eye::AutoAcquisitionUtil obj_acq_util;

    cv::FileStorage fs( configuration_path, cv::FileStorage::READ );
    if( !fs.isOpened() ) 
    {
        ROS_ERROR("Error opening configuration file");
        exit(-1);
    }

    std::vector<double> rototransl_vector;
    fs["rototransl_matrix"] >> rototransl_vector;

    Eigen::Matrix3d rot_matrix;
    rot_matrix << rototransl_vector[0], rototransl_vector[1], rototransl_vector[2],
                  rototransl_vector[4], rototransl_vector[5], rototransl_vector[6],
                  rototransl_vector[8], rototransl_vector[9], rototransl_vector[10];

    std::cout << rot_matrix << std::endl;

    Eigen::Quaterniond q(rot_matrix);
    std::cout << "q ->: " << q.x() << " " << q.y() << " " << q.z() << " " << q.w() << std::endl;

    Eigen::Vector3d euler = q.toRotationMatrix().eulerAngles(0, 1, 2);
    std::cout << "euler -> roll, pitch, yaw: " << degrees(euler[0]) << " " << degrees(euler[1]) << " " << degrees(euler[2]) << std::endl;

    Eigen::Vector3d transl;
    transl << rototransl_vector[3], rototransl_vector[7], rototransl_vector[11];    
    std::cout << "transl -> x, y, z: " << transl[0] << " " << transl[1] << " " << transl[2] << std::endl;

    std::vector<double> quat_1;
    std::vector<double> quat_2;
    fs["q1"] >> quat_1;
    fs["q2"] >> quat_2;
    Eigen::Quaterniond q1(quat_1[3], quat_1[0], quat_1[1], quat_1[2]);
    Eigen::Quaterniond q2(quat_2[3], -quat_2[0], -quat_2[1], -quat_2[2]);
    Eigen::Quaterniond q_res = q1 * q2;
    double angle = degrees(2*std::acos(q_res.w()));
    std::cout << "Angle quaternion -> " << angle << std::endl;

    fs.release();

    return 0;
}