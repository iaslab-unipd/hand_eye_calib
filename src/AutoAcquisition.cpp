/**
 *   AutoAcquisition.cpp
 *
 *   Last edit: 20/04/2017
 *   Author: Andrea Gobbi
**/

#include "test_hand_eye/AutoAcquisitionUtil.h"

cv::Mat leftImage; 
sensor_msgs::CameraInfo info_camera_left;

/** 
 * @brief leftImageCallback read the left camera image from the topic
 * @param[in]  msg: info message of the left camera 
 * @return     
 */
void leftImageCallback( const sensor_msgs::ImageConstPtr& msg ) 
{
    cv_bridge::CvImagePtr cv_ptr;

    try
    { 
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        leftImage = cv_ptr->image.clone();

        // Image visualization
        cv::namedWindow("Left image");
        cv::imshow("Left image", leftImage);
        cv::startWindowThread();    
    }
    catch(cv_bridge::Exception& e)
    {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }
}

/** 
 * @brief cb_left_camera_info read info topic of the left camera 
 * @param[in]  msg: info message of the left camera 
 * @return     
 */
void cb_left_camera_info( const sensor_msgs::CameraInfo::ConstPtr& msg )
{
    info_camera_left = *msg;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "AutoAcquisition_node");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ROS_INFO(" --- AutoAcquisition node active --- ");

    std::string configuration_path = "/home/andrea/workspace/ros/catkin/src/test_hand_eye/config/Configuration_RA_AutoAcquisition.yaml";
    test_hand_eye::AutoAcquisitionUtil obj_acq_util;

    cv::FileStorage fs( configuration_path, cv::FileStorage::READ );
    if( !fs.isOpened() ) 
    {
        ROS_ERROR("Error opening configuration file");
        exit(-1);
    }
    const int number_imgs_for_plane = 5;
    int num_p = 0, number_inclinations = 0; 
    double time = 0.0, L = 0.0, d_p = 0.0;
    double t_x = 0.0, t_y = 0.0, t_z = 0.0, r_x = 0.0, r_y = 0.0, r_z = 0.0; // Start position
    double x_step = 0.0, y_step = 0.0, z_step = 0.0; // X, Y, Z step in the translation [m]
    double max_dist_x = 0.0, max_dist_y = 0.0, max_dist_z = 0.0; // X, Y, Z max distance step in the translation [m]
    std::vector<double> x_increment, y_increment, z_increment; // To have the chessboard in the center of the image [m]
    std::vector<double> x_angles, y_angles, z_angles; // To have different orientation of the end effector [degree]
    std::string image_topic, camera_info_topic;
    std::string base_frame, hand_frame, target_frame;
    std::string path_project, dir_acquisitions, dir_drawncenters, pose_file_name, config_file_name; 

    fs["num_p"] >> num_p;
    fs["d_p"] >> d_p;
    fs["ros_time"] >> time;
    fs["L"] >> L;

    fs["t_x"] >> t_x;
    fs["t_y"] >> t_y;
    fs["t_min_z"] >> t_z;
    fs["r_x"] >> r_x;
    fs["r_y"] >> r_y;
    fs["r_z"] >> r_z;

    fs["x_step"] >> x_step;
    fs["y_step"] >> y_step;
    fs["z_step"] >> z_step;
    fs["max_dist_x"] >> max_dist_x;
    fs["max_dist_y"] >> max_dist_y;
    fs["max_dist_z"] >> max_dist_z;
    fs["x_increment"] >> x_increment;
    fs["y_increment"] >> y_increment;
    fs["z_increment"] >> z_increment;

    fs["x_angles"] >> x_angles;
    fs["y_angles"] >> y_angles;
    fs["z_angles"] >> z_angles;

    fs["image_topic"] >> image_topic;
    fs["camera_info_topic"] >> camera_info_topic;
    fs["base_frame"] >> base_frame;
    fs["hand_frame"] >> hand_frame;
    fs["new_frame"] >> target_frame; // When chessboard frame is computed
    //fs["acircle_frame"] >> target_frame;   

    fs["path_project"] >> path_project;
    fs["sub_dir_acquisitions"] >> dir_acquisitions;
    fs["sub_dir_drawncenters"] >> dir_drawncenters;
    fs["pose_file_name"] >> pose_file_name;
    fs["config_file_name"] >> config_file_name;

    fs.release();

    // To move respect the camera
    cv::FileStorage fs_( path_project + config_file_name, cv::FileStorage::READ );
    if( !fs_.isOpened() ) 
    {
        ROS_ERROR("Error opening configuration file");
        exit(-1);
    }

    std::vector<double> hand_eye_vector;
    fs_["tf_hand_eye"] >> hand_eye_vector;

    
    tf::Vector3 transl_hand_eye( hand_eye_vector[3], hand_eye_vector[7], hand_eye_vector[11] );

    tf::Matrix3x3 rot_hand_eye( hand_eye_vector[0], hand_eye_vector[1], hand_eye_vector[2],
                                hand_eye_vector[4], hand_eye_vector[5], hand_eye_vector[6],
                                hand_eye_vector[8], hand_eye_vector[9], hand_eye_vector[10] );

    tf::Transform hand_eye(rot_hand_eye, transl_hand_eye);
    tf::Transform eye_hand = hand_eye.inverse();

    fs_.release();

    //sleep(3);
    // Read left camera images from the topic image_raw
    image_transport::ImageTransport imgLeft( nh );
    image_transport::Subscriber subLeft = imgLeft.subscribe( image_topic, 1, leftImageCallback );  
    // Read left camera info (useful to know intrinsic parameters of the camera)
    ros::Subscriber sb_left_camera_info = nh.subscribe( camera_info_topic, 1, cb_left_camera_info);

    sleep(time); 
 

    std::cout << "Reading configuration file" << std::endl
              << "num_p: " << num_p << ", d_p: " << d_p 
              << ", number_inclinations: " << number_inclinations << std::endl;
    std::cout << "path_project: " << path_project << std::endl;
    std::cout << "sub_dir_acquisitions: " << path_project << dir_acquisitions << std::endl; 

    std::string path_acquisitions = path_project + dir_acquisitions;
    std::string path_drawnCirclesImages = path_acquisitions + dir_drawncenters;

    // File where to save the end effector positions
    std::ofstream fout( path_acquisitions + pose_file_name );

    //The :move_group_interface:`MoveGroup` class can be easily setup using just the name 
    //of the group you would like to control and plan for.
    moveit::planning_interface::MoveGroup group("manipulator");
    //We will use the :planning_scene_interface:`PlanningSceneInterface` class to deal directly with the world.
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface; 
    //If you don't provide any start state, Moveit will use the current state
    group.setStartStateToCurrentState();

    // Get the tolerance that is used for reaching an orientation goal. 
    // This is the tolerance for roll, pitch and yaw, in radians. 
    double ori_tolerance = group.getGoalOrientationTolerance();
    // Get the tolerance that is used for reaching a position goal. 
    // This is be the radius of a sphere where the end-effector must reach. 
    double pose_tolerance = group.getGoalPositionTolerance();
    std::cout << "Position tolerance: " << pose_tolerance << ", Orientation tolerance: " << ori_tolerance << std::endl;
    //Set the orientation tolerance that is used for reaching the goal when moving to a pose.
    //group.setGoalOrientationTolerance(0.01);
    //ori_tolerance = group.getGoalOrientationTolerance();
    //Set the position tolerance that is used for reaching the goal when moving to a pose. 
    //group.setGoalPositionTolerance(0.001);
    //pose_tolerance = group.getGoalPositionTolerance();
    //std::cout << "Position tolerance: " << pose_tolerance << ", Orientation tolerance: " << ori_tolerance << std::endl; 

    std::string frame = group.getPoseReferenceFrame();
    std::cout << "Reference frame: " << frame << std::endl;

    if( frame != base_frame )
    {
        group.setPoseReferenceFrame(base_frame);
        frame = group.getPoseReferenceFrame();
        std::cout << "Reference frame: " << frame << std::endl;
    }

    // Class useful to find the transform between two frames
    TransformManager tf_mng(nh);

    sleep(time);

    group.setPoseReferenceFrame( target_frame );
    frame = group.getPoseReferenceFrame();
    std::cout << "Target frame: " << frame << std::endl;

    // Read intrinsic parameters from left/camera_info topic
    uint height = info_camera_left.width;
    uint width = info_camera_left.height;
    float focal_x = info_camera_left.K[0];
    float focal_y = info_camera_left.K[4];
    float c_x = info_camera_left.K[2];
    float c_y = info_camera_left.K[5];

    std::cout << "Image height: " << height << ", width: " << width << std::endl;
    std::cout << "Focal_x: " << focal_x << ", focal_y: " << focal_y << std::endl;
    std::cout << "C_x: " << c_x << ", c_y: " << c_y << std::endl << std::endl;

    vpCameraParameters cam(focal_x, focal_y, c_x, c_y); 
    std::vector<cv::Point2f> centers;
    cv::Size s(4, 11); // The circles aren't found with s(11, 4)
    std::vector<vpHomogeneousMatrix> c_o;
    std::vector<vpHomogeneousMatrix> w_e;
    std::vector<vpPoint> point(s.height*s.width);
    int j=s.height, k=0;
    while (k < s.height*s.width)
    {
        if (k%s.width == 0)
            j--;

        point[k].setWorldCoordinates(L/2*j, L/2*(j%2)+(k%s.width)*L, 0);
        //std::cout << "-- Point" << k << " " << point[k].oP[0] << " " << point[k].oP[1] << " " << point[k].oP[2] << std::endl; 

        k++;
    } 
    
    if( x_angles.size() == y_angles.size() && x_angles.size() == z_angles.size() && 
        x_increment.size() == y_increment.size() && x_angles.size() == x_increment.size() )
        number_inclinations = x_angles.size();
    else
        ROS_ERROR("Check that x_angles, y_angles, z_angles, x_increment and y_increment have the same size in the configuration file.");
    
    // Acquisition
    double z_temp = t_z;
    int count_image = 0;
    for( int plane = 0; plane < num_p; ++plane )
    {
        sleep(time);

        for( int angle = 0; angle < number_inclinations; ++angle )
        {
            sleep(time);

            // Copy the start position of the end effector
            double x_temp = t_x; 
            double y_temp = t_y;
            //double z_temp = t_z;
            double r_x_temp = r_x;
            double r_y_temp = r_y;
            double r_z_temp = r_z;

            // To center the chessboard in the image view
            x_temp += x_increment[ angle ];
            y_temp += y_increment[ angle ];

            double x_temp_start = x_temp; 
            double y_temp_start = y_temp;
            double z_temp_start = z_temp;

            // Change the orientation of the end effector according to the read values
            r_x_temp += x_angles[ angle ];
            r_y_temp += y_angles[ angle ];
            r_z_temp += z_angles[ angle ];
            
            // Go in the start position
            tf::Vector3 transl( x_temp, y_temp, z_temp );
            tf::Quaternion q;
            q.setRPY( radians(r_x_temp), radians(r_y_temp), radians(r_z_temp) );

            bool success = obj_acq_util.planPoseMotionToCameraGoal( group, 
                                                                    transl,
                                                                    q,
                                                                    hand_eye,
                                                                    time );

            if( success )
                ROS_INFO_STREAM("Plane " << plane << ", angle " << angle << " reached.");
            else
            {
                ROS_ERROR_STREAM("Plane " << plane << ", angle " << angle << " not reached.");
                //break; // Go out from the inner loop
                continue; // To force the next iteration of the loop to take place
            }

            cv::Mat image = leftImage.clone(); 
            sleep(time);

            bool found = cv::findCirclesGrid( image, s, centers, cv::CALIB_CB_ASYMMETRIC_GRID ); 

            std::string image_name;
            if( found )
            {
                // Give the name to the image to save
                char num[400];
                sprintf(num, "Img%03d.bmp", count_image);

                std::stringstream name;
                name << num;   
                image_name = name.str();
                
                // Save the image
                cv::imwrite( path_acquisitions + image_name, image );  
                count_image++;  

                vpHomogeneousMatrix cMo;
                obj_acq_util.computePose(point, centers, cam, true, cMo); 
                std::cout << "cMo: " << std::endl << cMo << std::endl << std::endl;
                c_o.push_back(cMo);

                vpHomogeneousMatrix wMe;
                // Save in the text file the end effector position respect to the world reference
                obj_acq_util.saveEEPosition( fout, tf_mng, base_frame, hand_frame, wMe );
                w_e.push_back(wMe);

                // Draw the centers in the image and then save it
                cv::drawChessboardCorners( image, s, centers, found ); 
                cv::imwrite( path_drawnCirclesImages + image_name, image );  

                ROS_INFO_STREAM("Image (" << image_name << ") saved.");         

                for(int y=1; y<5; ++y)
                {
                    // Come back in the start position
                    tf::Vector3 transl_start( x_temp_start, y_temp_start, z_temp_start );
                    success = obj_acq_util.planPoseMotionToCameraGoal( group, 
                                                                       transl_start, 
                                                                       q, 
                                                                       hand_eye,
                                                                       time );

                    image = leftImage.clone();

                    x_temp = x_temp_start;
                    y_temp = y_temp_start;
                    z_temp = z_temp_start;

                    double last_x_good = x_temp;
                    double last_y_good = y_temp;
                    double last_z_good = z_temp;

                    // Change the position of the end effector until the chessboard is found
                    while( cv::findCirclesGrid(image, s, centers, cv::CALIB_CB_ASYMMETRIC_GRID) && success )
                    {
                        last_x_good = x_temp;
                        last_y_good = y_temp;
                        last_z_good = z_temp;

                        // Update the position that the end effector has to go
                        ////////////////////////////////////////////////////////
                        switch(y) // Bisogna modificare qui in base agli assi che si trovano sul piano della scacchiera
                                  // Nel simulato ci si muoveva rispetto agli assi x e y del frame sulla scacchiera
                        {
                            case 1:
                                x_temp -= x_step;
                                break;
                            case 2:
                                x_temp += x_step;
                                break;
                            case 3:         
                                y_temp -= y_step;
                                break;
                            case 4:
                                y_temp += y_step;
                                break;
                        }

                        // Exection of the movement
                        tf::Vector3 transl_temp( x_temp, y_temp, z_temp );
                        success = obj_acq_util.planPoseMotionToCameraGoal( group, 
                                                                           transl_temp, 
                                                                           q, 
                                                                           hand_eye,
                                                                           time ); 

                        // Update the view image
                        image = leftImage.clone(); 

                        if( success && cv::findCirclesGrid(image, s, centers, cv::CALIB_CB_ASYMMETRIC_GRID) )
                        {
                            // Give the name to the image to save
                            char num_temp_[400];
                            sprintf(num_temp_, "Img%03d.bmp", count_image);

                            std::stringstream name_temp_;
                            name_temp_ << num_temp_;   
                            std::string image_name_temp_ = name_temp_.str();
                            // Save the image
                            cv::imwrite( path_acquisitions + image_name_temp_, image );  
                            count_image++;

                            obj_acq_util.computePose(point, centers, cam, true, cMo); 
                            std::cout << "cMo: " << std::endl << cMo << std::endl << std::endl;
                            c_o.push_back(cMo);

                            vpHomogeneousMatrix wMe;
                            // Save in the text file the end effector position respect to the world reference
                            obj_acq_util.saveEEPosition( fout, tf_mng, base_frame, hand_frame, wMe );
                            w_e.push_back(wMe);

                            // Draw the centers in the image and then save it
                            cv::drawChessboardCorners( image, s, centers, found ); 
                            cv::imwrite( path_drawnCirclesImages + image_name_temp_, image );  

                            ROS_INFO_STREAM("Image (" << image_name_temp_ << ") saved.");    
                        }

                        // Update the view image
                        image = leftImage.clone(); 

                        if( !success )
                        {
                            ROS_ERROR_STREAM("Check if the position (" << x_temp << ", " << y_temp 
                                          << ", " << z_temp << ") is reachable from the end effector."); 
                            continue;
                        }
                    }
                    /*
                    // Come back to the last position where the chessboard is found 
                    tf::Vector3 transl_good( last_x_good, last_y_good, last_z_good );
                    success = obj_acq_util.planPoseMotionToGoal( group, transl_good, q, time );
                    // Update the view image
                    image = leftImage.clone(); 
                    found = cv::findCirclesGrid(image, s, centers, cv::CALIB_CB_ASYMMETRIC_GRID);

                    if( success && found )
                    {
                        // Give the name to the image to save
                        char num_temp[400];
                        sprintf(num_temp, "Img%03d.bmp", count_image);

                        std::stringstream name_temp;
                        name_temp << num_temp;   
                        std::string image_name_temp = name_temp.str();
                        // Save the image
                        cv::imwrite( path_acquisitions + image_name_temp, image );  
                        count_image++;

                        obj_acq_util.computePose(point, centers, cam, true, cMo); 
                        std::cout << "cMo: " << std::endl << cMo << std::endl << std::endl;
                        c_o.push_back(cMo);

                        vpHomogeneousMatrix wMe;
                        // Save in the text file the end effector position respect to the world reference
                        obj_acq_util.saveEEPosition( fout, tf_mng, base_frame, hand_frame, wMe );
                        w_e.push_back(wMe);

                        // Draw the centers in the image and then save it
                        cv::drawChessboardCorners( image, s, centers, found ); 
                        cv::imwrite( path_drawnCirclesImages + image_name_temp, image );  

                        ROS_INFO_STREAM("Image (" << image_name_temp << ") saved.");      
                    }
                    else
                    {
                        ROS_ERROR_STREAM("Problem to come back to the pose to save the image.");   
                        continue;    
                    }
                    */
                }
            }
            else
            {
                ROS_ERROR_STREAM("Chessboard not found in the initial position. Check if the position (" << x_temp 
                                  << ", " << y_temp << ", " << z_temp << ") is reachable from the end effector.");
            }       
        }

        z_temp -= d_p;
    }

    vpHomogeneousMatrix eMc;
    if( c_o.size() == w_e.size() ) 
    {
        std::cout << "\nc_o and w_e size: " << std::endl << c_o.size() << std::endl << std::endl;

        vpCalibration::calibrationTsai(c_o, w_e, eMc);
        std::cout << "eMc: " << std::endl << eMc << std::endl << std::endl;
    }
    
    // Come back to the start position
    tf::Vector3 transl( t_x, t_y, t_z );
    tf::Quaternion q;
    q.setRPY( radians(r_x), radians(r_y), radians(r_z) );   
    bool success = obj_acq_util.planPoseMotionToGoal( group, transl, q, time );

    if( success )
        ROS_INFO("The acquisition is completed.");
    
    cv::destroyWindow("Left image");

    return 0;
}