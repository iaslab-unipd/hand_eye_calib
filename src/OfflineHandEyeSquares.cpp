/*
 *   OfflineHandEyeSquares.cpp
 *
 *   Last edit: 27/03/2017
 *   Author: Andrea Gobbi
 */

#include "test_hand_eye/HandEyeUtil.h"

// struct param used in selectImagesUI to call callBackFunc
struct param
{
   int count = 0;                      // Index of the last image in the window
   int last_img = 0;                   // Number of images in a window
   int original_dim_rows = 0; 
   int original_dim_cols = 0;
   std::vector<cv::Mat> images_vector; // Vector with the images of the dataset
   std::vector<bool> images_index;     // Vector that has true values in the indices of the selected images
   cv::Mat dst;                        // Window to show
   // Counters of the number of clicks in every image (if c_o is odd, image 0 in the window is selected)
   int c_0 = 0, c_1 = 0, c_2 = 0, c_3 = 0, c_4 = 0, c_5 = 0, c_6 = 0, c_7 = 0, c_8 = 0, c_9 = 0; 
};

/**
 * @brief callBackFunc implements the choice of the images by UI
 * @param[in] event: type of action (in this case EVENT_LBUTTONDOWN)
 * @param[in] col: column of the selected pixels 
 * @param[in] row: row of the selected pixels
 * @param[in] flags: not used
 * @param[in/out] val: struct containing some useful parameters passed by reference
 */
void callBackFunc( int event, int col, int row, int flags, void* val )
{
    struct param* par = (struct param*) val;
    if( event == cv::EVENT_LBUTTONDOWN )
    {
        //std::cout << "Left button clicked at (row = " << row << ") & (col = " << col << ")" << std::endl;

        int selected_image;
        cv::Mat color_green = cv::Mat( par->original_dim_rows, par->original_dim_cols, CV_8UC3, cv::Scalar(0,255,0) );
        cv::Mat color_red = cv::Mat( par->original_dim_rows, par->original_dim_cols, CV_8UC3, cv::Scalar(0,0,255) );
        cv::Mat image;

        // According to the pixel clicked, the interested images are stored
        if( row < par->original_dim_rows && col < par->original_dim_cols )
        {
            if( par->last_img == 10 )
                selected_image = par->count - 10;
            else
                selected_image = par->count/10*10;

            par->c_0++;
            if( par->c_0 % 2 != 0 )
            {
                std::cout << "Selected image: " << selected_image << std::endl;
                // images_index stores the selected image
                par->images_index[selected_image] = true;
                // Change the color around the image (green if it is selected)
                color_green.copyTo( par->dst.rowRange( 0, par->original_dim_rows ).colRange( 0, par->original_dim_cols ) );
            }
            else
            {
                std::cout << "Deselected image: " << selected_image << std::endl;
                // images_index stores the deselected image
                par->images_index[selected_image] = false;
                // Change the color around the image (red if it is selected)
                color_red.copyTo( par->dst.rowRange( 0, par->original_dim_rows ).colRange( 0, par->original_dim_cols ) );
            }
            
            image = par->images_vector[selected_image];
            image.copyTo( par->dst.rowRange( 3, par->original_dim_rows-3 ).colRange( 3, par->original_dim_cols-3 ) );
        }
        else if( row < par->original_dim_rows && col < 2 * par->original_dim_cols && par->last_img > 1 )
        {
            if( par->last_img == 10 )
                selected_image = par->count - 9;
            else
                selected_image = par->count/10*10 + 1;

            par->c_1++;
            if( par->c_1 % 2 != 0 )
            {
                std::cout << "Selected image: " << selected_image << std::endl;
                par->images_index[selected_image] = true;
                color_green.copyTo( par->dst.rowRange( 0, par->original_dim_rows ).colRange( 1*par->original_dim_rows, 2*par->original_dim_rows ) );
            }
            else
            {
                std::cout << "Deselected image: " << selected_image << std::endl;
                par->images_index[selected_image] = false;
                color_red.copyTo( par->dst.rowRange( 0, par->original_dim_rows ).colRange( 1*par->original_dim_rows, 2*par->original_dim_rows ) );
            }
            
            image = par->images_vector[selected_image];
            image.copyTo( par->dst.rowRange( 3, par->original_dim_rows-3 ).colRange( 3+1*par->original_dim_cols, 2*par->original_dim_cols-3 ) );
        }
        else if( row < par->original_dim_rows && col < 3 * par->original_dim_cols && par->last_img > 2 )
        {
            if( par->last_img == 10 )
                selected_image = par->count - 8;
            else
                selected_image = par->count/10*10 + 2;

            par->c_2++;
            if( par->c_2 % 2 != 0 )
            {
                std::cout << "Selected image: " << selected_image << std::endl;
                par->images_index[selected_image] = true;
                color_green.copyTo( par->dst.rowRange( 0, par->original_dim_rows ).colRange( 2*par->original_dim_rows, 3*par->original_dim_rows ) );
            }
            else
            {
                std::cout << "Deselected image: " << selected_image << std::endl;
                par->images_index[selected_image] = false;
                color_red.copyTo( par->dst.rowRange( 0, par->original_dim_rows ).colRange( 2*par->original_dim_rows, 3*par->original_dim_rows ) );
            }
            
            image = par->images_vector[selected_image];
            image.copyTo( par->dst.rowRange( 3, par->original_dim_rows-3 ).colRange( 3+2*par->original_dim_cols, 3*par->original_dim_cols-3 ) );
        }
        else if( row < par->original_dim_rows && col < 4 * par->original_dim_cols && par->last_img > 3 )
        {
            if( par->last_img == 10 )
                selected_image = par->count - 7;
            else
                selected_image = par->count/10*10 + 3;

            par->c_3++;
            if( par->c_3 % 2 != 0 )
            {
                std::cout << "Selected image: " << selected_image << std::endl;
                par->images_index[selected_image] = true;
                color_green.copyTo( par->dst.rowRange( 0, par->original_dim_rows ).colRange( 3*par->original_dim_rows, 4*par->original_dim_rows ) );
            }
            else
            {
                std::cout << "Deselected image: " << selected_image << std::endl;
                par->images_index[selected_image] = false;
                color_red.copyTo( par->dst.rowRange( 0, par->original_dim_rows ).colRange( 3*par->original_dim_rows, 4*par->original_dim_rows ) );
            }
            
            image = par->images_vector[selected_image];
            image.copyTo( par->dst.rowRange( 3, par->original_dim_rows-3 ).colRange( 3+3*par->original_dim_cols, 4*par->original_dim_cols-3 ) );
        }
        else if( row < par->original_dim_rows && col < 5 * par->original_dim_cols && par->last_img > 4 )
        {
            if( par->last_img == 10 )
                selected_image = par->count - 6;
            else
                selected_image = par->count/10*10 + 4;

            par->c_4++;
            if( par->c_4 % 2 != 0 )
            {
                std::cout << "Selected image: " << selected_image << std::endl;
                par->images_index[selected_image] = true;
                color_green.copyTo( par->dst.rowRange( 0, par->original_dim_rows ).colRange( 4*par->original_dim_rows, 5*par->original_dim_rows ) );
            }
            else
            {
                std::cout << "Deselected image: " << selected_image << std::endl;
                par->images_index[selected_image] = false;
                color_red.copyTo( par->dst.rowRange( 0, par->original_dim_rows ).colRange( 4*par->original_dim_rows, 5*par->original_dim_rows ) );
            }
            
            image = par->images_vector[selected_image];
            image.copyTo( par->dst.rowRange( 3, par->original_dim_rows-3 ).colRange( 3+4*par->original_dim_cols, 5*par->original_dim_cols-3 ) );
        }
        else if( row < 2 * par->original_dim_rows && col < par->original_dim_cols && par->last_img > 5 )
        {
            if( par->last_img == 10 )
                selected_image = par->count - 5;
            else
                selected_image = par->count/10*10 + 5;

            par->c_5++;
            if( par->c_5 % 2 != 0 )
            {
                std::cout << "Selected image: " << selected_image << std::endl;
                par->images_index[selected_image] = true;
                color_green.copyTo( par->dst.rowRange( par->original_dim_rows, 2*par->original_dim_rows ).colRange( 0, par->original_dim_cols ) );
            }
            else
            {
                std::cout << "Deselected image: " << selected_image << std::endl;
                par->images_index[selected_image] = false;
                color_red.copyTo( par->dst.rowRange( par->original_dim_rows, 2*par->original_dim_rows ).colRange( 0, par->original_dim_cols ) );
            }
            
            image = par->images_vector[selected_image];
            image.copyTo( par->dst.rowRange( 3+par->original_dim_rows, 2*par->original_dim_rows-3 ).colRange( 3, par->original_dim_cols-3 ) );
        }
        else if( row < 2 * par->original_dim_rows && col < 2 * par->original_dim_cols && par->last_img > 6 )
        {
            if( par->last_img == 10 )
                selected_image = par->count - 4;
            else
                selected_image = par->count/10*10 + 6;

            par->c_6++;
            if( par->c_6 % 2 != 0 )
            {
                std::cout << "Selected image: " << selected_image << std::endl;
                par->images_index[selected_image] = true;
                color_green.copyTo( par->dst.rowRange( par->original_dim_rows, 2*par->original_dim_rows ).colRange( 1*par->original_dim_rows, 2*par->original_dim_rows ) );
            }
            else
            {
                std::cout << "Deselected image: " << selected_image << std::endl;
                par->images_index[selected_image] = false;
                color_red.copyTo( par->dst.rowRange( par->original_dim_rows, 2*par->original_dim_rows ).colRange( 1*par->original_dim_rows, 2*par->original_dim_rows ) );
            }
            
            image = par->images_vector[selected_image];
            image.copyTo( par->dst.rowRange( 3+par->original_dim_rows, 2*par->original_dim_rows-3 ).colRange( 3+1*par->original_dim_cols, 2*par->original_dim_cols-3 ) );
        }
        else if( row < 2 * par->original_dim_rows && col < 3 * par->original_dim_cols && par->last_img > 7 )
        {
            if( par->last_img == 10 )
                selected_image = par->count - 3;
            else
                selected_image = par->count/10*10 + 7;

            par->c_7++;
            if( par->c_7 % 2 != 0 )
            {
                std::cout << "Selected image: " << selected_image << std::endl;
                par->images_index[selected_image] = true;
                color_green.copyTo( par->dst.rowRange( par->original_dim_rows, 2*par->original_dim_rows ).colRange( 2*par->original_dim_rows, 3*par->original_dim_rows ) );
            }
            else
            {
                std::cout << "Deselected image: " << selected_image << std::endl;
                par->images_index[selected_image] = false;
                color_red.copyTo( par->dst.rowRange( par->original_dim_rows, 2*par->original_dim_rows ).colRange( 2*par->original_dim_rows, 3*par->original_dim_rows ) );
            }
            
            image = par->images_vector[selected_image];
            image.copyTo( par->dst.rowRange( 3+par->original_dim_rows, 2*par->original_dim_rows-3 ).colRange( 3+2*par->original_dim_cols, 3*par->original_dim_cols-3 ) );
        }
        else if( row < 2 * par->original_dim_rows && col < 4 * par->original_dim_cols && par->last_img > 8 )
        {
            if( par->last_img == 10 )
                selected_image = par->count - 2;
            else
                selected_image = par->count/10*10 + 8;

            par->c_8++;
            if( par->c_8 % 2 != 0 )
            {
                std::cout << "Selected image: " << selected_image << std::endl;
                par->images_index[selected_image] = true;
                color_green.copyTo( par->dst.rowRange( par->original_dim_rows, 2*par->original_dim_rows ).colRange( 3*par->original_dim_rows, 4*par->original_dim_rows ) );
            }
            else
            {
                std::cout << "Deselected image: " << selected_image << std::endl;
                par->images_index[selected_image] = false;
                color_red.copyTo( par->dst.rowRange( par->original_dim_rows, 2*par->original_dim_rows ).colRange( 3*par->original_dim_rows, 4*par->original_dim_rows ) );
            }
            
            image = par->images_vector[selected_image];
            image.copyTo( par->dst.rowRange( 3+par->original_dim_rows, 2*par->original_dim_rows-3 ).colRange( 3+3*par->original_dim_cols, 4*par->original_dim_cols-3 ) );
        }
        else if( row < 2 * par->original_dim_rows && col < 5 * par->original_dim_cols && par->last_img > 9 )
        {
            if( par->last_img == 10 )
                selected_image = par->count - 1;
            else
                selected_image = par->count/10*10 + 9;

            par->c_9++;
            if( par->c_9 % 2 != 0 )
            {
                std::cout << "Selected image: " << selected_image << std::endl;
                par->images_index[selected_image] = true;
                color_green.copyTo( par->dst.rowRange( par->original_dim_rows, 2*par->original_dim_rows ).colRange( 4*par->original_dim_rows, 5*par->original_dim_rows ) );
            }
            else
            {
                std::cout << "Deselected image: " << selected_image << std::endl;
                par->images_index[selected_image] = false;
                color_red.copyTo( par->dst.rowRange( par->original_dim_rows, 2*par->original_dim_rows ).colRange( 4*par->original_dim_rows, 5*par->original_dim_rows ) );
            }
            
            image = par->images_vector[selected_image];
            image.copyTo( par->dst.rowRange( 3+par->original_dim_rows, 2*par->original_dim_rows-3 ).colRange( 3+4*par->original_dim_cols, 5*par->original_dim_cols-3 ) );
        }
        else
            std::cout << "Not valid point" << std::endl;            

        cv::imshow( "Images Window", par->dst );
    }
}

/**
 * @brief selectImagesUI allows you to choose by UI the images to use for the Hand-Eye Calibration  
 * @param[in] img_path: path of the folder that contains all the images
 * @param[in] img_name: name of the images (Camera_)
 * @param[in] img_extension: extension od the images (.bmp)
 * @param[in] pose_file_name: path of the original file of the positions
 * @param[in] selected_image_folder: directory where the selected images are saved 
 * @param[in] tot_image_number: total number of images 
 * @return    count: number of images selected
 */
int selectImagesUI( const std::string img_path,
                    const std::string img_name,
                    const std::string img_extension,
                    const std::string pose_file_name,
                    const std::string selected_image_folder,
                    const int tot_image_number )
{
    std::cout << "Displays 10 images at a time, select those of interest with the mouse (with the left button)" << std::endl;

    // struct for the CallBack function
    struct param par;

    // Vector which stores all images
    std::vector<cv::Mat> images_vector;
    images_vector.reserve( tot_image_number );
    par.images_vector.reserve( tot_image_number ); // struct
    par.images_index.reserve( tot_image_number );  // struct
    for( int i=0; i<tot_image_number; ++i )
    {
        std::string number;
        if( i < 10 )
        {
            std::string tmp = static_cast<std::ostringstream*>( &(std::ostringstream() << i) )->str();
            number = "00"+tmp;
        }
        else if ( i >= 10 && i < 100 )
        {
            std::string tmp = static_cast<std::ostringstream*>( &(std::ostringstream() << i) )->str();
            number = "0" + tmp;
        }
        else
            number = static_cast<std::ostringstream*>( &(std::ostringstream() << i) )->str();

        images_vector.push_back( cv::imread( img_path+img_name+number+img_extension, CV_LOAD_IMAGE_GRAYSCALE ) );
        
        par.images_index.push_back( false );
    }

    const int original_dim_rows = images_vector[0].rows / 4;
    const int original_dim_cols = images_vector[0].cols / 4;
    int dim_rows = original_dim_rows-6;
    int dim_cols = original_dim_cols-6;

    // struct
    par.original_dim_rows = original_dim_rows;
    par.original_dim_cols = original_dim_cols;

    const int num_img_to_display = 10;
    const int dst_cols = original_dim_cols * 5;
    const int dst_rows = original_dim_rows * 2;
    
    cv::Mat dst; 

    int count = 0; 
    int last_img = 10, middle_img = 5;
    cv::Size img_size = cv::Size( dim_cols , dim_rows );

    int num_iter = tot_image_number / num_img_to_display;
    if( tot_image_number % num_img_to_display != 0 )
        num_iter++;

    cv::namedWindow("Images Window");

    // The selection proceeds with 10 images at a time
    for( int j=0; j<num_iter; ++j )
    {
        dst = cv::Mat( dst_rows, dst_cols, CV_8UC3, cv::Scalar(0,0,255) );

        if( tot_image_number < (j+1) * num_img_to_display )
        {
            last_img = tot_image_number % num_img_to_display;
            par.last_img = last_img;
        }
        else
        {
            last_img = 10;
            par.last_img = last_img;
        }
            
        std::cout << "Displays images from " << count << " to " << count + last_img - 1 << std::endl;

        for( int i=0; i<last_img; ++i )
        {
            cv::Mat image = images_vector[count];
            
            cv::resize( image, image, img_size );
            cv::cvtColor( image, image, CV_GRAY2BGR );

            par.images_vector.push_back( image ); // struct

            if( i < middle_img )
                image.copyTo( dst.rowRange( 3, original_dim_rows-3 ).colRange( 3+i*original_dim_cols, 3+i*original_dim_cols+image.cols ) );
            else
                image.copyTo( dst.rowRange( 3+original_dim_rows, 3+original_dim_rows+image.rows ).colRange( 3+(i-middle_img)*original_dim_cols, 3+(i-middle_img)*original_dim_cols+image.cols ) );

            count++;
        }
        par.dst = dst;
        par.count = count;
        par.c_0 = par.c_1 = par.c_2 = par.c_3 = par.c_4 = par.c_5 = par.c_6 = par.c_7 = par.c_8 = par.c_9 = 0;  
        
        cv::imshow( "Images Window", dst );
        cv::setMouseCallback( "Images Window", callBackFunc, &par );
        cv::waitKey(0); 
    }

    std::ifstream fin( img_path + pose_file_name ) ;
    std::ofstream fout( img_path + selected_image_folder + "Robot.txt" );
    std::string line;

    count = 0;
    if( fin.is_open() )
    {
        for( int i=0; i<tot_image_number; ++i )
        {
            std::getline( fin, line );

            if( par.images_index[i] == true ) // If the image i has been selected
            {
                std::string number;
                if( count < 10 )
                {
                    std::string tmp = static_cast<std::ostringstream*>( &(std::ostringstream() << count) )->str();
                    number = "00" + tmp;
                }
                else if ( count >= 10 && count < 100 )
                {
                    std::string tmp = static_cast<std::ostringstream*>( &(std::ostringstream() << count) )->str();
                    number = "0" + tmp;
                }
                else
                {
                    std::string tmp = static_cast<std::ostringstream*>( &(std::ostringstream() << count) )->str();
                    number = "" + tmp;
                }

                // Write the image selected in the new directory
                cv::imwrite( img_path + selected_image_folder + img_name + number + img_extension, images_vector[i] );

                // Write the six values of the end-effector position in the new file
                fout << line + "\n";

                count++;
            }
        }
    }

    fin.close();
    cv::destroyWindow( "Images Window" );

    return count;
}

int main(int argc, char** argv)
{
    std::string configuration_path = "/home/andrea/workspace/ros/catkin/src/test_hand_eye/config/Configuration_ED3D.yaml";
    std::cout << "\nConfiguration file:\n" << configuration_path << std::endl << std::endl;

    cv::FileStorage fs(configuration_path, cv::FileStorage::READ);
    if (!fs.isOpened()) 
    {
        std::cerr << "Error opening configuration file." << std::endl;
        exit(-1);
    }

    test_hand_eye::HandEyeUtil obj_util;

    const float PI = 3.14159;
    int image_number, width, height, width_image_px, height_image_px, focal_values;
    int image_selection;
    bool obtain_focal_values = false;
    float L, focal_x, focal_y, c_x, c_y;
    std::string main_path, camera_info_topic, img_path, img_corners_path, img_visp_res_path; 
    std::string img_selected_path, pose_file_name, img_name, img_extension;

    fs["main_path"] >> main_path;
    fs["camera_info_topic"] >> camera_info_topic;
    fs["img_path"] >> img_path;
    fs["img_selected_path"] >> img_selected_path;
    fs["img_corners_path"] >> img_corners_path;
    fs["img_visp_res_path"] >> img_visp_res_path;
    fs["pose_file_name"] >> pose_file_name;
    fs["img_name"] >> img_name;
    fs["img_extension"] >> img_extension;

    fs["image_number"] >> image_number;
    fs["width"] >> width;
    fs["height"] >> height;
    fs["length"] >> L;
    fs["width_image_px"] >> width_image_px;
    fs["height_image_px"] >> height_image_px;

    fs["use_known_focal_values"] >> focal_values;
    // If focal_values isn't 0, cameraCalibration computes focal_x, focal_y, c_x and c_y using the OpenCV process
    if( focal_values == 0 )
    {
        fs["focal_x"] >> focal_x;
        fs["focal_y"] >> focal_y;
        fs["c_x"] >> c_x;
        fs["c_y"] >> c_y;
    }
    else 
        obj_util.cameraCalibration( main_path+img_path+img_name, img_extension, image_number, L, width-1, height-1, focal_x, focal_y, c_x, c_y );
    
    fs["image_selection"] >> image_selection;
    // If image_selection is 0, the user selects the images from user interface
    if( image_selection == 0 )
    {
        image_number = selectImagesUI( main_path+img_path, img_name, img_extension, pose_file_name, img_selected_path, image_number );
        img_path  = img_path + img_selected_path; // Update the path to open the images and the positions file
        std::cout << std::endl;
    }

    fs.release();

    // The main intrinsic camera parameters are (focal_x, focal_y) the ratio between the focal length and 
    // the size of a pixel, and (c_x, c_y) the coordinates of the principal point in pixel. 
    // Not in this case, but the lens distortion can also be considered by two additional parameters (k_{ud}, k_{du}).
    vpCameraParameters cam(focal_x, focal_y, c_x, c_y); 

    // Object initialization, they are useful to compute the camera -> object matrix (cMo)
    cv::Size s(width-1, height-1); // The chessboard has 11 columns and 9 rows, so 10*8 corners
    std::vector<vpPoint> point(s.height*s.width); 
    std::vector<vpPoint> invertedPoint(s.height*s.width);
    
    int j=s.height; // 10
    int k=0;
    while(k < s.height*s.width)
    {
        //Set the coordinates (ox, oy, oz) of the chessboard corners
        point[k].setWorldCoordinates(L*j, L*((k%s.width)+1), 0);
        //std::cout << " point[" << k << "] = (" << point[k].get_oX() << ", " << point[k].get_oY() << ", 0)" << std::endl;

        if((k+1)%s.width==0)
            j--;
        k++;
    }

    k=0; j=s.height*s.width;
    while(k < s.height*s.width)
    {
        //Set the coordinates (ox, oy, oz) of the chessboard corners, in case of a different chessboard orientation 
        invertedPoint[k] = point[j-1];

        k++; j--;
    }

    vpImage< unsigned char > I(width_image_px, height_image_px); // 800*800 pixels
    vpDisplayX d(I);
    std::vector<vpHomogeneousMatrix> c_o; // Camera -> object matrices vector
    std::vector<vpHomogeneousMatrix> w_e; // World -> end-effector matrices vector

    int count = -1;
    // To store which image has the corners in the opposite order
    int inverted_count = 0;
    bool invertedP = false;
    std::vector<bool> areCornersImagesInverted; 
    // Vector with all the corners of all the images that has to be passed to the function returnITError2D
    std::vector<std::vector<cv::Point2f>> images_corners;
    
    // Read the poses, from the txt file, of the end-effector respect to the base of the robot 
    std::ifstream poses_file;
    poses_file.open(main_path+img_path+pose_file_name);

    //std::this_thread::sleep_for (std::chrono::seconds(1));
    for( int img=0; img<image_number; ++img )
    {
        double x = 0, y = 0, z = 0, rad_x = 0, rad_y = 0, rad_z = 0; // Translation and rotation values
        if(poses_file.is_open())
        {
            std::string line;
            if(getline(poses_file, line))
                count++;
            else
                break;

            std::string::size_type sz;
            std::cout << "Row " << count << ": " << line << std::endl;
            //std::cout << "Extracts values: ";
            x = std::stod(line, &sz);
           
            line = line.substr(sz+1);
            y = std::stod(line, &sz);
        
            line = line.substr(sz+1);
            z = std::stod(line, &sz);
            
            line = line.substr(sz+1);
            rad_x = std::stod(line, &sz);
            //rad_x = (rad_x * PI) / 180; // Convert to radiant, in case these values are in cartesian
            
            line = line.substr(sz+1);
            rad_y = std::stod(line, &sz);
            //rad_y = (rad_y * PI) / 180; // Convert to radiant
           
            line = line.substr(sz+1);
            rad_z = std::stod(line, &sz);
            //rad_z = (rad_z * PI) / 180; // Convert to radiant

            //std::cout << x << " " << y << " " << z << " "; 
            //std::cout << rad_x << " " << rad_y << " " << rad_z << std::endl;
        }
        else
        {
            std::cerr << "Something got wrong while reading "<< main_path+img_path+pose_file_name 
                      << ". Execution interrupted" << std::endl;
            break;
        }
        // Number of the image to read
        std::string number;
        if (count < 10)
        {
            std::string tmp = static_cast<std::ostringstream*>( &(std::ostringstream() << count) )->str();
            number = "00"+tmp;
        }
        else if (count >= 10 && count < 100)
        {
            std::string tmp = static_cast<std::ostringstream*>( &(std::ostringstream() << count) )->str();
            number = "0" + tmp;
        }
        else
            number = static_cast<std::ostringstream*>( &(std::ostringstream() << count) )->str();
        //std::cout << "Reading image " << main_path+img_path+img_name+number+img_extension << std::endl;

        vpImageIo::read(I, main_path+img_path+img_name+number+img_extension);
        // We need a cv::Mat image to use OpenCV to find the corners 
        cv::Mat frame = cv::imread(main_path+img_path+img_name+number+img_extension, CV_LOAD_IMAGE_GRAYSCALE);
       
        std::vector<cv::Point2f> corners; 
        vpDisplay::display(I);

        //std::this_thread::sleep_for(std::chrono::seconds(1));

        // s -> patternSize = Size(corners_per_row, corners_per_colum)
        // Default flags -> CV_CALIB_CB_ADAPTIVE_THRESH + CV_CALIB_CB_NORMALIZE_IMAGE
        // findChessboardCorners finds all the corners in the main dataset only with CV_CALIB_CB_FILTER_QUADS
        bool found = cv::findChessboardCorners( frame, s, corners, //CV_CALIB_CB_ADAPTIVE_THRESH + 
                                                                   // CV_CALIB_CB_NORMALIZE_IMAGE + 
                                                                      CV_CALIB_CB_FILTER_QUADS
                                                                   //+CV_CALIB_CB_FAST_CHECK 
                                              ); 
        // In the datasets in which findChessboardCorners doesn't find all the corners
        // we try to resize the image and repeat the research
        if( !found )
        {
            std::cout << "Corners not found." << std::endl;

            cv::Mat frame_resize = cv::imread(main_path+img_path+img_name+number+img_extension, CV_LOAD_IMAGE_GRAYSCALE);
            int downsampling_factor = 2;
            cv::resize( frame, frame_resize, cv::Size( width_image_px/downsampling_factor, width_image_px/downsampling_factor) );
            bool found_resize = cv::findChessboardCorners( frame_resize, s, corners,  // CV_CALIB_CB_ADAPTIVE_THRESH + 
                                                                                      // CV_CALIB_CB_NORMALIZE_IMAGE + 
                                                                                         CV_CALIB_CB_FILTER_QUADS
                                                                                      //+CV_CALIB_CB_FAST_CHECK 
                                                         ); 
            if( found_resize )
            {
                std::cout << "Corners found in the second research." << std::endl;

                for( int j=0; j<corners.size(); ++j )
                {
                    corners[j].x *= downsampling_factor;
                    corners[j].y *= downsampling_factor;
                }

                found = true;
            }
        }
        
        if( found ) // findChessboardCorners returns true if it finds exactly 's' corners 
        {
            for( int i=0; i<corners.size(); ++i )
            {
                //std::cout << corners[i].x << " " << corners[i].y << " " << i << std::endl;
                std::string str = "" + std::to_string(i);
                cv::putText(frame, str, cv::Point(corners[i].x, corners[i].y), CV_FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0,0,0), 1.0);
            }
            cv::cornerSubPix(frame, corners, cv::Size(11, 11), cv::Size(-1, -1), cv::TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
            cv::drawChessboardCorners(frame, s, corners, found);
            images_corners.push_back( corners );

            // To find the orientation of the chessboard I need to know where are the two white circles
            int x_1 = (corners[6].x + corners[7].x + corners[14].x + corners[15].x)/4;
            int y_1 = (corners[6].y + corners[7].y + corners[14].y + corners[15].y)/4;
            //std::cout << "Point 1: " << x_1 << ", " << y_1 << std::endl;

            int x_2 = (corners[0].x + corners[1].x + corners[8].x + corners[9].x)/4;
            int y_2 = (corners[0].y + corners[1].y + corners[8].y + corners[9].y)/4;
            //std::cout << "Point 2: " << x_2 << ", " << y_2 << std::endl;

            std::string str = "" + std::to_string(00);
            cv::putText(frame, str, cv::Point(x_1, y_1), CV_FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0,0,0), 1.0);
            cv::putText(frame, str, cv::Point(x_2, y_2), CV_FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0,0,0), 1.0);

            cv::Scalar intensity_1 = frame.at<uchar>(y_1, x_1);
            int inten_1 = (int) intensity_1.val[0];
            //std::cout << "inten 1: " << inten_1 << std::endl;

            cv::Scalar intensity_2 = frame.at<uchar>(y_2, x_2);
            int inten_2 = (int) intensity_2.val[0];
            //std::cout << "inten 2: " << inten_2 << std::endl;

            std::cout << "Image " << img << ". Corners found." << std::endl;
            if(inten_1 > 200 & inten_2 < 50) // The white circle is in the top left
                invertedP = false;           // The order of the vector 'point' is ok

            if(inten_1 < 50 & inten_2 > 200) // The white circle is in the top right
            {
                inverted_count++;
                std::cout << "But they are reversed (inverted_count = " << inverted_count << ")." << std::endl;
                invertedP = true;            // The order of the vector 'point' is wrong (we will use 'invertedPoint')
            }     

            areCornersImagesInverted.push_back( invertedP );
        }
        else
            std::cerr << "Corners image " << img << " not found." << std::endl;
        
        vpDisplay::display(I);
        
        // Save the image in the directory img_corners_path
        cv::imwrite( main_path+img_path+img_corners_path+img_name+number+img_extension, frame);
        //std::cout << main_path+img_path+img_corners_path+img_name+number+img_extension << std::endl;

        if( found )
        {
            // Show the found corners 
            for( int i=0; i<corners.size() && corners.size()>0; ++i )
            {
                vpImagePoint p(corners[i].y, corners[i].x);
                vpDisplay::displayCircle(I, p, 4, vpColor::red, false, 2);
            }

            vpDisplay::displayText(I, vpImagePoint(10,10), "Blue circles should be roughly inside red points", vpColor::blue);
            
            
            vpHomogeneousMatrix cMo; // Matrix camera -> object
            if(!invertedP)
                obj_util.computePose(point, corners, cam, true, cMo);         // With false computePose doesn't work
            else
                obj_util.computePose(invertedPoint, corners, cam, true, cMo);

            // To display the reference frame
            vpDisplay::displayFrame(I, cMo, cam, 3, vpColor::none, 3);
            vpDisplay::flush(I);

            c_o.push_back(cMo); // Add the matrix to the vector

            // zyz convention to compute the world -> end-effector matrix 
            vpRzyzVector r_ZYZ(rad_x, rad_y, rad_z); 
            //vpRxyzVector r_XYZ(rad_x,rad_y,rad_z); // It doesn't work

            vpRotationMatrix R(r_ZYZ);
            //if( img == 0 )
            //    std::cout << "R" << R << std::endl;

            vpHomogeneousMatrix wMe(vpTranslationVector(x,y,z), R); // Matrix world -> end-effector 
            w_e.push_back(wMe); // Add the matrix to the vector

            // Debug, repoject points to have a visual confirmation of the computed cMo
            for(int i = 0; i < point.size(); i++)
            {
                point[i].project(cMo);
                double x = point[i].p[0], y = point[i].p[1];
                vpImagePoint p(0,0);
                vpMeterPixelConversion::convertPoint(cam, x, y, p);
                vpDisplay::displayCircle(I, p, 2, vpColor::blue, false, 2);
            }
    
            vpDisplay::flush(I);
        }
        else
            vpDisplay::flush(I);

        //std::this_thread::sleep_for (std::chrono::seconds(1)); // To visualize the images
    } // End for
  
    poses_file.close();

    // Homogeneous matrix representing the transformation between the end-effector and the camera
    vpHomogeneousMatrix eMc;

    if( c_o.size() == w_e.size() )
    {
        std::cout << "\n *** calibrationTsai working with " << c_o.size() << " acquisitions *** " << std::endl;

        // Hand-eye calibration function that implements R. Tsai and R. Lorenz method
        // c_o[in]:  vector of homogeneous matrices representing the transformation between the camera and the scene
        // w_e[in]:  vector of homogeneous matrices representing the transformation between the base of the manipulator
        //           and the end-effector
        // eMc[out]: hand-eye calibration matrix
        vpCalibration::calibrationTsai(c_o, w_e, eMc);
        // Visualization of results
        obj_util.stampResult( w_e, eMc, c_o );
    }
    else
        std::cerr << "Bad data acquisition, failed calibration." << std::endl;

    std::vector<int> images_error_order_2D, images_error_order_3D;

    int IT_error_2D = obj_util.returnITError2D( w_e, eMc, c_o, images_corners, 
                                                areCornersImagesInverted, images_error_order_2D,
                                                height-1, width-1, width_image_px, height_image_px, 
                                                focal_x, focal_y, c_x, c_y, L );

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr input_cloud( new pcl::PointCloud<pcl::PointXYZRGB> );
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr output_cloud( new pcl::PointCloud<pcl::PointXYZRGB> );

    int error_3D = obj_util.returnReprojectionError3D( w_e, eMc, c_o, images_error_order_3D,
                                                       height-1, width-1, 
                                                       input_cloud, output_cloud,
                                                       L );

    if( images_error_order_2D.size() == images_error_order_3D.size() )
    {
        std::cout << "Increasing error image order (returnITError2D): " << std::endl;

        for( int i=0; i<images_error_order_2D.size(); ++i )
            std::cout << images_error_order_2D[i] << " ";

        std::cout << "\nIncreasing error image order (returnReprojectionError3D): " << std::endl;

        for( int i=0; i<images_error_order_3D.size(); ++i )
            std::cout << images_error_order_3D[i] << " ";

        std::cout << std::endl;
    }
    else
    {
        std::cerr << "Bad computed error, failed comparison: \nSize 2D - 3D: ";
        std::cerr << images_error_order_2D.size() << " - " << images_error_order_3D.size() << std::endl;
    }
    
    // Visualization of one projection 3D example
    pcl::visualization::PCLVisualizer viewer("PCL Viewer");

    viewer.setBackgroundColor( 255.0, 255.0, 255.0);
    viewer.addCoordinateSystem( 3 );
    viewer.addText ("Source PointCloud -> red, Reprojected PointCloud -> blue", 20, 20);
    viewer.initCameraParameters();
    // Cloud 1: red / Cloud 2: blue
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> single_color_1( input_cloud, 255, 0, 0 );
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> single_color_2( output_cloud, 0, 0, 255 );
    // Add both
    viewer.addPointCloud<pcl::PointXYZRGB>( input_cloud, single_color_1, "cloud_1" );
    viewer.addPointCloud<pcl::PointXYZRGB>( output_cloud, single_color_2, "cloud_2" );

    // Loop for visualization (so that the visualizers are continuously updated):
    std::cout << "Visualization... "<< std::endl;
    
    viewer.spin ();
    
    return 0;
}