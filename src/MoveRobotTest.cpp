/**
 *   MoveRobotTest.cpp
 *
 *   Last edit: 03/04/2017
 *   Author: Andrea Gobbi
**/

#include "test_hand_eye/AutoAcquisitionUtil.h"

cv::Mat leftImage; 

/** 
 * @brief leftImageCallback read the left camera image from the topic
 * @param[in]  msg: info message of the left camera 
 * @return     
 */
void leftImageCallback( const sensor_msgs::ImageConstPtr& msg ) 
{
    cv_bridge::CvImagePtr cv_ptr;

    try
    { 
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        leftImage = cv_ptr->image.clone();

        // Image visualization
        cv::namedWindow("Left image");
        cv::imshow("Left image", leftImage);
        cv::startWindowThread();    
    }
    catch(cv_bridge::Exception& e)
    {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "MoveRobot_node");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ROS_INFO(" --- MoveRobot_node active --- ");

    std::string configuration_path = "/home/andrea/workspace/ros/catkin/src/test_hand_eye/config/Configuration_RA_FindChessboardFrame.yaml";
    test_hand_eye::AutoAcquisitionUtil obj_acq_util;

    cv::FileStorage fs( configuration_path, cv::FileStorage::READ );
    if( !fs.isOpened() ) 
    {
        ROS_ERROR("Error opening configuration file");
        exit(-1);
    }

    std::string base_frame, image_topic, path_project, sub_dir_acquisitions;
    double time = 0;
    double x = 0, y = 0, z = 0, r_x = 0, r_y = 0, r_z = 0; // Goal position
    
    fs["image_topic"] >> image_topic;
    fs["path_project"] >> path_project;
    fs["sub_dir_acquisitions"] >> sub_dir_acquisitions;
    fs["MoveRobotTest_frame"] >> base_frame;
    fs["ros_time"] >> time;
    fs["t_x_w"] >> x;
    fs["t_y_w"] >> y;
    fs["t_z_w"] >> z;
    fs["r_x_w"] >> r_x;
    fs["r_y_w"] >> r_y;
    fs["r_z_w"] >> r_z;

    fs.release();

    // Read left camera images from the topic image_raw
    image_transport::ImageTransport imgLeft( nh );
    image_transport::Subscriber subLeft = imgLeft.subscribe( image_topic, 1, leftImageCallback );  

    //The :move_group_interface:`MoveGroup` class can be easily setup using just the name 
    //of the group you would like to control and plan for.
    moveit::planning_interface::MoveGroup group("manipulator");
    //We will use the :planning_scene_interface:`PlanningSceneInterface` class to deal directly with the world.
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface; 
    //If you don't provide any start state, Moveit will use the current state
    //group.setStartStateToCurrentState();

    // Get the tolerance that is used for reaching an orientation goal. 
    // This is the tolerance for roll, pitch and yaw, in radians. 
    double ori_tolerance = group.getGoalOrientationTolerance();
    // Get the tolerance that is used for reaching a position goal. 
    // This is be the radius of a sphere where the end-effector must reach. 
    double pose_tolerance = group.getGoalPositionTolerance();
    std::cout << "Position tolerance: " << pose_tolerance << ", Orientation tolerance: " << ori_tolerance << std::endl;
    //Set the orientation tolerance that is used for reaching the goal when moving to a pose.
    //group.setGoalOrientationTolerance(0.01);
    //ori_tolerance = group.getGoalOrientationTolerance();
    //Set the position tolerance that is used for reaching the goal when moving to a pose. 
    //group.setGoalPositionTolerance(0.001);
    //pose_tolerance = group.getGoalPositionTolerance();
    //std::cout << "Position tolerance: " << pose_tolerance << ", Orientation tolerance: " << ori_tolerance << std::endl; 

    std::string frame = group.getPoseReferenceFrame();
    std::cout << "Frame: " << frame << std::endl;

    group.setPoseReferenceFrame(base_frame);
    frame = group.getPoseReferenceFrame();
    std::cout << "Frame: " << frame << std::endl;

    //Set the number of times the motion plan is to be computed from scratch 
    //before the shortest solution is returned. The default value is 1
    int num_attempts = 3;
    group.setNumPlanningAttempts(num_attempts);
    
    tf::Vector3 transl_tf( x, y, z );

    tf::Quaternion quat;
    quat.setRPY( radians(r_x), radians(r_y), radians(r_z) );            
    
    bool success = obj_acq_util.planPoseMotionToGoal( group, transl_tf, quat, time );
    std::cout << "Success attempt: " << success << std::endl;

    if(success)
        cv::imwrite( path_project + sub_dir_acquisitions + "TestImage.png", leftImage);

    return 0;
}