/**
 *   FindChessboardFrame.cpp
 *
 *   Last edit: 06/04/2017
 *   Author: Andrea Gobbi
**/

#include "test_hand_eye/AutoAcquisitionUtil.h"

cv::Mat leftImage; 
sensor_msgs::CameraInfo info_camera_left;

/** 
 * @brief leftImageCallback read the left camera image from the topic
 * @param[in]  msg: info message of the left camera 
 * @return     
 */
void leftImageCallback( const sensor_msgs::ImageConstPtr& msg ) 
{
    cv_bridge::CvImagePtr cv_ptr;

    try
    { 
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        leftImage = cv_ptr->image.clone();

        // Image visualization
        cv::namedWindow("Left image");
        cv::imshow("Left image", leftImage);
        cv::startWindowThread();    
    }
    catch(cv_bridge::Exception& e)
    {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }
}

/** 
 * @brief cb_left_camera_info read info topic of the left camera 
 * @param[in]  msg: info message of the left camera 
 * @return     
 */
void cb_left_camera_info( const sensor_msgs::CameraInfo::ConstPtr& msg )
{
    info_camera_left = *msg;
}

/** 
 * @brief handEyeCalibration computes the camera - object matrix and world - end effector matrix
          for every acquisition, useful to find the hand-eye matrix
 * @param[in]  group: MoveGroup object that performs the movement
 * @param[in]  obj_acq_util: object of the AutoAcquisitionUtil class
 * @param[in]  tf_mng: object useful to find the transform between two frames
 * @param[in]  transl: translation vector (x, y, z) [m]
 * @param[in]  quat: rotation quaternion 
 * @param[in]  cam: intrinsic camera parameters
 * @param[in]  point: vector useful to compute CAM->CHESSBOARD matrix (cMo)
 * @param[in]  time: time for the function sleep
 * @param[in]  base_frame: base frame to find BASE->TCP matrix (wMe)
 * @param[in]  hand_frame: hand frame to find BASE->TCP matrix (wMe)
 * @param[out] c_o: vector of CAM->CHESSBOARD matrices
 * @param[out] w_e: vector of BASE->TCP matrices
 * @return     the success of the operation
 */
bool handEyeCalibration( moveit::planning_interface::MoveGroup& group,
                         test_hand_eye::AutoAcquisitionUtil& obj_acq_util,
                         TransformManager& tf_mng, 
                         const tf::Vector3& transl,
                         const tf::Quaternion& q,
                         const vpCameraParameters& cam,
                         std::vector<vpPoint>& point, 
                         double time,
                         std::string base_frame,
                         std::string hand_frame,
                         std::vector<vpHomogeneousMatrix>& c_o,
                         std::vector<vpHomogeneousMatrix>& w_e )
{
    // Go to the goal position
    bool success = obj_acq_util.planPoseMotionToGoal( group, transl, q, time );

    sleep(time);

    if( success )
    {
        // Find the world - end effector rototranslation matrix
        vpHomogeneousMatrix wMe;
        bool wTe_found = tf_mng.get_Tf( base_frame, hand_frame, wMe, 0.05 );

        // Find the camera - object matrix
        // It's necessary to find the circles grid in the image
        cv::Mat frame = leftImage.clone();
        cv::Size s(4, 11);
        std::vector<cv::Point2f> centers;
        bool circles_found = cv::findCirclesGrid( frame, s, centers, cv::CALIB_CB_ASYMMETRIC_GRID );

        if( circles_found )
        {
            vpHomogeneousMatrix cMo;
            obj_acq_util.computePose( point, centers, cam, true, cMo );

            if( wTe_found )
            {
                std::cout << "wMe: " << std::endl << wMe << std::endl << std::endl;
                w_e.push_back(wMe);

                std::cout << "cMo: " << std::endl << cMo << std::endl << std::endl;
                c_o.push_back(cMo);

                return true;
            }
            else
                ROS_ERROR_STREAM("World - end effector matrix not successfully computed.");
        }
        else
            ROS_ERROR_STREAM("Chessboard not found.");
    }
    else
        ROS_ERROR_STREAM("Position not reached.");

    return false;
}

/** 
 * @brief computeChessboardFrame 
 * @param[in]  nh: ROS node
 * @param[in]  obj_acq_util: object of the AutoAcquisitionUtil class
 * @param[in]  tf_mng: object useful to find the transform between two frames
 * @param[in]  configuration_path: path of the configuration file
 * @return     tf_chessboard, computed transform matrix
 */
std::vector<double> computeChessboardFrame( ros::NodeHandle& nh, 
                                            test_hand_eye::AutoAcquisitionUtil& obj_acq_util,
                                            TransformManager& tf_mng, 
                                            const std::string& configuration_path )
{
    cv::FileStorage fs( configuration_path, cv::FileStorage::READ );
    if( !fs.isOpened() ) 
    {
        ROS_ERROR("Error opening configuration file.");
        exit(-1);
    }

    int num_p = 0, number_inclinations = 0, start_from_ee_position = 0; 
    double time = 0.0, L = 0.0, d_p = 0.0;
    double t_x = 0.0, t_y = 0.0, t_z = 0.0, r_x = 0.0, r_y = 0.0, r_z = 0.0; // Start position
    double x_step = 0.0, y_step = 0.0, z_step = 0.0; // X, Y and Z step in the translation [m]
    double max_dist_x = 0.0, max_dist_y = 0.0, max_dist_z = 0.0; // X, Y, Z max distance step in the translation [m]
    std::vector<double> x_increment, y_increment, z_increment; // To have the chessboard in the center of the image [m]
    std::vector<double> x_angles, y_angles, z_angles; // To have different orientation of the end effector [degree]
    std::string image_topic, camera_info_topic;
    std::string base_frame, hand_frame, acircle_frame;
    std::string path_project, config_file_name;

    fs["num_p"] >> num_p;
    fs["d_p"] >> d_p;
    fs["ros_time"] >> time;
    fs["L"] >> L;

    fs["x_step"] >> x_step;
    fs["y_step"] >> y_step;
    fs["z_step"] >> z_step;
    fs["max_dist_x"] >> max_dist_x;
    fs["max_dist_y"] >> max_dist_y;
    fs["max_dist_z"] >> max_dist_z;
    fs["x_increment"] >> x_increment;
    fs["y_increment"] >> y_increment;
    fs["z_increment"] >> z_increment;

    fs["x_angles"] >> x_angles;
    fs["y_angles"] >> y_angles;
    fs["z_angles"] >> z_angles;

    fs["image_topic"] >> image_topic;
    fs["camera_info_topic"] >> camera_info_topic;
    fs["base_frame"] >> base_frame;
    fs["hand_frame"] >> hand_frame;
    fs["acircle_frame"] >> acircle_frame;

    fs["path_project"] >> path_project;
    fs["config_file_name"] >> config_file_name;

    fs["start_from_ee_position"] >> start_from_ee_position;
    if(start_from_ee_position == 0)
    {
        // Start position is the actual position of the end effector
        tf::Vector3 tf_vector;
        bool tf_found = tf_mng.get_t( base_frame, hand_frame, tf_vector, 0.05 );

        t_x = tf_vector[0];
        t_y = tf_vector[1];
        t_z = tf_vector[2];

        //tf::Matrix3x3 tf_rot;
        //tf_found = tf_mng.get_R( base_frame, hand_frame, tf_rot, 0.05 );

        tf::Quaternion q;
        tf_found = tf_mng.get_q( base_frame, hand_frame, q, 0.05 );

        std::cout << "q: " << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << std::endl;
/*
        Eigen::Quaternion quaternion(q.getW(), q.getX(), q.getY(), q.getZ());

        Eigen::Vector3d euler = quaternion.toRotationMatrix().eulerAngles(0, 1, 2);
*/
        tf::Matrix3x3(q).getRPY(r_x, r_y, r_z);

        r_x = degrees( r_x );
        r_y = degrees( r_y );
        r_z = degrees( r_z );

        tf::Quaternion qq;
        qq.setRPY( radians(r_x), radians(r_y), radians(r_z) );   

        std::cout << "qq: " << qq[0] << " " << qq[1] << " " << qq[2] << " " << qq[3] << std::endl;
/*
        r_x = degrees( euler[0] );
        r_y = degrees( euler[1] );
        r_z = degrees( euler[2] );*/
    }
    else
    {
        // Start position is read from configuration file
        fs["t_x_w"] >> t_x;
        fs["t_y_w"] >> t_y;
        fs["t_z_w"] >> t_z;
        fs["r_x_w"] >> r_x;
        fs["r_y_w"] >> r_y;
        fs["r_z_w"] >> r_z;
    }

    std::cout << "Start position: transl(x, y, z) = ( " << t_x << ", " << t_y << ", " << t_z << "), " 
              << "rot(x, y, z) = ( " << r_x << ", " << r_y << ", " << r_z << ")" << std::endl;

    fs.release();
    
    // Read left camera images from the topic image_raw
    image_transport::ImageTransport imgLeft( nh );
    image_transport::Subscriber subLeft = imgLeft.subscribe( image_topic, 1, leftImageCallback );  
    // Read left camera info (useful to know intrinsic parameters of the camera)
    ros::Subscriber sb_left_camera_info = nh.subscribe( camera_info_topic, 1, cb_left_camera_info);

    sleep(time);  

    std::cout << "Reading configuration file" << std::endl
              << "num_p: " << num_p << ", d_p: " << d_p 
              << ", number_inclinations: " << number_inclinations << std::endl;
    std::cout << "path_project: " << path_project << std::endl;

    //The :move_group_interface:`MoveGroup` class can be easily setup using just the name 
    //of the group you would like to control and plan for.
    moveit::planning_interface::MoveGroup group("manipulator");
    //We will use the :planning_scene_interface:`PlanningSceneInterface` class to deal directly with the world.
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface; 
    //If you don't provide any start state, Moveit will use the current state
    group.setStartStateToCurrentState();

    // Get the tolerance that is used for reaching an orientation goal. 
    // This is the tolerance for roll, pitch and yaw, in radians. 
    double ori_tolerance = group.getGoalOrientationTolerance();
    // Get the tolerance that is used for reaching a position goal. 
    // This is be the radius of a sphere where the end-effector must reach. 
    double pose_tolerance = group.getGoalPositionTolerance();
    std::cout << "Position tolerance: " << pose_tolerance << ", Orientation tolerance: " << ori_tolerance << std::endl;
    //Set the orientation tolerance that is used for reaching the goal when moving to a pose.
    //group.setGoalOrientationTolerance(0.01);
    //ori_tolerance = group.getGoalOrientationTolerance();
    //Set the position tolerance that is used for reaching the goal when moving to a pose. 
    //group.setGoalPositionTolerance(0.001);
    //pose_tolerance = group.getGoalPositionTolerance();
    //std::cout << "Position tolerance: " << pose_tolerance << ", Orientation tolerance: " << ori_tolerance << std::endl; 

    std::string frame = group.getPoseReferenceFrame();
    std::cout << "Reference frame: " << frame << std::endl;

    if( frame != base_frame )
    {
        group.setPoseReferenceFrame(base_frame);
        frame = group.getPoseReferenceFrame();
        std::cout << "Reference frame: " << frame << std::endl;
    }
    
    sleep(time);

    // Read intrinsic parameters from left/camera_info topic
    uint width = info_camera_left.width;
    uint height = info_camera_left.height;
    float focal_x = info_camera_left.K[0];
    float focal_y = info_camera_left.K[4];
    float c_x = info_camera_left.K[2];
    float c_y = info_camera_left.K[5];

    std::cout << "Image height: " << height << ", width: " << width << std::endl;
    std::cout << "Focal_x: " << focal_x << ", focal_y: " << focal_y << std::endl;
    std::cout << "C_x: " << c_x << ", c_y: " << c_y << std::endl << std::endl;

    // The main intrinsic camera parameters are (focal_x, focal_y) the ratio between the focal length and 
    // the size of a pixel, and (c_x, c_y) the coordinates of the principal point in pixel. 
    // Not in this case, but the lens distortion can also be considered by two additional parameters (k_{ud}, k_{du}).
    vpCameraParameters cam( focal_x, focal_y, c_x, c_y ); 
    
    // Object initialization, useful to compute the camera -> object matrix (cMo)
    cv::Size s(4, 11);
    std::vector<vpPoint> point( s.height * s.width );
    int j=s.height, k=0;
    //std::cout << s.height << " " << s.width << std::endl;
    while( k < s.height*s.width )
    {
        if( k%s.width == 0 )
            j--;

        point[k].setWorldCoordinates( L/2*j, L/2*(j%2)+(k%s.width)*L, 0 );
        //std::cout << "-- Point" << k << " " << point[k].oP[0] << " " << point[k].oP[1] << " " << point[k].oP[2] << std::endl; 

        k++;
    }

    sleep(time);

    // Vectors of the camera - object and world - end effector matricies
    std::vector<vpHomogeneousMatrix> c_o;
    std::vector<vpHomogeneousMatrix> w_e;

    //int attempts = 5;
    //group.setNumPlanningAttempts(attempts);

    std::vector<cv::Point2f> centers;
    bool success = false;

    if( x_angles.size() == y_angles.size() && x_angles.size() == z_angles.size() && 
        x_angles.size() == x_increment.size() && x_angles.size() == y_increment.size() && x_angles.size() == z_increment.size() )
        number_inclinations = x_angles.size();
    else
        ROS_ERROR("Check that x_angles, y_angles, z_angles, x_increment, y_increment and z_increment have the same size in the configuration file.");

    // Acquisition
    double y_temp = t_y;
    for( int plane = 0; plane < num_p; ++plane )
    {
        for( int angle = 0; angle < number_inclinations; ++angle )
        {
            // Copy the start position of the end effector
            double x_temp = t_x; 
            //double y_temp = t_y;
            double z_temp = t_z;
            double r_x_temp = r_x;
            double r_y_temp = r_y;
            double r_z_temp = r_z;

            // To center the chessboard in the image view
            //x_temp += x_increment[ angle ];
            //y_temp += y_increment[ angle ];
            z_temp += z_increment[ angle ];

            double x_temp_start = x_temp; 
            double y_temp_start = y_temp;
            double z_temp_start = z_temp;

            // Change the orientation of the end effector according to the read values
            r_x_temp += x_angles[ angle ];
            r_y_temp += y_angles[ angle ];
            r_z_temp += z_angles[ angle ];
            
            // Go in the start position
            tf::Vector3 transl( x_temp, y_temp, z_temp );
            tf::Quaternion q;
            q.setRPY( radians(r_x_temp), radians(r_y_temp), radians(r_z_temp) );   

            // Compute world - end effector and camera - object matrices
            success = handEyeCalibration( group, obj_acq_util, tf_mng, transl, q, cam, point, time, 
                                          base_frame, hand_frame, c_o, w_e );

            if( success )
                ROS_INFO_STREAM("Successful operation. Plane " << plane << ", angle " << angle << ".");
            else
            {
                ROS_ERROR_STREAM("Unsuccessful operation. Plane " << plane << ", angle " << angle << ".");
                //break;  // Go out from the inner loop
                continue; // To force the next iteration of the loop to take place
            }
            
            cv::Mat image = leftImage.clone();
            bool found = cv::findCirclesGrid( image, s, centers, cv::CALIB_CB_ASYMMETRIC_GRID ); 

            if( found )
            {
                for(int y=1; y<7; ++y)
                {
                    // Come back in the start position
                    tf::Vector3 transl_start( x_temp_start, y_temp_start, z_temp_start );
                    success = obj_acq_util.planPoseMotionToGoal( group, transl_start, q, time );

                    image = leftImage.clone();

                    x_temp = x_temp_start;
                    y_temp = y_temp_start;
                    z_temp = z_temp_start;

                    double last_x_good = x_temp;
                    double last_y_good = y_temp;
                    double last_z_good = z_temp;

                    // Change the position of the end effector until the chessboard is found
                    while( cv::findCirclesGrid(image, s, centers, cv::CALIB_CB_ASYMMETRIC_GRID) && success &&
                           x_temp < x_temp_start + max_dist_x && x_temp_start - max_dist_x < x_temp && 
                           y_temp < y_temp_start + max_dist_y && y_temp_start - max_dist_y < y_temp &&
                           z_temp < z_temp_start + max_dist_z && z_temp_start - max_dist_z < z_temp )
                    {
                        last_x_good = x_temp;
                        last_y_good = y_temp;
                        last_z_good = z_temp;

                        // Add pose at every step
                        tf::Vector3 transl_good( last_x_good, last_y_good, last_z_good );
                        // And acquisition of the two desidered matricies
                        success = handEyeCalibration( group, obj_acq_util, tf_mng, transl_good, q, cam, point, time, 
                                                      base_frame, hand_frame, c_o, w_e );

                        // Update the position that the end effector has to go
                        switch(y) 
                        {
                            case 1:
                                x_temp -= x_step;
                                break;
                            case 2:
                                x_temp += x_step;
                                break;
                            case 3:         
                                y_temp += y_step;
                                break;
                            case 4:
                                y_temp -= y_step;
                                break;
                            case 5:         
                                z_temp += z_step;
                                break;
                            case 6:
                                z_temp -= z_step;
                                break;
                        }

                        // Exection of the movement
                        tf::Vector3 transl_temp( x_temp, y_temp, z_temp );
                        success = obj_acq_util.planPoseMotionToGoal( group, transl_temp, q, time ); 

                        if( !success )
                        {
                            ROS_ERROR_STREAM("Check if the position (" << x_temp << ", " << y_temp 
                                          << ", " << z_temp << ") is reachable from the end effector."); 
                            continue;
                        }

                        // Update the view image
                        image = leftImage.clone(); 
                    }

                    // Come back to the last position where the chessboard is found 
                    //tf::Vector3 transl_good( last_x_good, last_y_good, last_z_good );
                    // And acquisition of the two desidered matricies
                    //success = handEyeCalibration( group, obj_acq_util, tf_mng, transl_good, q, cam, point, time, 
                    //                              base_frame, hand_frame, c_o, w_e );
                    //if( !success )
                    //{
                    //    ROS_ERROR_STREAM("Problem to come back to the pose.");   
                    //   continue;    
                    //}
                }
            }
            else
            {
                ROS_ERROR_STREAM("Chessboard not found in the initial position. Check if the position (" << x_temp 
                                  << ", " << y_temp << ", " << z_temp << ") is reachable from the end effector.");
            }       
        }

        y_temp -= d_p;
    }
    // Come back to the start position
    tf::Vector3 transl( t_x, t_y, t_z );
    tf::Quaternion q;
    q.setRPY( radians(r_x), radians(r_y), radians(r_z) );   
    success = obj_acq_util.planPoseMotionToGoal( group, transl, q, time );

    vpHomogeneousMatrix eMc;  // Hand-eye matrix
    vpHomogeneousMatrix wMo;  // World - chessboard matrix 

    if( c_o.size() == w_e.size() )
    {
        ROS_INFO_STREAM("Hand-eye calibration works with " << c_o.size() << " acquisitions." );

        // Found tf between end effector frame and camera frame
        vpCalibration::calibrationTsai(c_o, w_e, eMc);
        std::cout << "eMc: " << std::endl << eMc << std::endl << std::endl;

        // Tf between end effector frame and camera frame
        vpHomogeneousMatrix tf;
        bool tf_found = tf_mng.get_Tf( "/ee_link", "/left_camera_optical_frame", tf, 0.05 );

        if( tf_found )
        {
            std::cout << "/ee_link to /left_camera_optical_frame, tf: " << std::endl;
            std::cout << tf << std::endl  << std::endl;
        }
        else
            std::cout << "/ee_link to /left_camera_optical_frame not found" << std::endl << std::endl;

        vpHomogeneousMatrix cMo = c_o[ c_o.size()-1 ];
        //std::cout << "cMo: " << std::endl << cMo << std::endl << std::endl;
        cMo = cMo.inverse();

        eMc = eMc.inverse();

        vpHomogeneousMatrix wMe = w_e[ w_e.size()-1 ];
        //std::cout << "wMe: " << std::endl << wMe << std::endl << std::endl;
        wMe = wMe.inverse();

        wMo = cMo * eMc * wMe;
        //std::cout << "wMo: " << std::endl << wMo << std::endl << std::endl;

        // Found tf between world frame and chessboard frame 
        wMo = wMo.inverse();
        std::cout << "wMo: " << std::endl << wMo << std::endl << std::endl;

        //vpHomogeneousMatrix c_m;
        //To rotate a point about the x-axis by an angle of theta (90 degree)
        //P = P*[ 1,          0,           0;
        //        0, cos(theta), -sin(theta);
        //        0, sin(theta),  cos(theta);

        //To rotate a point about the y-axis by an angle of theta (90 degree)
        //P = P*[ cos(theta), 0, sin(theta);
        //                 0, 1,          0;
        //       -sin(theta), 0, cos(theta);

        // Tf between world frame and chessboard frame
        tf_found = tf_mng.get_Tf( base_frame, acircle_frame, tf, 0.05 );

        if( tf_found )
        {
            std::cout << base_frame << " to " << acircle_frame << ", tf: " << std::endl;
            std::cout << tf << std::endl << std::endl;
        }
        else
            std::cout << base_frame << " to " << acircle_frame << " not found" << std::endl << std::endl;
    }
    else
        ROS_ERROR("Some errors during the acquisition, c_o and w_e has different size.");
    
    std::vector<double> tf_chessboard = { wMo[0][0], wMo[0][1], wMo[0][2], wMo[0][3], 
                                          wMo[1][0], wMo[1][1], wMo[1][2], wMo[1][3],
                                          wMo[2][0], wMo[2][1], wMo[2][2], wMo[2][3],
                                          wMo[3][0], wMo[3][1], wMo[3][2], wMo[3][3] };
    
    std::vector<double> tf_hand_eye = { eMc[0][0], eMc[0][1], eMc[0][2], eMc[0][3], 
                                        eMc[1][0], eMc[1][1], eMc[1][2], eMc[1][3],
                                        eMc[2][0], eMc[2][1], eMc[2][2], eMc[2][3],
                                        eMc[3][0], eMc[3][1], eMc[3][2], eMc[3][3] };

    // Write the found transform in the configuration file
    cv::FileStorage fs_write( path_project + config_file_name, cv::FileStorage::WRITE );
    fs_write << "tf_chessboard" << tf_chessboard;
    fs_write << "tf_hand_eye" << tf_hand_eye;
    fs_write.release();

    cv::destroyWindow("Left image");
    
    return tf_chessboard;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "FindChessboardFrame_node");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ROS_INFO(" --- FindChessboardFrame node active --- ");

    std::string configuration_path = "/home/andrea/workspace/ros/catkin/src/test_hand_eye/config/Configuration_RA_FindChessboardFrame.yaml";
    test_hand_eye::AutoAcquisitionUtil obj_acq_util;
    TransformManager tf_mng(nh);

    cv::FileStorage fs( configuration_path, cv::FileStorage::READ );
    if( !fs.isOpened() ) 
    {
        ROS_ERROR("Error opening configuration file.");
        exit(-1);
    }

    int compute_chessboard_frame;
    std::vector<double> tf_chessboard;
    fs["compute_chessboard_frame"] >> compute_chessboard_frame;

    std::string path_project, config_file_name;
    fs["path_project"] >> path_project;
    fs["config_file_name"] >> config_file_name;

    std:: string parent_frame, new_frame;
    fs["parent_frame"] >> parent_frame;
    fs["new_frame"] >> new_frame;

    fs.release();

    sleep(3);

    if( compute_chessboard_frame == 0 )
        tf_chessboard = computeChessboardFrame( nh, obj_acq_util, tf_mng, configuration_path );
    else
    {
        cv::FileStorage fs_tf( path_project + config_file_name, cv::FileStorage::READ );
        if( !fs_tf.isOpened() ) 
        {
            ROS_ERROR_STREAM("Error opening " << config_file_name << " file.");
            exit(-1);
        }

        fs_tf["tf_chessboard"] >> tf_chessboard;

        fs_tf.release();
    }

    if( tf_chessboard.size() == 16 ) // 4*4 matrix
    {
        ROS_INFO_STREAM("Publication of the new frame (" << new_frame << ").");

        double tf_t_x = tf_chessboard[3];
        double tf_t_y = tf_chessboard[7];
        double tf_t_z = tf_chessboard[11];
        tf::Vector3 tf_transl(tf_t_x, tf_t_y, tf_t_z);
        std::cout << "Translation vector: " << tf_t_x << " " << tf_t_y << " " << tf_t_z << std::endl;

        tf::Matrix3x3 tf_rot;
        tf_rot[0][0] = tf_chessboard[0]; tf_rot[0][1] = tf_chessboard[1]; tf_rot[0][2] = tf_chessboard[2];
        tf_rot[1][0] = tf_chessboard[4]; tf_rot[1][1] = tf_chessboard[5]; tf_rot[1][2] = tf_chessboard[6];
        tf_rot[2][0] = tf_chessboard[8]; tf_rot[2][1] = tf_chessboard[9]; tf_rot[2][2] = tf_chessboard[10];

        tf::Quaternion tf_q(0, 0, 0, 0);
        tf_rot.getRotation( tf_q );
        std::cout <<"Quaternion from rotation matrix: "<< tf_q.x() <<" "<< tf_q.y() <<" "<< tf_q.z() <<" "<< tf_q.w() << std::endl;

        tf::TransformBroadcaster br;
        tf::Transform transform;

        ros::Rate rate(10.0);
        while( nh.ok() )
        {
            // Publish the new frame
            transform.setOrigin( tf_transl );
            transform.setRotation( tf_q );
            br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), parent_frame, new_frame));
            rate.sleep();
        }
    }
    else
        ROS_ERROR("Error in the tf computation.");

    return 0;
}