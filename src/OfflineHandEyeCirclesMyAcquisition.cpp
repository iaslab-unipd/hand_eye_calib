/*
 *   OfflineHandEyeCircles.cpp
 *
 *   Last edit: 20/04/2017
 */

#include "test_hand_eye/HandEyeUtil.h"

int main(int argc, char** argv)
{
    test_hand_eye::HandEyeUtil obj_util;

    std::string main_path = "/home/andrea/workspace/ros/catkin/src/test_hand_eye/my_acquisition";
    std::string pose_file_name = "/Robot.txt";
    std::string img_extension = ".bmp";
    std::string img_name = "/Img";

    int width_image_px = 480, height_image_px = 640;
    double L = 0.04; // [m]
    int width = 4, height = 11;
    double focal_x = 381.362, focal_y = 381.362;
    double c_x = 320.5, c_y = 240.5;

    //vpCameraParameters cam(800, 800, 415.575, 344.406);
    vpCameraParameters cam(focal_x, focal_y, c_x, c_y); 

    //object initialization
    cv::Size s(width, height);
    std::vector<cv::Point2f> centers;
    std::vector<vpPoint> point(s.height*s.width);
    int j=s.height, k=0;
    while (k < s.height*s.width)
    {
        if (k%s.width == 0)
            j--;

        point[k].setWorldCoordinates(L/2*j, L/2*(j%2)+(k%s.width)*L, 0);
        //std::cout << "-- Point" << k << " " << point[k].oP[0] << " " << point[k].oP[1] << " " << point[k].oP[2] << std::endl; 

        k++;
    }

    vpImage< unsigned char > I(width_image_px, height_image_px); // le immagini del dataset sono 800*800 pixel
    vpDisplayX d(I);
    std::vector<vpHomogeneousMatrix> c_o;
    std::vector<vpHomogeneousMatrix> w_e;
    //std::vector<vpPoseVector> poses;

    int count = -1;
    std::ifstream poses_file;
    poses_file.open(main_path+pose_file_name);

    while(true)
    {
        double x, y, z, rx, ry, rz; //traslazione e rotazione
        if(poses_file.is_open())
        {
            std::string line;
            if (getline (poses_file ,line))
                count++;
            else
                break;

            std::string::size_type sz;
            std::cout<<"RIGA "<<count<< ": "<<line<<"\n"<<"VALORI ESTRATTI : ";
            x = std::stod(line, &sz);
            std::cout<<x<<" ";
            line = line.substr(sz+1);
            y = std::stod(line, &sz);
            std::cout<<y<<" ";
            line = line.substr(sz+1);
            z = std::stod(line, &sz);
            std::cout<<z<<" ";
            line = line.substr(sz+1);
            rx = std::stod(line, &sz);
            std::cout<<rx<<" ";
            line = line.substr(sz+1);
            ry = std::stod(line, &sz);
            std::cout<<ry<<" ";
            line = line.substr(sz+1);
            rz = std::stod(line, &sz);
            std::cout<<rz<<" "<<std::endl;
            //std::cout<<"VALORI RIGA "<<count<< " : "<<x<<" "<<y<<" "<<z<<" "<<rx<<" "<<ry<<" "<<rz<<" "<<std::endl;
        }
        else
        {
            std::cout<<"Something got wrong while reading "<<pose_file_name<<". Execution interrupted"<<std::endl;
            break;
        }

        std::string number;
        if (count < 10){
            std::string tmp = static_cast<std::ostringstream*>( &(std::ostringstream() << count) )->str();
            number = "00"+tmp;
        }
        else if (count >= 10 && count < 100){
            std::string tmp = static_cast<std::ostringstream*>( &(std::ostringstream() << count) )->str();
            number = "0" + tmp;
        }
        else
            number = static_cast<std::ostringstream*>( &(std::ostringstream() << count) )->str();;

        std::cout<<"reading image "<<main_path+img_name+number+img_extension<<"\n"<<std::endl;
        vpImageIo::read(I, main_path+img_name+number+img_extension);

        //std::this_thread::sleep_for (std::chrono::seconds(1));
        cv::Mat frame = cv::imread(main_path+img_name+number+img_extension);

        //vpImageConvert::convert(I, frame);
        
        // -> da sostituire per trovare i corner nella scacchiera
                                                // s -> patternSize = Size(points_per_row, points_per_colum)
        bool found = cv::findCirclesGrid(frame, s, centers, cv::CALIB_CB_ASYMMETRIC_GRID /*|| cv::CALIB_CB_CLUSTERING*/);

        //for(int i=0; i<centers.size(); ++i)
        //{
        //    std::cout << centers[i].x << " " << centers[i].y << " " << i << std::endl;
        //    std::string str = "" + std::to_string(i);
        //    cv::circle(frame, cv::Point(centers[i].x, centers[i].y), 5, cv::Scalar(125,125,125));
        //    cv::putText(frame, str, cv::Point(centers[i].x, centers[i].y), CV_FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255,255,255), 1.0);
        //}
        //cv::imwrite(main_path+img_visp_res_path+img_name+number+img_extension, frame);
        
        vpDisplay::display(I);
        if(found)
        {
            for(int i = 0; i < centers.size() && centers.size() > 0; i++)
            {
                vpImagePoint p(centers[i].y,centers[i].x);
                vpDisplay::displayCircle(I, p, 4, vpColor::red, false, 2);
            }
            vpDisplay::displayText(I, vpImagePoint(10,10), "Blue circles should be roughly inside red points", vpColor::blue);
            vpDisplay::flush(I);
            
            vpHomogeneousMatrix cMo;   
            obj_util.computePose(point, centers, cam, true, cMo); // cMo viene riempita
            std::cout << "cMo: " << std::endl << cMo << std::endl << std::endl;
            c_o.push_back(cMo); // c_o conterrà tutte le cMo delle varie posizioni

            double rad_x = rx; //* 3.1415 / 180;
            double rad_y = ry; //* 3.1415 / 180;
            double rad_z = rz; //* 3.1415 / 180;
            //std::cout << "rad_x: " << rad_x << ", rad_y: " << rad_y << ", rad_z: " << rad_z << std::endl;

            // Conversione da x-y-z -> z-y-z
            // prima era vpRxyzVector
            vpRzyzVector r(rad_x,rad_y,rad_z); // già fatta qua in automatico con funzione di visp
            //vpRxyzVector r(rad_x,rad_y,rad_z);
            //std::cout << "r: " << std::endl << r << std::endl << std::endl;
            vpRotationMatrix R(r);
            //std::cout << "R: " << std::endl << R << std::endl << std::endl;
            vpHomogeneousMatrix wMe(vpTranslationVector(x,y,z), R);
            std::cout << "wMe: " << std::endl << wMe << std::endl << std::endl;

            w_e.push_back(wMe); // w_e conterrà tutte le wMe delle varie posizioni

            //DEBUG
            for (int i = 0; i < point.size(); i++)
            {
                //repoject points to have a visual confirmation of the computed cMo
                point[i].project(cMo);
                // i tre valori di point[i].oP rimangono come inizializzati, mentre cambiano point[i].p[0] e point[i].p[1]
                // std::cout << "-- Point" << i << " " << point[i].oP[0] << " " << point[i].oP[1] << " " << point[i].oP[2] << std::endl; 

                double x = point[i].p[0], y = point[i].p[1];
                //std::cout << "x e y: " << x << " " << y << std::endl;

                vpImagePoint p(0,0);
                vpMeterPixelConversion::convertPoint(cam, x, y, p); // p punto in cui è stato stimato il centro del cerchio
                //std::cout << "p: " << std::endl << p << std::endl << std::endl;

                vpDisplay::displayCircle(I, p, 2, vpColor::blue ,false, 2);
            }
            vpDisplay::flush(I);
            //END DEBUG

            //vpImageIo::write(I, main_path+img_visp_res_path+img_name+number+img_extension);
        }
        //std::this_thread::sleep_for (std::chrono::seconds(1));
    }//end while

    poses_file.close();

    //Calibrazione Hand-eye
    vpHomogeneousMatrix eMc;

    std::cout<<"\n\nworking with "<<count+1<< " acquisitions"<<std::endl;

    if (c_o.size() == w_e.size()) //per sicurezza
    {
        std::cout << "c_o and w_e size: " << std::endl << c_o.size() << std::endl << std::endl;

        vpCalibration::calibrationTsai(c_o, w_e, eMc);
        //std::cout << "c_o -2: " << std::endl << c_o[c_o.size()-2] << std::endl << std::endl;
        //std::cout << "w_e -2: " << std::endl << w_e[c_o.size()-2] << std::endl << std::endl;
        //std::cout << "c_o -1: " << std::endl << c_o[c_o.size()-1] << std::endl << std::endl;
        //std::cout << "w_e -1: " << std::endl << w_e[c_o.size()-1] << std::endl << std::endl;
        std::cout << "eMc: " << std::endl << eMc << std::endl << std::endl;

        vpQuaternionVector q;
        vpTranslationVector t;
        
        eMc.extract(t);
        //std::cout << "t: " << t << std::endl << std::endl;

        eMc.extract(q);
        //std::cout << "r: " << q << std::endl << std::endl;

        vpRotationMatrix RR(q);

        //std::cout << "RR: " << std::endl << RR << std::endl << std::endl;

        Eigen::Matrix3d e_RR;
        e_RR << RR[0][0], RR[0][1], RR[0][2], 
               RR[1][0], RR[1][1], RR[1][2],
               RR[2][0], RR[2][1], RR[2][2]; 
        //std::cout << "e_RR: " << std::endl << e_RR << std::endl << std::endl;

        Eigen::Vector3d e_Angles_xyz = e_RR.eulerAngles(0, 1, 2); // 0->x, 1->y, 2->z

        //std::cout << "e_Angles_xyz: " << std::endl << e_Angles_xyz << std::endl << std::endl;

        Eigen::Vector3d e_Angles_zyz = e_RR.eulerAngles(2, 1, 2); // 0->x, 1->y, 2->z

        //std::cout << "e_Angles_zyz: " << std::endl << e_Angles_zyz << std::endl << std::endl;

        vpRxyzVector r_xyz(e_Angles_xyz[0], e_Angles_xyz[1], e_Angles_xyz[2]); 
        vpRotationMatrix RR_xyz(r_xyz);

        //std::cout << "RR_xyz: " << std::endl << RR_xyz << std::endl << std::endl;

        vpRzyzVector r_zyz(e_Angles_zyz[0], e_Angles_zyz[1], e_Angles_zyz[2]); 
        vpRotationMatrix RR_zyz(r_zyz);

        //std::cout << "RR_zyz: " << std::endl << RR_zyz << std::endl << std::endl;
    }

    //std::vector<double> errors = obj_util.calculateErrors(eMc, c_o, w_e);

    return 0;
}
